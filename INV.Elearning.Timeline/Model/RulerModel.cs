﻿namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: RulerModel.cs
    // Description: Dữ liệu cho RulerTimeline
    // Develope by : Nguyen Van Ngoc
    // History:
    // 22/03/2018 : Modifier
    //---------------------------------------------------------------------------
    /// <summary>
    /// Dữ liệu cho RulerTimeline
    /// </summary>
    public class RulerModel
    {
        /// <summary>
        /// Độ chia nhỏ nhất
        /// </summary>
        public double MinimummScale { get; set; }
        /// <summary>
        /// Độ chia
        /// </summary>
        public double Scale { get; set; }
        /// <summary>
        /// Độ chia lớn nhất
        /// </summary>
        public double MaximumScale { get; set; }
        /// <summary>
        /// Thời điểm bắt đầu
        /// </summary>
        public double StartTime { get; set; }
        /// <summary>
        /// Độ chia thời gian
        /// </summary>
        public double ScaleTime { get; set; }
        /// <summary>
        /// Khoảng cuộn
        /// </summary>
        public double ScrollOffset { get; set; }
        /// <summary>
        /// Tổng thời gian trình chiếu
        /// </summary>
        public double TotalTime { get; set; }
        /// <summary>
        /// Thời điểm bắt đầu Preview
        /// </summary>
        public double LeftTime { get; set; }
        /// <summary>
        /// Thời điểm kết thúc Preview
        /// </summary>
        public double RightTime { get; set; }
        /// <summary>
        /// Thời điểm hiện tại khi Preview
        /// </summary>
        public double HeadTime { get; set; }
        /// <summary>
        /// Vị trí bắt đầu vùng chọn Preview
        /// </summary>
        public double LeftTimePixel { get; set; }
        /// <summary>
        /// Vị trí kết thúc vùng chọn Preview
        /// </summary>
        public double RightTimePixel { get; set; }
        /// <summary>
        /// Vị trí hiện tại khi Preview
        /// </summary>
        public double HeadTimePixel { get; set; }
        /// <summary>
        /// Vị trí ứng với TotalTime
        /// </summary>
        public double TotalTimePixel { get; set; }
        /// <summary>
        /// Độ rộng của khay chứa
        /// </summary>
        public double TrackManagerWidth { get; set; }
        /// <summary>
        /// Trạng thái Ẩn/Hiện các thumb trên <see cref="RulerTimelineContainer"/>
        /// </summary>
        public bool IsShowThumbs { get; set; }
    }
}
