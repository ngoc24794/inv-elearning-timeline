﻿using INV.Elearning.Core.Model;
using System.Windows;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: CuePointModel.cs
    // Description: Dữ liệu cho CuePoint
    // Develope by : Nguyen Van Ngoc
    // History:
    // 22/03/2018 : Modifier
    //---------------------------------------------------------------------------
    /// <summary>
    /// Dữ liệu cho CuePoint
    /// </summary>
    public class CuePointModel : RootModel
    {
        /// <summary>
        /// Tên của CuePoint
        /// </summary>
        public static string PreCuePointName = Application.Current.TryFindResource("CuePoint") as string;
        private int _order;
        private double _timePixel;

        public CuePointModel(int order, double time)
        {
            Order = order;
            Time = time;
        }
        /// <summary>
        /// Số thứ tự
        /// </summary>
        public int Order
        {
            get => _order; set
            {
                _order = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// Thời điểm trên thước ứng với một vị trí của CuePoint
        /// </summary>
        public double Time { get; set; }
        /// <summary>
        /// Tọa độ của CuePoint trên thước
        /// </summary>
        public double TimePixel
        {
            get => _timePixel; set
            {
                _timePixel = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// Tên
        /// </summary>
        public new string Name { get => $"{PreCuePointName} {Order}"; }

        #region Methods
        /// <summary>
        /// Tạo nhân bản
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            return new CuePointModel(Order, Time) { TimePixel = TimePixel };
        }
        /// <summary>
        /// Lấy data
        /// </summary>
        /// <returns></returns>
        public CuePointData GetData()
        {
            return new CuePointData(Order, Time);
        }
        #endregion

    }
}
