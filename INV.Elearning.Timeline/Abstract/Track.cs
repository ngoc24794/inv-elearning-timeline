﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: Track.cs
    // Description: Lớp cơ sở tạo khay
    // Develope by : Nguyen Van Ngoc
    // History:
    // 08/03/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp cơ sở tạo khay
    /// </summary>
    public abstract class Track : Control
    {
        public static readonly DependencyProperty IsSelectedProperty;
        public static readonly DependencyProperty TargetNameProperty;

        /// <summary>
        /// Khởi tạo DependencyProperties
        /// </summary>
        static Track()
        {
            IsSelectedProperty = DependencyProperty.Register("IsSelected", typeof(bool), typeof(Track), new PropertyMetadata(false));
            TargetNameProperty = DependencyProperty.Register("TargetName", typeof(string), typeof(Track), new PropertyMetadata(string.Empty));
        }

        #region Thuộc tính IsSelected
        /// <summary>
        /// Khay được chọn
        /// </summary>
        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }
        #endregion

        #region Thuộc tính TargetName
        /// <summary>
        /// Tên của đối tượng trên trang trình chiếu
        /// được quản lí bởi khay này
        /// </summary>
        public string TargetName
        {
            get { return (string)GetValue(TargetNameProperty); }
            set { SetValue(TargetNameProperty, value); }
        }
        #endregion

        #region Xử lí sự kiện PreviewMouseDown
        /// <summary>
        /// Xử lí chọn khay
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {

            //bật/tắt chọn
            //IsSelected = true;
            if (DataContext is ObjectElement objectElement)
            {
                if (Keyboard.Modifiers == ModifierKeys.Control)
                {
                    objectElement.IsSelected = objectElement.IsShow && !objectElement.Locked;
                }
                else
                {
                    if (!objectElement.IsSelected)
                    {
                        ObjectElementsHelper.UnSelectedAll(objectElement);
                    }
                    objectElement.IsSelected = objectElement.IsShow && !objectElement.Locked;
                }
            }

        }
        #endregion
    }
}
