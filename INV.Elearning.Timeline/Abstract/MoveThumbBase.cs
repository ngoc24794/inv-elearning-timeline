﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Timeline;
using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: MoveThumbBase.cs
    // Description: Lớp tạo các nút co giãn/di chuyển RightTrack
    // Develope by : Nguyen Van Ngoc
    // History:
    // 14/3/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Nút di chuyển RightTrack
    /// </summary>
    public abstract class MoveThumbBase : Thumb
    {
        #region Properties
        /// <summary>
        /// Đồng hồ cuộn khay chứa sang phải
        /// </summary>
        public DispatcherTimer TimerScrollToRight { get; set; }
        /// <summary>
        /// Đồng hồ cuộn khay chứa sang trái
        /// </summary>
        public DispatcherTimer TimerScrollToLeft { get; set; }
        /// <summary>
        /// Vị trí đang cuộn
        /// </summary>
        public double ScrollOffset { get; set; }
        /// <summary>
        /// Lưu giá trị của TotalTime để xử lí việc thay đổi TotalTime bằng cách kéo thumb
        /// </summary>
        protected double OldTotalTime { get; set; }
        protected double Delta { get; set; }
        internal List<TimingState> TimingStates { get; set; }
        internal RulerState RulerState { get; set; }
        #endregion

        #region Contructors
        /// <summary>
        /// Hàm tạo thể hiện
        /// </summary>
        public MoveThumbBase()
        {
            Template = null;
            DragStarted += OnDragStarted;
            DragDelta += OnDragDelta;
            DragCompleted += OnDragComleted;
            //---------------------------------------------------------------------------
            // Khởi tạo các đồng hồ sẽ cuộn khay chứa sau mỗi 1/10 giây
            //---------------------------------------------------------------------------
            TimerScrollToRight = new DispatcherTimer();
            TimerScrollToRight.Tick += OnTimerScrollToRightTick;
            TimerScrollToRight.Interval = TimeSpan.FromMilliseconds(100);
            TimerScrollToLeft = new DispatcherTimer();
            TimerScrollToLeft.Tick += OnTimerScrollToLeftTick;
            TimerScrollToLeft.Interval = TimeSpan.FromMilliseconds(100);
        }

        protected virtual void OnDragStarted(object sender, DragStartedEventArgs e)
        {
            if (TimelineHelper.FindParent<Timeline>(this) is Timeline container)
            {
                if (container.DataContext is TimelineViewModel viewModel)
                {
                    // Khởi tạo và ghi trạng thái cũ cho Ruler
                    RulerState = new RulerState(viewModel.Ruler);
                    RulerState.SetOldState();

                    // Khởi tạo và ghi trạng thái cũ cho Timing
                    TimingStates = new List<TimingState>();
                    List<ObjectElement> objectElements = viewModel.GetObjectElements(false);
                    if (objectElements != null)
                    {
                        foreach (ObjectElement item in objectElements)
                        {
                            if (item.IsSelected && item.Timing != null)
                            {
                                TimingState timingState = new TimingState(item.Timing);
                                timingState.SetOldState();
                                TimingStates.Add(timingState);
                            }
                        }
                    }
                }
            }
        }

        protected virtual void OnDragComleted(object sender, DragCompletedEventArgs e)
        {
            if (TimelineHelper.FindParent<Timeline>(this) is Timeline container)
            {
                if (container.DataContext is TimelineViewModel viewModel)
                {
                    if (TimingStates != null)
                    {
                        // Ghi giá trị mới cho Ruler
                        RulerState.SetNewState();
                        // Ghi giá trị mới cho Timing
                        foreach (TimingState state in TimingStates)
                        {
                            state.SetNewState();
                        }
                        Global.PushUndo(new ThumbDragStep(TimingStates, RulerState, UpdateTracks));
                    }
                }
            }
        }
        #endregion

        #region EventHandlers
        /// <summary>
        /// Xử lí sự kiện kéo khay hiện tại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected abstract void OnDragDelta(object sender, DragDeltaEventArgs e);

        /// <summary>
        /// Xử lí cuộn khay chứa sang trái
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnTimerScrollToLeftTick(object sender, EventArgs e)
        {
            ScrollOffset = -60;
            if (TimelineHelper.FindParent<MoveThumbAdorner>(this) is MoveThumbAdorner container)
            {
                if (TimelineHelper.FindParent<Timeline>(container.AdornedElement) is Timeline timeline && timeline.DataContext is TimelineViewModel viewModel)
                {
                    Drag(viewModel, ScrollOffset);
                    timeline.HorizontalBar.Value += ScrollOffset;
                    timeline.RightTrackManagerScrollViewer.ScrollToHorizontalOffset(timeline.HorizontalBar.Value);
                }
            }
        }

        /// <summary>
        /// Xử lí cuộn khay chứa sang phải
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnTimerScrollToRightTick(object sender, EventArgs e)
        {
            ScrollOffset = 60;
            if (TimelineHelper.FindParent<MoveThumbAdorner>(this) is MoveThumbAdorner container)
            {
                if (TimelineHelper.FindParent<Timeline>(container.AdornedElement) is Timeline timeline && timeline.DataContext is TimelineViewModel viewModel)
                {
                    Drag(viewModel, ScrollOffset);
                    timeline.HorizontalBar.Value += ScrollOffset;
                    timeline.RightTrackManagerScrollViewer.ScrollToHorizontalOffset(timeline.HorizontalBar.Value);
                }
            }
        }

        public virtual void Drag(TimelineViewModel viewModel, double change) { }
        /// <summary>
        /// Xử lí bắt chuột khi khay chứa hiện tại được nhấn
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            CaptureMouse();
            UpdateTotalTime();
        }

        /// <summary>
        /// Xử lí thả chuột, ngừng đồng hồ và xóa dải băng xanh 
        /// khi sự kiện thả chuột phát sinh trên khay hiện tại
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            ReleaseMouseCapture();
            RemoveRippleAdoner();
            if (TimerScrollToRight.IsEnabled)
            {
                TimerScrollToRight.Stop();
                if (TimelineHelper.FindParent<Timeline>(this) is Timeline timeline && timeline.DataContext is TimelineViewModel viewModel)
                {
                    timeline.HorizontalBar.Value = viewModel.Ruler.TrackManagerWidth;
                    timeline.RightTrackManagerScrollViewer.ScrollToRightEnd();
                }
            }
            TimerScrollToLeft.Stop();
            UpdateTotalTime();
        }

        private void UpdateTotalTime()
        {
            if (TimelineHelper.FindParent<Timeline>(this) is Timeline container)
            {
                if (container.DataContext is TimelineViewModel viewModel)
                {
                    OldTotalTime = viewModel.Ruler.TotalTime;
                }
            }
            //if (TimelineHelper.FindParent<MoveThumbAdorner>(this) is MoveThumbAdorner container)
            //{
            //    if (container.DataContext is TimelineViewModel viewModel)
            //    {
            //        OldTotalTime = viewModel.Ruler.TotalTime;
            //    }
            //}
        }
        /// <summary>
        /// Vẽ bề mặt cho đối tượng này
        /// </summary>
        /// <param name="dc"></param>
        protected override void OnRender(DrawingContext dc)
        {
            // vẽ hình chữ nhật trong suốt chiếm trọn bề mặt để đối tượng có thể bắt sự kiện
            dc.DrawRectangle(Brushes.Transparent, null, new Rect(new Point(0, 0), RenderSize));
        }

        protected override void OnContextMenuOpening(ContextMenuEventArgs e)
        {
            //ContextMenu = BuildContextMenu();
        }

        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            if (e.RightButton == MouseButtonState.Pressed)
            {
                ContextMenu contextMenu = BuildContextMenu();
                contextMenu.IsOpen = true;
            }
        }

        private ContextMenu BuildContextMenu()
        {
            ContextMenu contextMenu = new ContextMenu();

            //if (DataContext is RightTrack track)
            //{
            if (TimelineHelper.FindParent<Timeline>(this) is Timeline timeline)
            {
                if (timeline.DataContext is TimelineViewModel timelineViewModel)
                {
                    contextMenu = timeline.Resources["TrackContextMenu1"] as ContextMenu;
                    if (timelineViewModel.CuePoint.CuePoints.Count > 0)
                    {
                        contextMenu = timeline.Resources["TrackContextMenu2"] as ContextMenu;
                    }
                    if (contextMenu != null)
                    {
                        contextMenu.DataContext = timelineViewModel.Track;
                        timelineViewModel.Track.CurrentTrack = DataContext as ObjectElement;
                    }
                }
            }
            //}

            return contextMenu;
        }

        protected void UpdateTracks(bool ignoreGroup = true)
        {
            //---------------------------------------------------------------------------
            // Cập nhật lại View và dữ liệu cho các khay
            //---------------------------------------------------------------------------
            if (TimelineHelper.FindParent<MoveThumbAdorner>(this) is MoveThumbAdorner container)
            {
                if (container.DataContext is TimelineViewModel viewModel)
                {
                    viewModel.UpdateTracks(ignoreGroup);
                }
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Xử lí xóa dải băng xanh
        /// </summary>
        public void RemoveRippleAdoner()
        {
            if (TimelineHelper.FindParent<Timeline>(this) is Timeline container)
            {
                if (container.DataContext is TimelineViewModel viewModel)
                {
                    viewModel.Ruler.LeftRipple = viewModel.Ruler.RightRipple = 0;
                }
            }
        }

        /// <summary>
        /// Xử lí thêm dải băng xanh
        /// </summary>
        protected void AddRippleAdorner()
        {
            if (TimelineHelper.FindParent<Timeline>(this) is Timeline container)
            {
                if (container.DataContext is TimelineViewModel viewModel)
                {
                    ObjectElement[] listTrack = new ObjectElement[viewModel.GetObjectElements().Count];
                    viewModel.GetObjectElements().CopyTo(listTrack, 0);
                    TimelineHelper.CalculateRipple(listTrack, out double left, out double right);
                    viewModel.Ruler.LeftRipple = left;
                    viewModel.Ruler.RightRipple = right;
                }
            }
        }

        /// <summary>
        /// Xử lí cuộn khay chứa sang trái hay sang phải
        /// </summary>
        protected void StartScrolling()
        {
            if (Timeline.Current.RightTrackManagerScrollViewer is ScrollViewer scrollViewer)
            {
                double mouseX = Mouse.GetPosition(scrollViewer).X;
                Console.WriteLine($"{mouseX}, {scrollViewer.ActualWidth}");
                if (mouseX > scrollViewer.ActualWidth)
                {
                    TimerScrollToRight.Start();
                }
                else if (mouseX < 0)
                {
                    TimerScrollToLeft.Start();
                }
                else
                {
                    TimerScrollToLeft.Stop();
                    TimerScrollToRight.Stop();
                }
            }
        }
        #endregion
    }
}
