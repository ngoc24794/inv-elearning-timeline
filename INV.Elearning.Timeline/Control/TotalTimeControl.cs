﻿using INV.Elearning.Controls.Audio;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Timeline;
using INV.Elearning.Core.View;
using INV.Elearning.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Threading;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: TotalTimeControl.cs
    // Description: Điều khiển thanh xác định tổng thời gian trình chiều của trang
    // Develope by : Nguyen Van Ngoc
    // History:
    // 22/03/2018 : Modifier
    //---------------------------------------------------------------------------
    /// <summary>
    /// Điều khiển thanh xác định tổng thời gian trình chiều của trang
    /// </summary>
    public partial class TotalTimeControl : Thumb
    {
        internal RulerState RulerState { get; set; }

        #region Xử lí cuộn khay chứa
        private void InitalizeTimer()
        {
            TimerScrollToRight = new DispatcherTimer();
            TimerScrollToRight.Tick += OnTimerScrollToRightTick;
            TimerScrollToRight.Interval = TimeSpan.FromMilliseconds(100);
            TimerScrollToLeft = new DispatcherTimer();
            TimerScrollToLeft.Tick += OnTimerScrollToLeftTick;
            TimerScrollToLeft.Interval = TimeSpan.FromMilliseconds(100);
        }

        public double ScrollOffset { get; set; }
        private void OnTimerScrollToLeftTick(object sender, EventArgs e)
        {
            ScrollOffset = -60;
            if (TimelineHelper.FindParent<Timeline>(this) is Timeline timeline && timeline.DataContext is TimelineViewModel viewModel)
            {
                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing is Timing timing)
                {
                    DragToLeft(viewModel, timing.TotalTime, ScrollOffset / viewModel.Ruler.Scale * 0.1);
                }
                timeline.HorizontalBar.Value += ScrollOffset;
                timeline.RightTrackManagerScrollViewer.ScrollToHorizontalOffset(timeline.HorizontalBar.Value);
            }
        }

        private void OnTimerScrollToRightTick(object sender, EventArgs e)
        {
            ScrollOffset = 60;
            if (TimelineHelper.FindParent<Timeline>(this) is Timeline timeline && timeline.DataContext is TimelineViewModel viewModel)
            {
                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing is Timing timing)
                {
                    timing.TotalTime += ScrollOffset / timing.ScaleRuler * 0.1;
                    viewModel.Ruler.TotalTimePixel = (viewModel.Ruler.TotalTime / 0.1).Round() * timeline.ScaleRuler;
                    viewModel.UpdateTracks(false);
                }
                timeline.HorizontalBar.Value += ScrollOffset;
                timeline.RightTrackManagerScrollViewer.ScrollToHorizontalOffset(timeline.HorizontalBar.Value);
            }
        }

        private void StartScrolling(double delta, double mouseX)
        {
            if (delta > 0)
            {
                TimerScrollToRight.Start();
            }
            else if (mouseX < 0)
            {
                TimerScrollToLeft.Start();
            }
            else if (delta < 0 && mouseX > 0)
            {
                TimerScrollToRight.Stop();
                TimerScrollToLeft.Stop();
            }
        }
        public DispatcherTimer TimerScrollToRight { get; set; }
        public DispatcherTimer TimerScrollToLeft { get; set; }
        internal List<TimingState> TimingStates { get; private set; }
        #endregion

        /// <summary>
        /// Hàm tạo thể hiện
        /// </summary>
        public TotalTimeControl()
        {
            Cursor = CursorsHelper.VerticalChange;
            DragStarted += OnDragStarted;
            DragDelta += OnDragDelta;
            DragCompleted += OnDragCompleted;
            Template = null;
            InitalizeTimer();
        }

        private void OnDragStarted(object sender, DragStartedEventArgs e)
        {
            if (TimelineHelper.FindParent<ScrollViewer>(this) is ScrollViewer container && TimelineHelper.FindParent<Timeline>(this) is Timeline timeline && timeline.DataContext is TimelineViewModel viewModel)
            {
                RulerState = new RulerState(viewModel.Ruler);
                RulerState.SetOldState();

                // Khởi tạo và ghi trạng thái cũ cho Timing
                TimingStates = new List<TimingState>();
                List<ObjectElement> objectElements = viewModel.GetObjectElements(false);
                if (objectElements != null)
                {
                    foreach (ObjectElement item in objectElements)
                    {
                        if (item.IsSelected && item.Timing != null)
                        {
                            TimingState timingState = new TimingState(item.Timing);
                            timingState.SetOldState();
                            TimingStates.Add(timingState);
                        }
                    }
                }
            }
        }

        private void OnDragCompleted(object sender, DragCompletedEventArgs e)
        {
            //RulerState.SetNewState();
            //Global.PushUndo(new DragTotalTimeControlStep(RulerState));
            TimerScrollToLeft?.Stop();
            TimerScrollToRight?.Stop();
            if (TimelineHelper.FindParent<ScrollViewer>(this) is ScrollViewer container && TimelineHelper.FindParent<Timeline>(this) is Timeline timeline && timeline.DataContext is TimelineViewModel viewModel)
            {
                if (TimingStates != null)
                {
                    // Ghi giá trị mới cho Ruler
                    RulerState.SetNewState();
                    // Ghi giá trị mới cho Timing
                    foreach (TimingState state in TimingStates)
                    {
                        state.SetNewState();
                    }
                    Global.PushUndo(new ThumbDragStep(TimingStates, RulerState, viewModel.UpdateTracks));
                }
                double delta = Mouse.GetPosition(container).X - container.ActualWidth;
                if (delta > 0)
                {
                    timeline.HorizontalBar.Value = viewModel.Ruler.TrackManagerWidth;
                    timeline.RightTrackManagerScrollViewer.ScrollToRightEnd();
                }
                else if (Mouse.GetPosition(container).X < 0)
                {
                    timeline.HorizontalBar.Value = viewModel.Ruler.TotalTimePixel * 2;
                    timeline.RightTrackManagerScrollViewer.ScrollToHorizontalOffset(timeline.HorizontalBar.Value);
                }
            }


        }

        private void OnDragDelta(object sender, DragDeltaEventArgs e)
        {
            if (DataContext is TimelineViewModel viewModel)
            {
                double scaleRuler = viewModel.Ruler.Scale, scaleTime = viewModel.Ruler.ScaleTime;


                RulerTimeline.Amount amount = new RulerTimeline.Amount((int)scaleRuler);
                int alpha = amount.GetSmallAmout() * (int)scaleRuler;
                int times = (int)(Math.Abs(e.HorizontalChange) / alpha);
                if (Math.Abs(e.HorizontalChange) % alpha > alpha / 2)
                {
                    double
                        delta = (e.HorizontalChange / Math.Abs(e.HorizontalChange)) * (times + 1),
                        deltaTime = delta * 0.1 * amount.GetSmallAmout();


                    if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing is Timing timing)
                    {
                        ChangeTotalTime(viewModel, timing.TotalTime, deltaTime);
                        //if (delta > 0)                                                                      // được kéo sang phải
                        //{
                        //    SetTotalTime(timing.TotalTime + deltaTime);
                        //    viewModel.Ruler.TotalTimePixel = (viewModel.Ruler.TotalTime / scaleTime).Round() * scaleRuler;
                        //    viewModel.Ruler.Notify();
                        //}
                        //else                                                                                // được kéo sang trái
                        //{
                        //    DragToLeft(viewModel, timing.TotalTime, deltaTime);
                        //}
                    }


                    //---------------------------------------------------------------------------
                    // Xử lí cuộn khay chứa nếu cần
                    //---------------------------------------------------------------------------
                    if (TimelineHelper.FindParent<ScrollViewer>(this) is ScrollViewer container)
                    {
                        delta = Mouse.GetPosition(container).X - container.ActualWidth;
                        StartScrolling(delta, Mouse.GetPosition(container).X);
                    }
                }
            }
        }

        public static bool IsShouldChangeTotalTime(TimelineViewModel viewModel, double totalTime)
        {
            List<ObjectElement> objectElements = viewModel.GetObjectElements();
            bool isShouldChange = totalTime >= MaxDuration(objectElements);
            return isShouldChange;
        }

        private static double MaxDuration(List<ObjectElement> objectElements)
        {
            if (objectElements?.Count > 0)
            {
                ObjectElement first = objectElements.FirstOrDefault(x => x.Timing?.ShowUntilEnd == false);

                if (first != null && first.Timing != null)
                {
                    double maxDuration = first.Timing.Duration;
                    foreach (ObjectElement item in objectElements)
                    {
                        if (item.Timing != null && item.Timing.ShowUntilEnd == false)
                        {
                            if (item.Timing.Duration > maxDuration)
                            {
                                maxDuration = item.Timing.Duration;
                            }
                        }
                    }

                    return maxDuration;
                }
            }
            return 0;
        }

        private static double MaxStartTime(List<ObjectElement> objectElements)
        {
            if (objectElements?.Count > 0)
            {
                ObjectElement first = objectElements.FirstOrDefault();

                if (first?.Timing != null)
                {
                    double maxStartTime = first.Timing.StartTime;
                    foreach (ObjectElement item in objectElements)
                    {
                        if (item.Timing != null)
                        {
                            if (item.Timing.StartTime > maxStartTime)
                            {
                                maxStartTime = item.Timing.StartTime;
                            }
                        }
                    }

                    return maxStartTime;
                }
            }
            return 0;
        }

        public static void ChangeTotalTime(TimelineViewModel viewModel, double totalTime, double deltaTime)
        {
            if (deltaTime > 0)                                                                      // được kéo sang phải
            {
                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing is Timing timing)
                {
                    timing.TotalTime = totalTime + deltaTime;
                }
                viewModel.Ruler.TotalTimePixel = (viewModel.Ruler.TotalTime / viewModel.Ruler.ScaleTime).Round() * viewModel.Ruler.Scale;
                viewModel.Ruler.Notify();
            }
            else                                                                                // được kéo sang trái
            {
                List<ObjectElement> objectElements = viewModel.GetObjectElements();
                double maxDuration = MaxDuration(objectElements), maxStartTime = MaxStartTime(objectElements);
                viewModel.Ruler.TotalTime = Math.Max(Math.Max(maxDuration, maxStartTime + 0.1), totalTime + deltaTime);
                viewModel.Ruler.TotalTimePixel = viewModel.Ruler.TimeToPixel(viewModel.Ruler.TotalTime);
                viewModel.Ruler.Notify();
                // Xác định có nên thay đổi TotalTime không
                //---------------------------------------------------------------------------
                //bool isShouldChange = IsShouldChangeTotalTime(viewModel, totalTime);

                //if (isShouldChange)
                //{

                //    //---------------------------------------------------------------------------
                //    // Xử lí thay đổi TotalTime
                //    //---------------------------------------------------------------------------
                //    double max = 0;
                //    foreach (ObjectElement item in viewModel.GetObjectElements())
                //    {
                //        if (item.Timing.StartTime > max)
                //        {
                //            max = item.Timing.StartTime;
                //        }
                //    }

                //    if (totalTime + deltaTime < max + 0.1)
                //    {
                //        deltaTime = -(totalTime - max - 0.1);
                //    }

                //    if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing is Timing timing)
                //    {
                //        timing.TotalTime = totalTime + deltaTime;
                //    }
                //    viewModel.Ruler.TotalTimePixel = viewModel.Ruler.TimeToPixel(viewModel.Ruler.TotalTime);
                //    viewModel.Ruler.Notify();
                //}
            }
        }

        private void DragToLeft(TimelineViewModel viewModel, double totalTime, double deltaTime)
        {
            List<ObjectElement> objectElements = viewModel.GetObjectElements();
            double maxDuration = MaxDuration(objectElements), maxStartTime = MaxStartTime(objectElements);
            viewModel.Ruler.TotalTime = Math.Max(Math.Max(maxDuration, maxStartTime + 0.1), totalTime + deltaTime);
            viewModel.Ruler.TotalTimePixel = viewModel.Ruler.TimeToPixel(viewModel.Ruler.TotalTime);
            viewModel.Ruler.Notify();
        }
        private void SetTotalTime(double value)
        {
            if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing is Timing timing)
            {
                timing.TotalTime = value;
            }
        }
        /// <summary>
        /// Vẽ thanh này
        /// </summary>
        /// <param name="dc"></param>
        //protected override void OnRender(DrawingContext dc)
        //{
        //    if (DataContext is TimelineViewModel viewModel)
        //    {
        //        Point
        //        top = new Point(-ActualWidth / 2, 0),
        //        bottom = new Point(top.X + ActualWidth, ActualHeight);
        //        dc.DrawRectangle(Brushes.LightGray, null, new Rect(top, bottom));
        //    }
        //}
    }
}
