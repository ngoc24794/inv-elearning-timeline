﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  Triangle.cs
**
** Description: Hình tam giác
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace INV.Elearning.Timeline
{
    /// <summary>
    /// Hình tam giác
    /// </summary>
    public class Triangle : Shape
    {        
        protected override Geometry DefiningGeometry => GetTriangleGeometry();

        private Geometry GetTriangleGeometry()
        {
            StreamGeometry geometry = new StreamGeometry();
            using (StreamGeometryContext c = geometry.Open())
            {
                c.BeginFigure(new Point(0, ActualHeight), true, false);
                c.LineTo(new Point(ActualWidth, 0), false, false);
                c.LineTo(new Point(ActualWidth, ActualHeight), false, false);
            }

            return geometry;
        }

        protected override Size MeasureOverride(Size constraint)
        {
            return constraint;
        }
    }
}
