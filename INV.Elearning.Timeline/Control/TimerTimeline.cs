﻿using System;
using System.Windows.Threading;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: TimerTimeline.cs
    // Description: Đồng hồ quản lí Preview trên Timeline
    // Develope by : Nguyen Van Ngoc
    // History:
    // 22/03/2018 : Modifier
    //---------------------------------------------------------------------------
    /// <summary>
    /// Đồng hồ quản lí Preview trên Timeline
    /// </summary>
    public class TimerTimeline
    {
        public TimerTimeline()
        {
            Timer = new DispatcherTimer(DispatcherPriority.Render);
        }

        #region Properties
        public DispatcherTimer Timer { get; set; }
        /// <summary>
        /// Bước nhảy thời gian
        /// </summary>
        public double Interval { get; set; }
        /// <summary>
        /// Khoảng thời gian đã trôi qua
        /// </summary>
        public double ElapsedTime { get; set; }
        /// <summary>
        /// Thời điểm bắt đầu
        /// </summary>
        public double StartTime { get; set; }
        /// <summary>
        /// Thời điểm kết thúc
        /// </summary>
        public double EndTime { get; set; }
        #endregion

        #region Event
        public event EventHandler<TimerTimelineEventArgs> Tick;
        public void RaiseTimerEvent(TimerTimelineEventArgs e)
        {
            Tick?.Invoke(this, e);
        }

        private void OnTimerTick(object sender, EventArgs e)
        {
            ElapsedTime += Interval;
            TimerTimelineEventArgs arg = new TimerTimelineEventArgs(StartTime, EndTime, ElapsedTime);
            RaiseTimerEvent(arg);
        }
        #endregion

        #region Methods
        public void Start()
        {
            ElapsedTime = 0;
            Timer.Interval = TimeSpan.FromSeconds(Interval);
            Timer.Tick -= OnTimerTick;
            Timer.Tick += OnTimerTick;
            TimerTimelineEventArgs e = new TimerTimelineEventArgs(StartTime, EndTime, ElapsedTime);
            RaiseTimerEvent(e);
            Timer.Start();
        }

        public void Pause()
        {
            if (Timer != null)
                Timer.Stop();
        }

        public void Resume()
        {
            if (Timer != null)
                Timer.Start();
        }

        public void Reset()
        {
            ElapsedTime = 0;
            if (Timer != null)
                Timer.Start();
        }

        public void Stop()
        {
            ElapsedTime = 0;
            StartTime = 0;
            EndTime = 0;
            if (Timer != null)
            {
                Timer.Stop();
            }
        }
        #endregion
    }

    /// <summary>
    /// Các đối số cho sự kiện Tick của <see cref="TimerTimeline"/>
    /// </summary>
    public class TimerTimelineEventArgs : EventArgs
    {
        public TimerTimelineEventArgs(double startTime, double endTime, double elapsedTime)
        {
            StartTime = startTime;
            EndTime = endTime;
            ElapsedTime = elapsedTime;
        }

        /// <summary>
        /// Thời điểm bắt đầu
        /// </summary>
        public double StartTime { get; set; }
        /// <summary>
        /// Thời điểm kết thúc
        /// </summary>
        public double EndTime { get; set; }
        /// <summary>
        /// Thời gian đã trôi qua
        /// </summary>
        public double ElapsedTime { get; set; }
    }
}
