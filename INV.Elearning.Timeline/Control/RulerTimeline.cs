﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: RulerTimeline.cs
    // Description: Thước thời gian
    // Develope by : Nguyen Van Ngoc
    // History:
    // 09/03/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Thước thời gian
    /// </summary>
    public class RulerTimeline : Control
    {
        #region Thuộc tính ScrollOffset
        /// <summary>
        /// Giá trị hiện tại của nút cuộn khi cuộn thước
        /// </summary>
        public double ScrollOffset
        {
            get { return (double)GetValue(ScrollOffsetProperty); }
            set { SetValue(ScrollOffsetProperty, value); }
        }

        public static readonly DependencyProperty ScrollOffsetProperty =
            DependencyProperty.Register("ScrollOffset", typeof(double), typeof(RulerTimeline), new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsRender));
        #endregion

        #region Thuộc tính ScrollWidth
        /// <summary>
        /// Độ rộng của thanh cuộn
        /// </summary>
        public double ScrollWidth
        {
            get { return (double)GetValue(ScrollWidthProperty); }
            set { SetValue(ScrollWidthProperty, value); }
        }

        public static readonly DependencyProperty ScrollWidthProperty =
            DependencyProperty.Register("ScrollWidth", typeof(double), typeof(RulerTimeline), new FrameworkPropertyMetadata(100.0, FrameworkPropertyMetadataOptions.AffectsRender));
        #endregion

        #region Thuộc tính Scale
        /// <summary>
        /// Đơn vị của thước (px)
        /// </summary>
        public double Scale
        {
            get { return (double)GetValue(ScaleProperty); }
            set { SetValue(ScaleProperty, value); }
        }

        public static readonly DependencyProperty ScaleProperty =
            DependencyProperty.Register("Scale", typeof(double), typeof(RulerTimeline), new FrameworkPropertyMetadata(15.0, FrameworkPropertyMetadataOptions.AffectsRender));
        #endregion

        #region Thuộc tính ScaleTime
        public double ScaleTime
        {
            get { return (double)GetValue(ScaleTimeProperty); }
            set { SetValue(ScaleTimeProperty, value); }
        }

        public static readonly DependencyProperty ScaleTimeProperty =
            DependencyProperty.Register("ScaleTime", typeof(double), typeof(RulerTimeline), new FrameworkPropertyMetadata(0.125, FrameworkPropertyMetadataOptions.AffectsRender));
        #endregion

        #region Thuộc tính StartTime
        public double StartTime
        {
            get { return (double)GetValue(StartTimeProperty); }
            set { SetValue(StartTimeProperty, value); }
        }

        public static readonly DependencyProperty StartTimeProperty =
            DependencyProperty.Register("StartTime", typeof(double), typeof(RulerTimeline), new FrameworkPropertyMetadata(0.125, FrameworkPropertyMetadataOptions.AffectsRender));
        #endregion

        protected override void OnRender(DrawingContext dc)
        {

            //---------------------------------------------------------------------------
            // Vẽ khung bao và nền
            //---------------------------------------------------------------------------
            dc.DrawLine(new Pen(BorderBrush, BorderThickness.Top), new Point(0, 0), new Point(RenderSize.Width, 0));
            dc.DrawLine(new Pen(BorderBrush, BorderThickness.Left), new Point(0, 0), new Point(0, RenderSize.Height));
            dc.DrawRectangle(Background, null, new Rect(0, 0, RenderSize.Width, RenderSize.Height));
            dc.DrawLine(new Pen(BorderBrush, BorderThickness.Bottom), new Point(0, RenderSize.Height), new Point(RenderSize.Width, RenderSize.Height));
            dc.DrawLine(new Pen(BorderBrush, BorderThickness.Right), new Point(RenderSize.Width, 0), new Point(RenderSize.Width, RenderSize.Height));

            // chiều cao của vạch chia
            double height = 5;

            int
                n = 0,          // số vạch chia bị bỏ qua
                i = n;          // thứ tự vạch bắt đầu
            double x = Scale;   // vị trí vạch bắt đầu


            //---------------------------------------------------------------------------
            // Nếu thước có thể cuộn
            //---------------------------------------------------------------------------
            if (ScrollWidth > 0)
            {
                double percent = ScrollOffset / ScrollWidth;

                n = (int)(percent * ActualWidth / Scale);
                i = n;
                x = (i + 1) * Scale - percent * ActualWidth;
            }
            Pen pen = new Pen(Brushes.Gray, 0.5);

            StartTime = ScrollOffset / ScrollWidth * ActualWidth / Scale * ScaleTime;

            while (x < ActualWidth)
            {
                i++;

                Time time = new Time(i * minimum);

                Amount amountObject = new Amount((int)Scale);
                int amount = amountObject.GetAmount();

                if (i % amount == 0)         // ghi số sau mỗi 10 vạch
                {
                    dc.DrawText(new FormattedText(time.ToString(), CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface(""), 11, Brushes.Gray) { TextAlignment = TextAlignment.Center }, new Point(x, ActualHeight - height - 20));
                }
                if (i % (amount / 2) == 0)         // tăng chiều cao của vạch 3px sau mỗi 5 vạch
                {
                    GuidelineSet guidelineSet = new GuidelineSet();
                    guidelineSet.GuidelinesX.Add(x + pen.Thickness / 2);
                    dc.PushGuidelineSet(guidelineSet);
                    dc.DrawLine(pen, new Point(x, ActualHeight), new Point(x, ActualHeight - height - 3));
                    dc.Pop();
                }
                else if (i % amountObject.GetSmallAmout() == 0)                    // vẽ vạch chia
                {
                    GuidelineSet guidelineSet = new GuidelineSet();
                    guidelineSet.GuidelinesX.Add(x + pen.Thickness / 2);
                    dc.PushGuidelineSet(guidelineSet);
                    dc.DrawLine(pen, new Point(x, ActualHeight), new Point(x, ActualHeight - height));
                    dc.Pop();
                }
                x += Scale;             // bắt đầu vẽ vạch tiếp theo
            }

        }

        struct Time
        {
            public Time(double seconds)
            {
                TimeSpan timeSpan = TimeSpan.FromSeconds(seconds);
                Hours = timeSpan.Hours;
                Minutes = timeSpan.Minutes;
                Seconds = timeSpan.Seconds;
                Ticks = timeSpan.Milliseconds / 100;
            }

            public int Hours { get; set; }
            public int Minutes { get; set; }
            public int Seconds { get; set; }
            public int Ticks { get; set; }

            public override string ToString()
            {
                string
                    timeFormat1 = $"{Hours}:{Minutes}:{Seconds}",
                    timeFormat2 = $"{Minutes}:{Seconds}";
                return Hours > 0 ? timeFormat1 : timeFormat2;
            }
        }

        public struct Amount
        {
            private int _scale;
            public Amount(int scale)
            {
                _scale = scale;
            }

            public int GetAmount()
            {
                if (_scale > 5)
                {
                    return 10;
                }

                switch (_scale)
                {
                    case 4: return 20;
                    case 3: return 30;
                    case 2: return 40;
                    case 1: return 50;
                }

                return 10;
            }

            public int GetSmallAmout()
            {
                if (_scale > 5)
                {
                    return 1;
                }

                switch (_scale)
                {
                    case 4: return 2;
                    case 3: return 3;
                    case 2: return 4;
                    case 1: return 5;
                }
                return 1;
            }
        }

        private double minimum = 0.1; // 0.125s
    }
}
