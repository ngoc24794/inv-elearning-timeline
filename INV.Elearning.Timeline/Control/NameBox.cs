﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: NameBox.cs
    // Description: Hộp hiển thị/hiệu chỉnh tên đối tượng trên trang trình chiếu
    // Develope by : Nguyen Van Ngoc
    // History:
    // 08/03/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Hộp hiển thị/hiệu chỉnh tên đối tượng trên trang trình chiếu
    /// </summary>
    public class NameBox : TextBox
    {
        /// <summary>
        /// Lưu dữ liệu nhập từ người dùng
        /// </summary>
        public new string Name { get; set; }

        /// <summary>
        /// Xử lí sự kiện nhấn đúp chuột vào ô này
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            Name = Text;            // lưu giá trị đang chứa trong ô
            ToggleEditable(true);   // mở tính năng soạn thảo cho ô
        }

        /// <summary>
        /// Xử lí sự kiện nhấn phím trên ô
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.Escape || e.Key == Key.Enter)      // phím Esc hoặc Enter được nhấn
            {
                if (string.IsNullOrEmpty(Text))                 // ô rỗng
                {
                    Text = Name;                                // cập nhật dữ liệu ô về lại giá trị ban đầu
                }

                ToggleEditable(false);                          // khóa tính năng soạn thảo
            }
        }

        /// <summary>
        /// Xử lí sự kiện ô bị mất focus
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLostFocus(RoutedEventArgs e)
        {
            base.OnLostFocus(e);
            IsReadOnly = true;          // khóa tính năng soạn thảo
            Cursor = Cursors.Arrow;     // thay đổi biểu tượng con trỏ chuột
        }

        /// <summary>
        /// Bật/Tắt tính năng soạn thảo của ô
        /// </summary>
        /// <param name="condition"></param>
        public void ToggleEditable(bool condition)
        {
            if (condition)
            {
                IsReadOnly = false;
                Focusable = true;
                Focus();
                Cursor = Cursors.IBeam;
            }
            else
            {
                IsReadOnly = true;
                Focusable = false;
                Cursor = Cursors.Arrow;
            }
        }
    }
}
