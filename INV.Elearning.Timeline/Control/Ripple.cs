﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: Ripple.cs
    // Description: Dải băng xanh bao lấy các khay đang được kéo bởi người dùng
    // Develope by : Nguyen Van Ngoc
    // History:
    // 22/03/2018 : Modifier
    //---------------------------------------------------------------------------
    /// <summary>
    /// Dải băng xanh bao lấy các khay đang được kéo bởi người dùng
    /// </summary>
    public class Ripple : Control
    {
        public Ripple()
        {
            Opacity = 0.5;
            IsHitTestVisible = false;
            IsShowHeadLine = true;
        }

        #region Properties
        /// <summary>
        /// Lề trái (px) 
        /// </summary>
        public double Left
        {
            get { return (double)GetValue(LeftProperty); }
            set { SetValue(LeftProperty, value); }
        }

        public static readonly DependencyProperty LeftProperty =
            DependencyProperty.Register("Left", typeof(double), typeof(Ripple), new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Lề phải (px)
        /// </summary>
        public double Right
        {
            get { return (double)GetValue(RightProperty); }
            set { SetValue(RightProperty, value); }
        }

        public static readonly DependencyProperty RightProperty =
            DependencyProperty.Register("Right", typeof(double), typeof(Ripple), new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Vị trí điểm đầu (px)
        /// </summary>
        public double Head
        {
            get { return (double)GetValue(HeadProperty); }
            set { SetValue(HeadProperty, value); }
        }

        public static readonly DependencyProperty HeadProperty =
            DependencyProperty.Register("Head", typeof(double), typeof(Ripple), new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Xác định có đường gióng đầu không
        /// </summary>
        public bool IsShowHeadLine { get; set; }
        #endregion

        /// <summary>
        /// Vẽ dải băng
        /// </summary>
        /// <param name="dc"></param>
        protected override void OnRender(DrawingContext dc)
        {

            //---------------------------------------------------------------------------
            // Vẽ dải băng 
            //---------------------------------------------------------------------------
            dc.DrawRectangle(Brushes.LightBlue, null, new Rect(new Point(Left, 0), new Point(Right, ActualHeight)));


            //---------------------------------------------------------------------------
            // Vẽ đường gióng đầu nếu cần
            //---------------------------------------------------------------------------
            if (IsShowHeadLine)
            {
                Pen pen = new Pen(Brushes.Red, 1);
                GuidelineSet guidelineSet = new GuidelineSet();
                guidelineSet.GuidelinesX.Add(Head + pen.Thickness / 2);
                dc.PushGuidelineSet(guidelineSet);
                dc.DrawLine(pen, new Point(Head, 0), new Point(Head, ActualHeight));
                dc.Pop();
            }
        }
    }
}
