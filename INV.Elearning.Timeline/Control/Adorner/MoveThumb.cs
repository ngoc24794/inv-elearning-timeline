﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: MoveThumb.cs
    // Description: Nút di chuyển RightTrack
    // Develope by : Nguyen Van Ngoc
    // History:
    // 14/3/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Nút di chuyển RightTrack
    /// </summary>
    public class MoveThumb : MoveThumbBase
    {
        public MoveThumb() : base()
        {
            //ContextMenu = Application.Current.Resources["ContextMenu"] as ContextMenu;
        }
        public override void Drag(TimelineViewModel viewModel, double change)
        {
            double
                                   scaleRuler = viewModel.Ruler.Scale;

            RulerTimeline.Amount amount = new RulerTimeline.Amount((int)scaleRuler);
            int alpha = amount.GetSmallAmout() * (int)scaleRuler;
            if (Math.Abs(change) % alpha > alpha / 2)
            {
                List<ObjectElement> objectElements = viewModel.GetObjectElements(false);

                double previousTotalTime = OldTotalTime;
                foreach (ObjectElement info in objectElements)
                {
                    if (info.IsSelected && info.Timing != null)
                    {
                        double
                            scaleTime = viewModel.Ruler.ScaleTime * amount.GetSmallAmout(),
                            totalTime = OldTotalTime;

                        int times = (int)(change / alpha);
                        double
                            startTime = info.Timing.StartTime + times * scaleTime,
                            oldStartTime = info.Timing.StartTime,
                            oldDuration = info.Timing.Duration;

                        if (startTime <= 0.0)
                        {
                            info.Timing.StartTime = 0;
                            if (info.Timing.ShowUntilEnd)
                            {
                                info.Timing.Duration = totalTime;
                            }
                        }
                        else
                        {
                            info.Timing.StartTime = startTime;

                            if (info.Timing.ShowUntilEnd)
                            {
                                if (startTime > totalTime - scaleTime)
                                {
                                    info.Timing.Duration = scaleTime;
                                }
                                else
                                {
                                    info.Timing.Duration = Math.Abs(totalTime - startTime);
                                }
                            }

                            totalTime = Math.Max(info.Timing.StartTime + info.Timing.Duration, previousTotalTime);
                        }

                        double
                            newTotalTime = Math.Max(OldTotalTime, totalTime),
                            newTotalTimePixel = viewModel.Ruler.TimeToPixel(viewModel.Ruler.TotalTime),
                            newLeftTrack = viewModel.Ruler.TimeToPixel(info.Timing.StartTime),
                            newWidthTrack = viewModel.Ruler.TimeToPixel(info.Timing.Duration);
                        viewModel.Ruler.TotalTime = newTotalTime;
                        viewModel.Ruler.TotalTimePixel = newTotalTimePixel;
                        info.Timing.LeftTrack = newLeftTrack;
                        info.Timing.WidthTrack = newWidthTrack;
                        UpdateTracks(false);
                        previousTotalTime = viewModel.Ruler.TotalTime;
                    }
                }
            }

            // thêm dải băng xanh
            AddRippleAdorner();

            // cuộn khay chứa nếu cần
            StartScrolling();
        }

        protected override void OnDragDelta(object sender, DragDeltaEventArgs e)
        {
            if (DataContext is ObjectElement objectElement)
            {
                if (TimelineHelper.FindParent<Timeline>(this) is Timeline container && container.DataContext is TimelineViewModel viewModel)
                {
                    if (!objectElement.Timing.ShowAlways)
                    {
                        Delta = e.HorizontalChange;
                        Drag(viewModel, e.HorizontalChange);
                    } 
                }
            }
            //if (TimelineHelper.FindParent<MoveThumbAdorner>(this) is MoveThumbAdorner container)
            //{
            //    if (container.DataContext is TimelineViewModel viewModel)
            //    {
            //        if (DataContext is RightTrack track)
            //        {
            //            if (track.DataContext is ObjectElement objectElement)
            //            {
            //                if (!objectElement.Timing.ShowAlways)
            //                {
            //                    Delta = e.HorizontalChange;
            //                    Drag(viewModel, e.HorizontalChange);
            //                }
            //            }
            //        }
            //    }
            //}
        }
    }
}
