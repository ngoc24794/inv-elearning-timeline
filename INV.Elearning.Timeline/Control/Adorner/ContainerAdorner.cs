﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace INV.Elearning.Timeline
{
    public abstract class Container : Control
    {
        private VisualCollection _visuals;

        public VisualCollection Visuals { get => _visuals ?? (_visuals = new VisualCollection(this)); private set => _visuals = value; }
        public Container()
        {
        }

        protected override int VisualChildrenCount => Visuals.Count;
        protected override Visual GetVisualChild(int index)
        {
            return Visuals[index];
        }        
    }
}
