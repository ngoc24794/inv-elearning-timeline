﻿using INV.Elearning.Controls.Audio;
using INV.Elearning.Core.View;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace INV.Elearning.Timeline
{
    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: MoveThumbAdorner.cs
    // Description: Lớp hỗ trợ cho RightTrack có chứa LeftThumb, RightThumb, MoveThumb
    // Develope by : Nguyen Van Ngoc
    // History:
    // 22/03/2018 : Modifier
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp hỗ trợ cho RightTrack có chứa LeftThumb, RightThumb, MoveThumb
    /// </summary>
    public class MoveThumbAdorner : Adorner
    {
        #region Visuals
        private VisualCollection _visuals;
        /// <summary>
        /// Lưu các thumb
        /// </summary>
        public VisualCollection Visuals { get => _visuals ?? (_visuals = new VisualCollection(this)); private set => _visuals = value; }
        #endregion

        #region Contructors
        /// <summary>
        /// Hàm tạo thể hiện
        /// </summary>
        /// <param name="adornedElement"></param>
        public MoveThumbAdorner(UIElement adornedElement) : base(adornedElement)
        {
            if (AdornedElement is RightTrack track)
            {
                if (track.DataContext is ObjectElement)
                {
                    BindingOperations.SetBinding(this, IsHitTestVisibleProperty, new Binding("Locked")
                    {
                        Source = track.DataContext,
                        Mode = BindingMode.TwoWay,
                        UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                        Converter = new ToggleBooleanConverter()
                    });
                }
                bool isAudio = track.DataContext is Audio;
                Visuals.Add(new LeftThumb() { DataContext = track, Cursor = Cursors.SizeWE, IsEnabled = !isAudio });
                Visuals.Add(new MoveThumb() { DataContext = track, Cursor = Cursors.SizeAll });
                Visuals.Add(new RightThumb() { DataContext = track, Cursor = Cursors.SizeWE, IsEnabled = !isAudio });
            }
        }
        #endregion

        #region Overrided Methods
        protected override int VisualChildrenCount => Visuals.Count;
        protected override Visual GetVisualChild(int index)
        {
            return Visuals[index];
        }
        /// <summary>
        /// Sắp xếp lại các thumb trên bề mặt của nó
        /// </summary>
        /// <param name="finalSize"></param>
        /// <returns></returns>
        protected override Size ArrangeOverride(Size finalSize)
        {
            if (AdornedElement is RightTrack track)
            {
                double
                    trackWidth = track.ActualWidth,
                    trackHeight = track.ActualHeight,
                    lrWidth = 4,
                    mtWidth = trackWidth - 2 * lrWidth;

                if (mtWidth <= 0)
                {
                    mtWidth = 4;
                }

                foreach (Visual visual in Visuals)
                {
                    if (visual is MoveThumbBase thumb)
                    {
                        if (thumb is LeftThumb)
                        {
                            thumb.Arrange(new Rect(0, 0, lrWidth, trackHeight));
                        }

                        if (thumb is MoveThumb)
                        {
                            thumb.Arrange(new Rect(lrWidth, 0, mtWidth, trackHeight));
                        }

                        if (thumb is RightThumb)
                        {
                            thumb.Arrange(new Rect(lrWidth + mtWidth, 0, lrWidth, trackHeight));
                        }
                    }
                }
            }
            return base.ArrangeOverride(finalSize);
        }
        #endregion
    }
}
