﻿using INV.Elearning.Core.AppCommands;
using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Windows.Controls.Primitives;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: RightThumb.cs
    // Description: Nút co giãn tại cạnh phải của RightTrack
    // Develope by : Nguyen Van Ngoc
    // History:
    // 14/3/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Nút co giãn tại cạnh phải của RightTrack
    /// </summary>
    public class RightThumb : MoveThumbBase
    {
        public override void Drag(TimelineViewModel viewModel, double change)
        {
            double scaleRuler = viewModel.Ruler.Scale;
            RulerTimeline.Amount amount = new RulerTimeline.Amount((int)scaleRuler);
            int alpha = amount.GetSmallAmout() * (int)scaleRuler;
            double scaleTime = viewModel.Ruler.ScaleTime,
            totalTime = OldTotalTime;
            if (Math.Abs(change) % alpha > alpha / 2)
            {
                int times = (int)(change / alpha);
                double totalAmount = viewModel.Ruler.TotalTimeAmount;

                List<ObjectElement> objectElements = viewModel.GetObjectElements(false);

                foreach (ObjectElement info in objectElements)
                {
                    double duration = info.Timing.Duration + times * scaleTime;

                    if (info.IsSelected && info.Timing != null)
                    {
                        if (duration <= 0.0)
                        {
                            info.Timing.ShowAlways = false;
                            info.Timing.ShowUntilEnd = false;
                            info.Timing.Duration = scaleTime;
                        }
                        else if (info.Timing.StartTime + duration < totalTime)
                        {
                            info.Timing.ShowAlways = false;
                            info.Timing.ShowUntilEnd = false;
                            info.Timing.Duration = duration;
                        }
                        else if (!info.Timing.ShowAlways && !info.Timing.ShowUntilEnd)
                        {
                            info.Timing.Duration = duration;
                            totalTime = info.Timing.StartTime + duration;
                        }

                        viewModel.Ruler.TotalTime = Math.Max(OldTotalTime, totalTime);
                        viewModel.Ruler.TotalTimePixel = viewModel.Ruler.TimeToPixel(viewModel.Ruler.TotalTime);
                        info.Timing.LeftTrack = viewModel.Ruler.TimeToPixel(info.Timing.StartTime);
                        info.Timing.WidthTrack = viewModel.Ruler.TimeToPixel(info.Timing.Duration);
                        UpdateTracks(false);
                    }
                }

                // thêm dải băng xanh
                AddRippleAdorner();

                // cuộn khay chứa nếu cần
                StartScrolling();

            }
        }
        protected override void OnDragDelta(object sender, DragDeltaEventArgs e)
        {
            if (DataContext is ObjectElement objectElement)
            {
                if (TimelineHelper.FindParent<Timeline>(this) is Timeline container && container.DataContext is TimelineViewModel viewModel)
                {
                    Drag(viewModel, e.HorizontalChange);
                }
            }
            //if (TimelineHelper.FindParent<Timeline>(this) is Timeline container)
            //{
            //    if (container.DataContext is TimelineViewModel viewModel)
            //    {
            //        if (DataContext is RightTrack track)
            //        {
            //            if (track.DataContext is ObjectElement objectElement)
            //            {
            //                Drag(viewModel, e.HorizontalChange);
            //            }
            //        }
            //    }
            //}
        }
    }
}
