﻿using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Windows.Controls.Primitives;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: LeftThumb.cs
    // Description: Nút co giãn RightTrack
    // Develope by : Nguyen Van Ngoc
    // History:
    // 14/3/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Nút co giãn RightTrack
    /// </summary>
    public class LeftThumb : MoveThumbBase
    {
        public override void Drag(TimelineViewModel viewModel, double change)
        {
            double scaleRuler = viewModel.Ruler.Scale;
            
            RulerTimeline.Amount amount = new RulerTimeline.Amount((int)scaleRuler);
            int alpha = amount.GetSmallAmout() * (int)scaleRuler;
            double
                               scaleTime = viewModel.Ruler.ScaleTime * amount.GetSmallAmout();


            //---------------------------------------------------------------------------
            // Dịch chuyển khay khi độ dời chuột đủ lớn
            //---------------------------------------------------------------------------
            if (Math.Abs(change) % alpha > alpha / 2)
            {
                int times = (int)(change / alpha);
                double totalTime = OldTotalTime;

                List<ObjectElement> objectElements = viewModel.GetObjectElements(false);

                foreach (ObjectElement info in objectElements)
                {
                    if (info.IsSelected && info.Timing != null)
                    {
                        double startTime = info.Timing.StartTime + times * scaleTime;
                        if (startTime <= 0.0)
                        {
                            if (!info.Timing.ShowAlways)
                            {
                                info.Timing.Duration += info.Timing.StartTime;
                                info.Timing.StartTime = 0;
                            }
                        }
                        else
                        {
                            if (info.Timing.ShowAlways)
                            {
                                info.Timing.ShowUntilEnd = true;
                            }
                            info.Timing.ShowAlways = false;
                            double currentTotalTime = info.Timing.StartTime + info.Timing.Duration;
                            if (info.Timing.ShowUntilEnd)
                            {
                                info.Timing.StartTime = startTime;
                                if (startTime + scaleTime > totalTime)
                                {
                                    info.Timing.Duration = scaleTime;
                                    totalTime = startTime + scaleTime;
                                }
                                else
                                {
                                    info.Timing.Duration = totalTime - startTime;
                                }
                            }
                            else if (startTime + scaleTime > currentTotalTime)
                            {
                                info.Timing.StartTime = currentTotalTime - scaleTime;
                                info.Timing.Duration = scaleTime;
                            }
                            else
                            {
                                info.Timing.StartTime = startTime;
                                info.Timing.Duration = currentTotalTime - startTime;
                            }
                        }

                        viewModel.Ruler.TotalTime = Math.Max(OldTotalTime, totalTime);
                        viewModel.Ruler.TotalTimePixel = viewModel.Ruler.TimeToPixel(viewModel.Ruler.TotalTime);
                        info.Timing.LeftTrack = viewModel.Ruler.TimeToPixel(info.Timing.StartTime);
                        info.Timing.WidthTrack = viewModel.Ruler.TimeToPixel(info.Timing.Duration);
                        UpdateTracks(false);
                    }
                }

                // thêm dải băng xanh
                AddRippleAdorner();
                // cuộn khay chứa nếu cần
                StartScrolling();
            }
        }
        protected override void OnDragDelta(object sender, DragDeltaEventArgs e)
        {
            if (DataContext is ObjectElement objectElement)
            {
                if (TimelineHelper.FindParent<Timeline>(this) is Timeline container && container.DataContext is TimelineViewModel viewModel)
                {
                    Drag(viewModel, e.HorizontalChange);
                }
            }
            //if (TimelineHelper.FindParent<Timeline>(this) is Timeline container)
            //{
            //    if (container.DataContext is TimelineViewModel viewModel)
            //    {
            //        if (DataContext is RightTrack track)
            //        {
            //            if (track.DataContext is ObjectElement objectElement)
            //            {
            //                Drag(viewModel, e.HorizontalChange);
            //            }
            //        }
            //    }
            //}
        }
    }
}