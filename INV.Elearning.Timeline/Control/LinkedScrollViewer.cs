﻿using System.Windows;
using System.Windows.Controls;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: LinkedScrollViewer.cs
    // Description: ScrollViewer được liên kết với một ScrollViewer khác
    // Develope by : Nguyen Van Ngoc
    // History:
    // 14/3/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// ScrollViewer được liên kết với một ScrollViewer khác
    /// </summary>
    public class LinkedScrollViewer : ScrollViewer
    {
        public LinkedScrollViewer()
        {
            VerticalScrollBarVisibility = ScrollBarVisibility.Hidden;
            HorizontalScrollBarVisibility = ScrollBarVisibility.Hidden;
        }

        #region Thuộc tính LinkWith
        /// <summary>
        /// Liên kết với ScrollViewer nào
        /// </summary>
        public ScrollViewer LinkWith
        {
            get { return (ScrollViewer)GetValue(LinkWithProperty); }
            set { SetValue(LinkWithProperty, value); }
        }

        public static readonly DependencyProperty LinkWithProperty =
            DependencyProperty.Register("LinkWith", typeof(ScrollViewer), typeof(LinkedScrollViewer), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(OnBoundScrollViewerPropertyChanged)));

        private static void OnBoundScrollViewerPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is LinkedScrollViewer sender && e.NewValue != null)
            {
                sender.UpdateBindings();
            }
        }
        private void UpdateBindings()
        {

            //---------------------------------------------------------------------------
            // Đồng bộ 2 ScrollViewer bằng ScrollEventHandler
            //---------------------------------------------------------------------------
            AddHandler(ScrollChangedEvent, new ScrollChangedEventHandler(OnScrollChanged));
            LinkWith.AddHandler(ScrollViewer.ScrollChangedEvent, new ScrollChangedEventHandler(OnLinkedScrollChanged));
        }
        #endregion



        public OrientationX Orientation
        {
            get { return (OrientationX)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }

        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register("Orientation", typeof(OrientationX), typeof(LinkedScrollViewer), new PropertyMetadata(OrientationX.Vertical));



        #region Xử lí sự kiện ScrollChangedEvent
        private void OnLinkedScrollChanged(object sender, ScrollChangedEventArgs e)
        {

            //---------------------------------------------------------------------------
            // Khi đối tác cuộn thì ta cuộn
            //---------------------------------------------------------------------------
            if (true)
            {
                switch (Orientation)
                {
                    case OrientationX.Vertical:
                        ScrollToVerticalOffset(e.VerticalOffset);
                        break;
                    case OrientationX.Horizontal:
                        ScrollToHorizontalOffset(e.HorizontalOffset);
                        break;
                    default:
                        break;
                }
            }
        }

        private void OnScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (LinkWith != null)
            {

                //---------------------------------------------------------------------------
                // Khi ta cuộn thì đối tác cuộn
                //---------------------------------------------------------------------------
                if (e.VerticalOffset > 0 || e.HorizontalOffset > 0)
                {
                    switch (Orientation)
                    {
                        case OrientationX.Vertical:
                            LinkWith.ScrollToVerticalOffset(e.VerticalOffset);
                            break;
                        case OrientationX.Horizontal:
                            LinkWith.ScrollToHorizontalOffset(e.HorizontalOffset);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        #endregion
    }

    public enum OrientationX
    {
        Vertical,
        Horizontal
    }
}
