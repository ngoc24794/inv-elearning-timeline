﻿using INV.Elearning.Core.Timeline;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: CuePointControl.cs
    // Description: Điểm bổ sung trên thước được dùng để gióng các khay chứa
    // Develope by : Nguyen Van Ngoc
    // History:
    // 22/03/2018 : Modifier
    //---------------------------------------------------------------------------
    /// <summary>
    /// Điểm bổ sung trên thước được dùng để gióng các khay chứa
    /// </summary>
    public class CuePointControl : Thumb
    {
        #region Order
        /// <summary>
        /// Số thứ tự của điểm này
        /// </summary>
        public int Order
        {
            get { return (int)GetValue(OrderProperty); }
            set { SetValue(OrderProperty, value); }
        }

        public static readonly DependencyProperty OrderProperty =
            DependencyProperty.Register("Order", typeof(int), typeof(CuePointControl), new PropertyMetadata(0));

        #endregion

        #region Contructors
        /// <summary>
        /// Hàm tạo
        /// </summary>
        public CuePointControl()
        {
            Width = 5;
            Height = 14;
            Template = null;
            DragDelta += OnDragDelta;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Xử lí sự kiện kéo điểm này
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDragDelta(object sender, DragDeltaEventArgs e)
        {
            if (TimelineHelper.FindParent<Timeline>(this) is Timeline timeline)
            {
                if (timeline.DataContext is TimelineViewModel timelineViewModel)
                {
                    if (DataContext is CuePointModel cuePoint)
                    {
                        double
                           change = e.HorizontalChange,
                           scaleRuler = timelineViewModel.Ruler.Scale,
                           scaleTime = timelineViewModel.Ruler.ScaleTime;

                        if (Math.Abs(change) % scaleRuler > scaleRuler / 2)
                        {
                            int times = (int)(change / scaleRuler);

                            double
                                timePixel = cuePoint.TimePixel + times * scaleRuler,
                                totalTimePixel = timelineViewModel.Ruler.TotalTimePixel;

                            if (timePixel < 0)
                            {
                                cuePoint.TimePixel = 0;
                            }
                            else if (timePixel > totalTimePixel)
                            {
                                cuePoint.TimePixel = totalTimePixel;
                            }
                            else
                            {
                                cuePoint.TimePixel = timePixel;
                            }

                            cuePoint.Time = (cuePoint.TimePixel / scaleRuler).Round() * scaleTime;
                        }
                    }

                }
            }
        }

        /// <summary>
        /// Vẽ bề mặt điểm này
        /// </summary>
        /// <param name="dc"></param>
        protected override void OnRender(DrawingContext dc)
        {
            double
                bodyHeight = 10,
                arrowHeight = ActualHeight - bodyHeight;
            Point
                topLeft = new Point(-ActualWidth, 0),
                bottomRight = new Point(ActualWidth, ActualHeight);

            Brush background = Brushes.OrangeRed, foreground = Brushes.White;


            //---------------------------------------------------------------------------
            // Vẽ thân
            //---------------------------------------------------------------------------
            dc.DrawRectangle(background, null, new Rect(topLeft, new Point(bottomRight.X, bodyHeight)));


            //---------------------------------------------------------------------------
            // Vẽ mũi tên
            //---------------------------------------------------------------------------
            Geometry arrow = new PathGeometry()
            {
                Figures = new PathFigureCollection()
                    {
                        new PathFigure()
                        {
                            StartPoint=new Point(topLeft.X, bodyHeight),
                            Segments=new PathSegmentCollection()
                            {
                                new LineSegment(){ Point = new Point(bottomRight.X, bodyHeight)},
                                new LineSegment(){ Point = new Point(0, bottomRight.Y)}
                            },
                            IsClosed = true
                        }
                    }
            };

            dc.DrawGeometry(background, null, arrow);


            //---------------------------------------------------------------------------
            // Đánh số
            //---------------------------------------------------------------------------
            dc.DrawText(new FormattedText(Order.ToString(), CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface(""), 8, foreground) { TextAlignment = TextAlignment.Center }, new Point(0, topLeft.Y + 2));

        }

        protected override void OnContextMenuOpening(ContextMenuEventArgs e)
        {
            ContextMenu = BuildContextMenu();
        }
        private ContextMenu BuildContextMenu()
        {
            ContextMenu contextMenu = new ContextMenu();

            if (TimelineHelper.FindParent<Timeline>(this) is Timeline timeline)
            {
                if (timeline.DataContext is TimelineViewModel timelineViewModel)
                {
                    contextMenu = timeline.Resources["CuePointContextMenu"] as ContextMenu;
                    contextMenu.DataContext = timelineViewModel.CuePoint;
                    if (DataContext is CuePointModel cuePoint)
                    {
                        timelineViewModel.CuePoint.CurrentCuePoint = cuePoint;
                    }
                }
            }

            return contextMenu;
        }

        #endregion

    }
}
