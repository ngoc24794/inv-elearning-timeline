﻿using INV.Elearning.Controls.Audio;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using INV.Elearning.VideoElementControl;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace INV.Elearning.Timeline
{
    public partial class Timeline : UserControl
    {
        public static TimelineViewModel TimelineViewModel { get; set; }
        public static Timeline Current { get; set; }

        public ObservableCollection<ObjectElement> ObjectElements
        {
            get { return (ObservableCollection<ObjectElement>)GetValue(ObjectElementsProperty); }
            set { SetValue(ObjectElementsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ObjectElements.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ObjectElementsProperty =
            DependencyProperty.Register("ObjectElements", typeof(ObservableCollection<ObjectElement>), typeof(Timeline), new FrameworkPropertyMetadata(null));

        public static bool IsStartTimeLine = false;
        protected override void OnLostFocus(RoutedEventArgs e)
        {
            base.OnLostFocus(e);
            if (IsStartTimeLine)
                TimelineViewModel?.Timer?.StopCommand.Execute(null);
        }
        /// <summary>
        /// Đơn vị của thước
        /// </summary>
        public double ScaleRuler
        {
            get { return (double)GetValue(ScaleRulerProperty); }
            set { SetValue(ScaleRulerProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScaleRuler.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScaleRulerProperty =
            DependencyProperty.Register("ScaleRuler", typeof(double), typeof(Timeline), new PropertyMetadata(15.0, ScaleRulerPropertyChanged));

        private static void ScaleRulerPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (Timeline.TimelineViewModel != null)
            {
                Timeline.TimelineViewModel.UpdateTimeline();
            }
        }

        public Timeline()
        {
            if (Application.Current.TryFindResource("PauseToolTip") == null)
            {
                ResourceDictionary resourceDictionary = new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/INV.Elearning.Timeline;Component/Resource/Language/vi-vn/Language.xaml", UriKind.RelativeOrAbsolute)
                };
                Application.Current.Resources.MergedDictionaries.Add(resourceDictionary);
            }
            InitializeComponent();
            TimelineViewModel = new TimelineViewModel(this);
            DataContextChanged += Timeline_DataContextChanged;
            DataContext = TimelineViewModel;
            Current = this;




            Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                //---------------------------------------------------------------------------
                // Ngừng chạy timeline
                //---------------------------------------------------------------------------
                Application.Current.MainWindow.PreviewMouseLeftButtonDown += (s, e) =>
                {
                    if (!(e.Source is INV.Elearning.Controls.DockGroup dockGroup && dockGroup.Name.Equals("dockTimeLine")))
                    {
                        if (TimelineViewModel.Timer.IsPlaying)
                        {
                            Timeline.TimelineViewModel.Timer.StopCommand.Execute(null); 
                        }
                    }
                };

                // Gán EventHanlder cho CollectionChanged event của TriggerData của các layer và các ObjectElement
                if ((Application.Current as IAppGlobal).DocumentControl?.Slides is ObservableCollection<SlideBase> slides)
                {
                    slides.CollectionChanged += Slides_CollectionChanged;
                    (Application.Current as IAppGlobal).SelectedSlideChanged += Timeline_SelectedSlideChanged;
                    foreach (var item in slides)
                    {
                        if (item is SlideBase slide)
                        {
                            slide.SelectedLayoutChanged += Slide_SelectedLayoutChanged;
                            slide.MainLayout.Elements.CollectionChanged += Elements_CollectionChanged;
                            Update(slide.MainLayout.Elements);
                            TimelineViewModel?.UpdateTimeline(slide.MainLayout);
                            slide.Layouts.CollectionChanged += Layouts_CollectionChanged;
                            if (slide.Layouts?.Count > 0)
                            {
                                foreach (LayoutBase layout in slide.Layouts)
                                {
                                    if (layout.Elements?.Count > 0)
                                    {
                                        Update(layout.Elements);
                                    }
                                }
                            }
                        }
                    }
                }
            }));
        }

        private void Timeline_SelectedSlideChanged(object sender, SelectedItemEventArgs e)
        {
            if (DataContext is TimelineViewModel viewModel)
            {
                viewModel.Timer.StopCommand.Execute(null);
            }
        }

        private void Timeline_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (DataContext is TimelineViewModel viewModel)
            {
                viewModel.Ruler.TrackManagerWidth = viewModel.Ruler.TotalTimePixel + 150;
            }
        }

        private void Slide_SelectedLayoutChanged(object sender, SelectedItemEventArgs e)
        {
            if (DataContext is TimelineViewModel viewModel)
            {
                viewModel.Ruler.TrackManagerWidth = viewModel.Ruler.TotalTimePixel + 150;
                viewModel.Timer.StopCommand.Execute(null);
            }
        }

        private void Layouts_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (var item in e.NewItems)
                {
                    if (item is LayoutBase layer)
                    {
                        SetOnCollectionChanged(layer);
                    }
                }
            }
        }

        private void SetOnCollectionChanged(LayoutBase layer)
        {
            if (layer.Elements != null)
            {
                layer.Elements.CollectionChanged += Elements_CollectionChanged;
                TimelineViewModel?.UpdateTimeline(layer);
            }
        }

        private void Elements_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            TimelineViewModel?.UpdateTimeline();
            if (e.NewItems != null)
            {
                Update(e.NewItems);
            }
        }

        private void Update(IList list)
        {
            if (list == null)
            {
                return;
            }
            foreach (ObjectElement item in list)
            {
                if (item != null)
                {
                    item.SelectionChanged -= Item_SelectionChanged;
                    item.SelectionChanged += Item_SelectionChanged;
                    if (item is VideoPlayer videoElement)
                    {
                        double
                            duration = videoElement.Duration.TotalSeconds,
                            startTime = videoElement.Timing.StartTime;
                        videoElement.Timing.Duration = Math.Round(duration, 1);
                        TimelineViewModel.Ruler.TotalTime = Math.Max(startTime + videoElement.Timing.Duration, TimelineViewModel.Ruler.TotalTime);
                        TimelineViewModel.Ruler.TotalTimePixel = TimelineViewModel.Ruler.TimeToPixel(TimelineViewModel.Ruler.TotalTime);
                        TimelineViewModel.Ruler.Notify();
                    }
                    else if (item is Audio audio)
                    {
                        audio.Timing.ShowUntilEnd = false;
                        double
                            duration = audio.Duration,
                            startTime = audio.Timing.StartTime;
                        audio.Timing.Duration = Math.Round(duration, 1);
                        TimelineViewModel.Ruler.TotalTime = Math.Max(startTime + audio.Timing.Duration, TimelineViewModel.Ruler.TotalTime);
                        TimelineViewModel.Ruler.TotalTimePixel = TimelineViewModel.Ruler.TimeToPixel(TimelineViewModel.Ruler.TotalTime);
                        TimelineViewModel.Ruler.Notify();
                        audio.DurationChanged -= Audio_DurationChanged;
                        audio.DurationChanged += Audio_DurationChanged;
                    }
                }
            }
        }

        private void Audio_DurationChanged(object sender, EventArgs e)
        {
            Audio audio = sender as Audio;
            LayoutBase selectedLayer = (Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout;
            if (selectedLayer?.Timing != null && audio != null)
            {
                audio.Timing.Duration = audio.Duration;
                audio.Timing.WidthTrack = TimelineViewModel.Ruler.CalculatePixel(audio.Timing.Duration);
                selectedLayer.Timing.TotalTime = Math.Max(selectedLayer.Timing.TotalTime, audio.Timing.StartTime + audio.Timing.Duration);
                TimelineViewModel?.UpdateTimeline(selectedLayer);
            }
        }

        private double _previousIndex = -1;
        private void Item_SelectionChanged(object sender, ObjectElementEventArg e)
        {
            if (sender is ObjectElement objectElement && objectElement.IsSelected && (Application.Current as IAppGlobal).SelectedElements.Count == 1)
            {
                int currentIndex = (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.IndexOf(objectElement);
                if (currentIndex > -1)
                {
                    Scroll(-(currentIndex - _previousIndex) * 25);
                    _previousIndex = currentIndex;
                }
            }
        }

        private void Slides_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (var item in e.NewItems)
                {
                    if (item is SlideBase slide)
                    {
                        SetOnCollectionChanged(slide.MainLayout);
                        foreach (var itemLayer in slide.Layouts)
                        {
                            if (itemLayer is LayoutBase layer)
                            {
                                SetOnCollectionChanged(layer);
                            }
                        }
                        slide.Layouts.CollectionChanged += Layouts_CollectionChanged;
                        slide.SelectedLayoutChanged += Slide_SelectedLayoutChanged;
                    }
                }
            }
        }

        protected override void OnPreviewMouseWheel(MouseWheelEventArgs e)
        {
            Scroll(e.Delta);
            if (Keyboard.Modifiers == ModifierKeys.Control)
            {
                double delta = Math.Round(e.Delta / 100.0);
                ScaleRuler = Math.Min(Slider.Maximum, Math.Max(Slider.Minimum, ScaleRuler + delta));
            }
            e.Handled = true;
        }

        private void Scroll(double delta)
        {
            VerticalBar.Value -= delta;
            double offset = VerticalBar.BoundScrollViewer.VerticalOffset - delta;
            VerticalBar.BoundScrollViewer.ScrollToVerticalOffset(offset);
            LeftTrackManagerScrollViewer.ScrollToVerticalOffset(offset);
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            if (DataContext is TimelineViewModel timelineViewModel)
            {
                switch (e.Key)
                {
                    case Key.Home:
                        timelineViewModel.HomeCommand.Execute(null);
                        break;
                    case Key.End:
                        timelineViewModel.EndCommand.Execute(null);
                        break;
                    case Key.Escape:
                        timelineViewModel.Timer.StopCommand.Execute(null);
                        break;
                }
            }
        }

        private void Border_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (DataContext is TimelineViewModel timelineViewModel)
            {
                if (timelineViewModel.Timer.IsPlaying)
                {
                    timelineViewModel.Timer.StopCommand.Execute(null);
                }
            }
        }

        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            if (Keyboard.GetKeyStates(Key.LeftCtrl) == KeyStates.Down)
            {
                Slider.Value += e.Delta;
            }
        }

        private void LeftTrackManagerScrollViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ScrollViewer scrollViewer = sender as ScrollViewer;
            TrackManager trackManager = scrollViewer.Content as TrackManager;
            trackManager.Width = scrollViewer.ActualWidth;
        }
    }
}
