﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: BindableScrollBar.cs
    // Description: Thanh cuộn điều khiển thao tác cuộn của một ScrollViewer khác
    // Develope by : Nguyen Van Ngoc
    // History:
    // 14/3/2018 : Create
    //---------------------------------------------------------------------------

    /// <summary>
    /// Thanh cuộn điều khiển thao tác cuộn của một ScrollViewer khác
    /// </summary>
    public class BindableScrollBar : ScrollBar
    {
        #region Thuộc tính BoundScrollViewer
        /// <summary>
        /// ScrollViewer được điều khiển thao tác cuộn
        /// </summary>
        public ScrollViewer BoundScrollViewer
        {
            get { return (ScrollViewer)GetValue(BoundScrollViewerProperty); }
            set { SetValue(BoundScrollViewerProperty, value); }
        }

        public static readonly DependencyProperty BoundScrollViewerProperty =
            DependencyProperty.Register("BoundScrollViewer", typeof(ScrollViewer), typeof(BindableScrollBar), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(OnBoundScrollViewerPropertyChanged)));

        private static void OnBoundScrollViewerPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is BindableScrollBar sender && e.NewValue != null)
            {
                sender.UpdateBindings();
            }
        }
        #endregion

        #region Hàm tạo thể hiện
        /// <summary>
        /// Khởi tạo thể hiện mới của lớp <see cref="BindableScrollBar"/>
        /// </summary>
        /// <param name="scrollViewer">ScrollViewer</param>
        /// <param name="o">Hướng thanh cuộn</param>
        public BindableScrollBar(ScrollViewer scrollViewer, Orientation o) : base()
        {
            Orientation = o;
            BoundScrollViewer = scrollViewer;
        }

        /// <summary>
        /// Khởi tạo thể hiện mới của lớp <see cref="BindableScrollBar"/>
        /// </summary>
        public BindableScrollBar() : base() { }
        #endregion

        #region Methods

        /// <summary>
        /// Cập nhật Bindings
        /// </summary>
        private void UpdateBindings()
        {
            RemoveHandler(ScrollEvent, new ScrollEventHandler(OnScroll));
            AddHandler(ScrollEvent, new ScrollEventHandler(OnScroll));
            //BoundScrollViewer.RemoveHandler(ScrollViewer.ScrollChangedEvent, new ScrollChangedEventHandler(BoundScrollChanged));
            //BoundScrollViewer.AddHandler(ScrollViewer.ScrollChangedEvent, new ScrollChangedEventHandler(BoundScrollChanged));
            Minimum = 0;
            if (Orientation == Orientation.Horizontal)
            {
                SetBinding(MaximumProperty, (new Binding("ScrollableWidth") { Source = BoundScrollViewer, Mode = BindingMode.OneWay }));
                SetBinding(ViewportSizeProperty, (new Binding("ViewportWidth") { Source = BoundScrollViewer, Mode = BindingMode.OneWay }));
            }
            else
            {
                SetBinding(MaximumProperty, (new Binding("ScrollableHeight") { Source = BoundScrollViewer, Mode = BindingMode.OneWay }));
                SetBinding(ViewportSizeProperty, (new Binding("ViewportHeight") { Source = BoundScrollViewer, Mode = BindingMode.OneWay }));
            }
            LargeChange = 242;
            SmallChange = 16;
        }

        /// <summary>
        /// Xử lí thay đổi vị trí cuộn của đối tượng này 
        /// khi các ScrollViewer được liên kết đã cuộn xong
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BoundScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (e.VerticalChange > 0 || e.HorizontalChange > 0)
            {
                switch (Orientation)
                {
                    case Orientation.Horizontal:
                        BoundScrollViewer.ScrollToHorizontalOffset(e.HorizontalOffset);
                        break;
                    case Orientation.Vertical:
                        BoundScrollViewer.ScrollToVerticalOffset(e.VerticalOffset);
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Xử lí cuộn ScrollViewer khi sự kiện cuộn phát sinh trên đối tượng này
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnScroll(object sender, ScrollEventArgs e)
        {
            if (e.NewValue >= 0)
            {
                switch (Orientation)
                {
                    case Orientation.Horizontal:
                        BoundScrollViewer.ScrollToHorizontalOffset(e.NewValue);
                        break;
                    case Orientation.Vertical:
                        BoundScrollViewer.ScrollToVerticalOffset(e.NewValue);
                        break;
                    default:
                        break;
                } 
            }
        }
        #endregion

    }
}