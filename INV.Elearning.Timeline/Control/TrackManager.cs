﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Controls;
using System.Windows.Input;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: TrackManager.cs
    // Description: Quản lí các khay trên timeline
    // Develope by : Nguyen Van Ngoc
    // History:
    // 08/03/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Quản lí các khay trên timeline
    /// </summary>
    public class TrackManager : ListBox
    {
        public TrackManager()
        {
        }
        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            //if (Keyboard.Modifiers == ModifierKeys.Control)
            //    return;

            //ObjectElementsHelper.UnSelectedAll();
            //if (DataContext is TimelineViewModel viewModel)
            //{
            //    //---------------------------------------------------------------------------
            //    // Xử lí chọn nhiều đối tượng khi giữ phím Ctrl
            //    //---------------------------------------------------------------------------
            //    foreach (ObjectElement item in viewModel.GetObjectElements())
            //    {
            //        item.IsSelected = false;
            //    }
            //}
        }

        protected override DependencyObject GetContainerForItemOverride()
        {
            return new ContentPresenter();
        }

        protected override AutomationPeer OnCreateAutomationPeer()
        {
            return new ControlSpecificCustomAutomationPeer(this);
        }

    }

    public class ControlSpecificCustomAutomationPeer
        : ItemsControlAutomationPeer
    {
        public ControlSpecificCustomAutomationPeer(ItemsControl owner)
            : base(owner)
        {
        }

        protected override string GetNameCore()
        {
            return "";                         // return something meaningful here..
        }

        protected override ItemAutomationPeer CreateItemAutomationPeer(object item)
        {
            return new CustomDummyItemAutomationPeer(item, this);
        }
    }

    public class CustomDummyItemAutomationPeer
        : System.Windows.Automation.Peers.ItemAutomationPeer
    {
        public CustomDummyItemAutomationPeer(object item, ItemsControlAutomationPeer itemsControlAutomationPeer)
            : base(item, itemsControlAutomationPeer)
        {
        }

        protected override string GetNameCore()
        {
            if (Item == null)
                return "";

            return Item.ToString() ?? "";
        }

        protected override AutomationControlType GetAutomationControlTypeCore()
        {
            return System.Windows.Automation.Peers.AutomationControlType.Text;
        }

        protected override string GetClassNameCore()
        {
            return "Dummy";
        }
    }

    public class TrackContainer : ListBoxItem
    {
        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            e.Handled = false;
        }
    }
}
