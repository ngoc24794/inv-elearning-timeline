﻿using System.Windows;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: Track.cs
    // Description: Khay chứa Eye và Lock trên Timeline
    // Develope by : Nguyen Van Ngoc
    // History:
    // 08/03/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Khay chứa Eye và Lock trên Timeline
    /// </summary>
    public class LeftTrack : Track
    {
        public static readonly DependencyProperty IsLookingProperty;
        public static readonly DependencyProperty IsLockedProperty;

        static LeftTrack()
        {
            IsLookingProperty = DependencyProperty.Register("IsLooking", typeof(bool), typeof(LeftTrack), new PropertyMetadata(false));
            IsLockedProperty = DependencyProperty.Register("IsLocked", typeof(bool), typeof(LeftTrack), new PropertyMetadata(false));
        }

        #region Thuộc tính IsLooking
        /// <summary>
        /// Mắt đang mở
        /// </summary>
        public bool IsLooking
        {
            get { return (bool)GetValue(IsLookingProperty); }
            set { SetValue(IsLookingProperty, value); }
        }
        #endregion

        #region Thuộc tính IsLocked
        /// <summary>
        /// Khóa được đóng
        /// </summary>
        public bool IsLocked
        {
            get { return (bool)GetValue(IsLockedProperty); }
            set { SetValue(IsLockedProperty, value); }
        }
        #endregion
    }
}
