﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Timeline;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: RulerTimelineContainer.cs
    // Description: Khay chứa các RulerThumb
    // Develope by : Nguyen Van Ngoc
    // History:
    // 22/03/2018 : Modifier
    //---------------------------------------------------------------------------
    /// <summary>
    /// Khay chứa các RulerThumb
    /// </summary>
    public class RulerTimelineContainer : Control
    {
        #region IsShowThumbs
        /// <summary>
        /// Trạng thái Ẩn/Hiện các thumb
        /// </summary>
        public bool IsShowThumbs
        {
            get { return (bool)GetValue(IsShowThumbsProperty); }
            set { SetValue(IsShowThumbsProperty, value); }
        }

        public static readonly DependencyProperty IsShowThumbsProperty =
            DependencyProperty.Register("IsShowThumbs", typeof(bool), typeof(RulerTimelineContainer), new FrameworkPropertyMetadata(false, new PropertyChangedCallback(OnIsShowThumbsPropertyChanged)));

        /// <summary>
        /// Khi dữ liệu kết nối với IsShowThumbs thay đổi thì thay đổi trạng thái  IsVisible của các thumb
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnIsShowThumbsPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is RulerTimelineContainer container && e.NewValue is bool isShowThumbs)
            {
                container.StartThumb.IsVisible = isShowThumbs;
                container.EndThumb.IsVisible = isShowThumbs;
                container.HeadThumb.IsVisible = isShowThumbs;
                container.IsFirst = isShowThumbs;
                container.InvalidateVisual();
            }
        }
        #endregion

        #region Left
        /// <summary>
        /// Kết nối với dữ liệu để xác định vị trí đặt StartThumb
        /// </summary>
        public double Left
        {
            get { return (double)GetValue(LeftProperty); }
            set { SetValue(LeftProperty, value); }
        }

        public static readonly DependencyProperty LeftProperty =
            DependencyProperty.Register("Left", typeof(double), typeof(RulerTimelineContainer), new FrameworkPropertyMetadata(0.0, new PropertyChangedCallback(OnLeftPropertyChanged)));

        private static void OnLeftPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is RulerTimelineContainer container && e.NewValue is double left)
            {
                container.StartThumb.X = left;
                container.InvalidateVisual();
            }
        }
        #endregion

        #region Right
        /// <summary>
        /// Kết nối với dữ liệu để xác định vị trí đặt EndThumb
        /// </summary>
        public double Right
        {
            get { return (double)GetValue(RightProperty); }
            set { SetValue(RightProperty, value); }
        }

        public static readonly DependencyProperty RightProperty =
            DependencyProperty.Register("Right", typeof(double), typeof(RulerTimelineContainer), new FrameworkPropertyMetadata(0.0, new PropertyChangedCallback(OnRightPropertyChanged)));

        private static void OnRightPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is RulerTimelineContainer container && e.NewValue is double right)
            {
                container.EndThumb.X = right;
                container.InvalidateVisual();
            }
        }
        #endregion

        #region Head
        /// <summary>
        /// Kết nối với dữ liệu để xác định vị trí đặt HeadThumb
        /// </summary>
        public double Head
        {
            get { return (double)GetValue(HeadProperty); }
            set { SetValue(HeadProperty, value); }
        }

        public static readonly DependencyProperty HeadProperty =
            DependencyProperty.Register("Head", typeof(double), typeof(RulerTimelineContainer), new FrameworkPropertyMetadata(0.0, new PropertyChangedCallback(OnHeadPropertyChanged)));

        private static void OnHeadPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is RulerTimelineContainer container && e.NewValue is double head)
            {
                container.HeadThumb.X = head;
                if (container.DataContext is TimelineViewModel viewModel)
                {
                    container.HeadThumb.Time = viewModel.Ruler.HeadTime;
                    //container.HeadThumb.X = viewModel.Ruler.TimeToPixel(container.HeadThumb.Time);
                    if (TimelineHelper.FindParent<Timeline>(container) is Timeline timeline)
                    {

                        ScrollBar scrollBar = timeline.HorizontalBar;
                        ScrollViewer scrollViewer = timeline.RightTrackManagerScrollViewer;
                        RulerTimeline rulerTimeline = timeline.RulerTimeline;
                        double
                            scaleRuler = viewModel.Ruler.Scale,
                            scaleTime = viewModel.Ruler.ScaleTime,
                            headTime = viewModel.Ruler.HeadTime,
                            startTimeRuler = viewModel.Ruler.StartTime,
                            endTimeRuler = startTimeRuler + viewModel.Ruler.CalculateTime(rulerTimeline.ActualWidth);
                        if (viewModel.Timer.IsPlaying)
                        {
                            if (headTime > endTimeRuler)
                            {
                                double
                                    startTime = viewModel.Ruler.HeadTime,
                                    percent = viewModel.Ruler.CalculatePixel(startTime) / scrollBar.ActualWidth;

                                scrollBar.Value = percent * scrollBar.ActualWidth;
                                scrollViewer.ScrollToHorizontalOffset(scrollBar.Value);
                            }
                        }
                        else if (!viewModel.Timer.IsFreeThumb)
                        {
                            double
                                startTime = viewModel.Ruler.HeadTime / 2,
                                percent = viewModel.Ruler.CalculatePixel(startTime) / scrollBar.ActualWidth;

                            scrollBar.Value = percent * scrollBar.ActualWidth;
                            scrollViewer.ScrollToHorizontalOffset(scrollBar.Value);
                        }

                    }


                }

                container.InvalidateVisual();
            }
        }
        #endregion

        public RulerTimelineContainer()
        {
            StartThumb = new StartThumb()
            {
                Icon = "pack://application:,,,/INV.Elearning.Timeline;component/Resource/Icon/timeline_play_start_32.png",
            };
            HeadThumb = new HeadThumb()
            {
                Icon = "pack://application:,,,/INV.Elearning.Timeline;component/Resource/Icon/timeline_play_head_32.png",
            };
            EndThumb = new EndThumb()
            {
                Icon = "pack://application:,,,/INV.Elearning.Timeline;component/Resource/Icon/timeline_play_end_32.png",
            };
            InitalizeTimer();
        }

        #region Xử lí cuộn khay chứa 
        private void InitalizeTimer()
        {
            TimerScrollToRight = new DispatcherTimer();
            TimerScrollToRight.Tick += OnTimerScrollToRightTick;
            TimerScrollToRight.Interval = TimeSpan.FromMilliseconds(100);
            TimerScrollToLeft = new DispatcherTimer();
            TimerScrollToLeft.Tick += OnTimerScrollToLeftTick;
            TimerScrollToLeft.Interval = TimeSpan.FromMilliseconds(100);
        }

        public double ScrollOffset { get; set; }
        private void OnTimerScrollToLeftTick(object sender, EventArgs e)
        {
            ScrollOffset = -60;
            if (TimelineHelper.FindParent<Timeline>(this) is Timeline timeline && timeline.DataContext is TimelineViewModel viewModel)
            {
                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing is Timing timing)
                {
                    if (StartThumb.IsHitted) StartThumb.MoveTo(viewModel.Ruler.TotalTimePixel);
                    if (HeadThumb.IsHitted) HeadThumb.MoveTo(viewModel.Ruler.TotalTimePixel);
                    if (EndThumb.IsHitted) EndThumb.MoveTo(viewModel.Ruler.TotalTimePixel);
                    UpdateRipple();
                }
                timeline.HorizontalBar.Value += ScrollOffset;
                timeline.RightTrackManagerScrollViewer.ScrollToHorizontalOffset(timeline.HorizontalBar.Value);
            }
        }

        private void OnTimerScrollToRightTick(object sender, EventArgs e)
        {
            ScrollOffset = 60;
            if (TimelineHelper.FindParent<Timeline>(this) is Timeline timeline && timeline.DataContext is TimelineViewModel viewModel)
            {
                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing is Timing timing)
                {
                    if (StartThumb.IsHitted) StartThumb.MoveTo(viewModel.Ruler.TotalTimePixel);
                    if (HeadThumb.IsHitted) HeadThumb.MoveTo(viewModel.Ruler.TotalTimePixel);
                    if (EndThumb.IsHitted) EndThumb.MoveTo(viewModel.Ruler.TotalTimePixel);
                    UpdateRipple();
                }
                timeline.HorizontalBar.Value += ScrollOffset;
                timeline.RightTrackManagerScrollViewer.ScrollToHorizontalOffset(timeline.HorizontalBar.Value);
            }
        }

        private void StartScrolling(double delta, double mouseX)
        {
            if (delta > 0)
            {
                TimerScrollToRight.Start();
            }
            else if (mouseX < 0)
                TimerScrollToLeft.Start();
            else if (delta < 0 && mouseX > 0)
            {
                TimerScrollToRight.Stop();
                TimerScrollToLeft.Stop();
            }
        }
        public DispatcherTimer TimerScrollToRight { get; set; }
        public DispatcherTimer TimerScrollToLeft { get; set; }
        #endregion

        #region Properties
        public RulerThumb StartThumb { get; set; }
        public RulerThumb HeadThumb { get; set; }
        public RulerThumb EndThumb { get; set; }

        /// <summary>
        /// Trạng thái Dính nhau của các thumb
        /// </summary>
        public bool IsCollappsed { get; private set; }

        /// <summary>
        /// Lưu điểm kiểm tra thumb có được di chuyển không 
        /// khi nó đang ở trạng thái IsCollappsed = true
        /// </summary>
        public double PreviousMouseLocation { get; private set; }

        private bool _isFirst = true;
        /// <summary>
        /// Trạng thái xác định lần đầu tiên nhấn chuột lên đối tượng này
        /// </summary>
        public bool IsFirst { get => _isFirst; set => _isFirst = value; }
        #endregion

        /// <summary>
        /// Vẽ các thumb lên bề mặt của nó
        /// </summary>
        /// <param name="dc"></param>
        protected override void OnRender(DrawingContext dc)
        {
            dc.DrawRectangle(Brushes.Transparent, null, new Rect(new Point(0, 0), RenderSize));

            try
            {
                if (StartThumb.IsVisible) StartThumb.Draw(dc, ActualHeight);
                if (EndThumb.IsVisible) EndThumb.Draw(dc, ActualHeight);
                if (HeadThumb.IsVisible) HeadThumb.Draw(dc, ActualHeight);
            }

            catch (Exception e)
            {
            }
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                double mx = e.GetPosition(this).X;
                if (DataContext is TimelineViewModel viewModel)
                {
                    // hiện các thumb
                    viewModel.Ruler.IsShowThumbs = true;


                    //---------------------------------------------------------------------------
                    // Fix để vị trí nhấn chuột trong đoạn cho phép của thước
                    //---------------------------------------------------------------------------
                    if (mx > viewModel.Ruler.TotalTimePixel)
                    {
                        mx = viewModel.Ruler.TotalTimePixel;
                    }
                    else if (mx < 0)
                    {
                        mx = 0;
                    }

                    // ghi nhận điểm nhấn chuột
                    PreviousMouseLocation = mx;
                }

                if (IsFirst)                // nếu đây là lần đầu tiên nhấn chuột
                {
                    IsFirst = false;
                    IsCollappsed = true;    // đặt cờ ghi nhận sự kiện các thumb dính nhau


                    //---------------------------------------------------------------------------
                    // Đặt các thumb cùng vị trí nhấn chuột và cập nhật dữ liệu
                    //---------------------------------------------------------------------------
                    HeadThumb.HitMe();
                    HeadThumb.MoveTo(mx);
                    StartThumb.MoveTo(mx);
                    EndThumb.MoveTo(mx);
                }
                else
                {

                    //---------------------------------------------------------------------------
                    // Xác định Thumb nào được nhấn chọn
                    //---------------------------------------------------------------------------
                    HeadThumb.GetHit(mx);
                    StartThumb.GetHit(mx);
                    EndThumb.GetHit(mx);
                    HeadThumb.MoveTo(mx);
                }

                if (!HeadThumb.IsHitted && !StartThumb.IsHitted && !EndThumb.IsHitted && IsCollappsed)
                {
                    StartThumb.ReleaseMe();
                    EndThumb.ReleaseMe();
                    HeadThumb.HitMe();
                    StartThumb.MoveTo(mx);
                    EndThumb.MoveTo(mx);
                    HeadThumb.MoveTo(mx);
                }

                //---------------------------------------------------------------------------
                // Vẽ lại Container với vị trí mới của các thumb
                //---------------------------------------------------------------------------
                InvalidateVisual();
                UpdateRipple();
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                CaptureMouse();


                //---------------------------------------------------------------------------
                // Ghi nhận điểm nhấn chuột, fix nó và lấy đơn vị của thước
                //---------------------------------------------------------------------------
                double mx = e.GetPosition(this).X, scale = 0;

                if (DataContext is TimelineViewModel viewModel)
                {
                    if (mx > viewModel.Ruler.TotalTimePixel)
                    {
                        mx = viewModel.Ruler.TotalTimePixel;
                    }
                    else if (mx < 0)
                    {
                        mx = 0;
                    }

                    scale = viewModel.Ruler.Scale;
                }


                //---------------------------------------------------------------------------
                // Nếu đang kéo HeadThumb và chúng đang dính nhau thì di chuyển cả 3 cùng lúc
                //---------------------------------------------------------------------------
                if (HeadThumb.IsHitted)
                {
                    HeadThumb.MoveTo(mx);
                    if (IsCollappsed)
                    {
                        StartThumb.MoveTo(mx);
                        EndThumb.MoveTo(mx);
                    }
                }
                else
                {

                    //---------------------------------------------------------------------------
                    // Nếu đang kéo StartThumb hoặc EndThumb và chúng đang dính nhau
                    //---------------------------------------------------------------------------
                    if ((StartThumb.IsHitted || EndThumb.IsHitted) && IsCollappsed)           // đang dính nhau
                    {
                        //---------------------------------------------------------------------------
                        // Tính độ dời chuột so với điểm nhấn ban đầu
                        //---------------------------------------------------------------------------
                        double delta = mx - PreviousMouseLocation;

                        //---------------------------------------------------------------------------
                        // Nếu đang kéo StartThumb sang phải hoặc đang kéo EndThumb sang trái hoặc
                        // độ dời chuột quá nhỏ thì không di chuyển Thumb
                        //---------------------------------------------------------------------------
                        if (Math.Abs(delta) < scale) return; // kéo đúng nhưng độ dời chuột quá nhỏ

                        IsCollappsed = false;

                        if (StartThumb.IsHitted)
                        {
                            if (delta > 0)
                            {
                                StartThumb.ReleaseMe();
                                EndThumb.HitMe();
                                EndThumb.MoveTo(mx);
                            }
                            else
                            {
                                EndThumb.ReleaseMe();
                                StartThumb.HitMe();
                                StartThumb.MoveTo(mx);
                            }
                        }
                        else if (EndThumb.IsHitted)
                        {
                            if (delta < 0)
                            {
                                EndThumb.ReleaseMe();
                                StartThumb.HitMe();
                                StartThumb.MoveTo(mx);
                            }
                            else
                            {
                                StartThumb.ReleaseMe();
                                EndThumb.HitMe();
                                EndThumb.MoveTo(mx);
                            }
                        }
                    }


                    //---------------------------------------------------------------------------
                    // Nếu đang kéo StartThumb hoặc EndThumb
                    //---------------------------------------------------------------------------
                    if ((StartThumb.IsHitted || EndThumb.IsHitted))
                    {
                        double delta = Math.Abs(StartThumb.X - EndThumb.X);

                        // chúng đang rất gần nhau
                        if (delta < scale)
                        {
                            // đặt cờ ghi nhận sự kiện dính nhau
                            IsCollappsed = true;

                            // kéo các thumb để chúng dính nhau
                            if (StartThumb.IsHitted)
                            {
                                StartThumb.MoveTo(EndThumb.X);
                            }
                            else
                            {
                                EndThumb.MoveTo(StartThumb.X);
                            }
                            HeadThumb.MoveTo(StartThumb.X);

                            // ghi nhận lại điểm kiểm tra
                            PreviousMouseLocation = HeadThumb.X;
                        }
                        //---------------------------------------------------------------------------
                        // Nếu các thumb được kéo tự do
                        //---------------------------------------------------------------------------
                        else
                        {
                            HeadThumb.MoveTo(mx);
                            if (StartThumb.IsHitted)
                            {
                                StartThumb.MoveTo(mx);
                            }
                            else
                            {
                                EndThumb.MoveTo(mx);
                            }
                        }
                    }
                    else
                    {
                        HeadThumb.MoveTo(mx);
                    }
                }

                if (TimelineHelper.FindParent<ScrollViewer>(this) is ScrollViewer container)
                {
                    double delta = e.GetPosition(container).X - container.ActualWidth;
                    StartScrolling(delta, e.GetPosition(container).X);
                }

                UpdateRipple();
                InvalidateVisual();
            }
        }

        /// <summary>
        /// Cập nhật dữ liệu xác định dải băng 
        /// khi các thumb đã di chuyển đến vị trí mới
        /// </summary>
        private void UpdateRipple()
        {
            if (DataContext is TimelineViewModel viewModel)
            {
                viewModel.Ruler.LeftTimePixel = StartThumb.X;
                viewModel.Ruler.RightTimePixel = EndThumb.X;
                viewModel.Ruler.HeadTimePixel = HeadThumb.X;
                viewModel.Ruler.UpdateSelectionTime();
                HeadThumb.Time = viewModel.Ruler.HeadTime;
            }
        }

        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {

            //---------------------------------------------------------------------------
            // Đặt lại cờ trạng thái IsCollappsed
            //---------------------------------------------------------------------------
            IsCollappsed = true;


            //---------------------------------------------------------------------------
            // Ghi nhận điểm nhấn chuột và xác định trạng thái IsHitted của các thumb
            //---------------------------------------------------------------------------
            double mouseX = e.GetPosition(this).X, x = 0;
            StartThumb.GetHit(mouseX);
            EndThumb.GetHit(mouseX);
            HeadThumb.GetHit(mouseX);


            //---------------------------------------------------------------------------
            // Xác định thumb được nhấn và lưu tọa độ của nó
            //---------------------------------------------------------------------------
            if (StartThumb.IsHitted)
            {
                x = StartThumb.X;
            }
            else if (EndThumb.IsHitted)
            {
                x = EndThumb.X;
            }
            else if (HeadThumb.IsHitted)
            {
                x = HeadThumb.X;
            }
            else
            {
                x = mouseX;
            }


            //---------------------------------------------------------------------------
            // Di chuyển các thumb lại cùng một vị trí và bắt IsHitted cho HeadThumb
            //---------------------------------------------------------------------------
            StartThumb.ReleaseMe();
            EndThumb.ReleaseMe();
            HeadThumb.HitMe();
            StartThumb.MoveTo(x);
            EndThumb.MoveTo(x);
            HeadThumb.MoveTo(x);
            UpdateRipple();
        }

        /// <summary>
        /// Xử lí sự kiện chuột được thả trên đối tượng này
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            if (DataContext is TimelineViewModel timelineViewModel)
            {
                double scaleRuler = timelineViewModel.Ruler.Scale;

                StartThumb.MoveTo((StartThumb.X / scaleRuler).Round() * scaleRuler);
                EndThumb.MoveTo((EndThumb.X / scaleRuler).Round() * scaleRuler);
                HeadThumb.MoveTo((HeadThumb.X / scaleRuler).Round() * scaleRuler);
                UpdateRipple();
            }
            StartThumb.ReleaseMe();
            HeadThumb.ReleaseMe();
            EndThumb.ReleaseMe();
            ReleaseMouseCapture();

            //---------------------------------------------------------------------------
            // Ngừng các đồng hộ cuộn khay chứa
            //---------------------------------------------------------------------------
            TimerScrollToLeft.Stop();
            TimerScrollToRight.Stop();
        }

        protected override void OnContextMenuOpening(ContextMenuEventArgs e)
        {
            ContextMenu = BuildContextMenu();
        }
        private ContextMenu BuildContextMenu()
        {
            ContextMenu contextMenu = new ContextMenu();

            if (DataContext is TimelineViewModel timelineViewModel)
            {
                if (TimelineHelper.FindParent<Timeline>(this) is Timeline timeline)
                {
                    contextMenu = timeline.Resources["RulerTimelineContainerContextMenu1"] as ContextMenu;
                    if (timelineViewModel.CuePoint.CuePoints.Count > 0)
                    {
                        contextMenu = timeline.Resources["RulerTimelineContainerContextMenu2"] as ContextMenu;
                    }
                    if (contextMenu != null)
                        contextMenu.DataContext = timelineViewModel.CuePoint;
                }
            }

            return contextMenu;
        }
    }
}
