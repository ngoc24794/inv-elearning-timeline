﻿using INV.Elearning.Core.View;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: RightTrack.cs
    // Description: Khay chứa Eye và Lock trên Timeline
    // Develope by : Nguyen Van Ngoc
    // History:
    // 08/03/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Khay chứa Eye và Lock trên Timeline
    /// </summary>
    public class RightTrack : Track
    {
        public static readonly DependencyProperty IconProperty;
        public static readonly DependencyProperty LeftProperty;

        static RightTrack()
        {
            IconProperty = DependencyProperty.Register("Icon", typeof(string), typeof(RightTrack), new PropertyMetadata(string.Empty));
            LeftProperty = DependencyProperty.Register("Left", typeof(double), typeof(RightTrack), new PropertyMetadata(0.0));
            IsSelectedProperty.OverrideMetadata(typeof(RightTrack), new FrameworkPropertyMetadata(false, new PropertyChangedCallback(OnIsSelectedPropertyChanged)));
        }

        private static void OnIsSelectedPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is RightTrack track && e.NewValue is bool isSelected)
            {
                track.UpdateMoveThumbAdorner(isSelected);
            }
        }

        /// <summary>
        /// Thêm/Xóa MoveThumbAdorner khi trạng thái IsSelected thay đổi
        /// </summary>
        /// <param name="isSelected"></param>
        private void UpdateMoveThumbAdorner(bool isSelected)
        {
            if (isSelected)
            {
                if (TimelineHelper.FindParent<TrackManager>(this) is TrackManager trackManager)
                {
                    if (TimelineHelper.FindParent<Timeline>(trackManager) is Timeline grid)
                    {
                        if (AdornerLayer.GetAdornerLayer(grid) is AdornerLayer layer)
                        {
                            MoveThumbAdorner = new MoveThumbAdorner(this)
                            {
                                DataContext = grid.DataContext
                            };
                            //layer.Add(MoveThumbAdorner);
                        }
                    }
                }
            }
            else if (MoveThumbAdorner != null)
            {
                if (TimelineHelper.FindParent<TrackManager>(this) is TrackManager trackManager)
                {
                    if (TimelineHelper.FindParent<Timeline>(trackManager) is Timeline grid)
                    {
                        if (AdornerLayer.GetAdornerLayer(grid) is AdornerLayer layer)
                        {
                            foreach (var item in (MoveThumbAdorner as MoveThumbAdorner).Visuals)
                            {
                                if (item is MoveThumbBase thumb)
                                {
                                    thumb.TimerScrollToLeft.Stop();
                                    thumb.TimerScrollToRight.Stop();
                                }
                            }
                            layer.Remove(MoveThumbAdorner);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Biểu tượng
        /// </summary>
        public string Icon
        {
            get { return (string)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }
        /// <summary>
        /// Lề trái
        /// </summary>
        public double Left
        {
            get { return (double)GetValue(LeftProperty); }
            set { SetValue(LeftProperty, value); }
        }

        /// <summary>
        /// Lưu MoveThumbAdorner
        /// </summary>
        public Adorner MoveThumbAdorner { get; private set; }
    }
}
