﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  TimeControl.cs
**
** Description: Lớp TimeControl là lớp có chức năng điều chỉnh thời gian
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;

namespace INV.Elearning.Timeline
{
    [TemplatePart(Name = "PART_Hour", Type = typeof(SpinnerControl))]
    [TemplatePart(Name = "PART_Minute", Type = typeof(SpinnerControl))]
    [TemplatePart(Name = "PART_Second", Type = typeof(SpinnerControl))]
    [TemplatePart(Name = "PART_Tick", Type = typeof(SpinnerControl))]
    /// <summary>
    /// Lớp TimeControl là lớp có chức năng điều chỉnh thời gian
    /// </summary>
    public class TimeControl : Control
    {
        /// <summary>
        /// Thuộc tính Time là thời gian hiện tại của control này
        /// </summary>
        public TimeSpan Time
        {
            get { return (TimeSpan)GetValue(TimeProperty); }
            set { SetValue(TimeProperty, value); }
        }

        /// <summary>
        /// DependencyProperty cho thuộc tính Time.
        /// </summary>
        public static readonly DependencyProperty TimeProperty =
            DependencyProperty.Register("Time", typeof(TimeSpan), typeof(TimeControl), new PropertyMetadata(TimeSpan.FromSeconds(0), OnTimeChanged));

        private static void OnTimeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TimeControl timeControl = d as TimeControl;
            SpinnerControl
                hourControl = timeControl._hourControl,
                minuteControl = timeControl._minuteControl,
                secondControl = timeControl._secondControl,
                tickControl = timeControl._tickControl;
            if (hourControl != null)
            {
                hourControl.Value = timeControl.Time.Hours;
            }
            if (minuteControl != null)
            {
                minuteControl.Value = timeControl.Time.Minutes;
            }
            if (secondControl != null)
            {
                secondControl.Value = timeControl.Time.Seconds;
            }
            if (tickControl != null)
            {
                tickControl.Value = timeControl.Time.Milliseconds / 100;
            }
            timeControl.TimeChanged?.Invoke(timeControl, new TimeChangedEventArgs((TimeSpan)e.OldValue, (TimeSpan)e.NewValue));
        }

        /// <summary>
        /// Sự kiện giá trị của thuộc tính <see cref="Time"/> thay đổi
        /// </summary>
        public event EventHandler<TimeChangedEventArgs> TimeChanged;

        /// <summary>
        /// Thuộc tính UpHourEnabled xác định trạng thái bật/tắt của nút tăng giá trị giờ
        /// </summary>
        public bool UpHourEnabled
        {
            get { return (bool)GetValue(UpHourEnabledProperty); }
            set { SetValue(UpHourEnabledProperty, value); }
        }

        /// <summary>
        /// DependencyProperty cho thuộc tính UpHourEnabled.
        /// </summary>
        public static readonly DependencyProperty UpHourEnabledProperty =
            DependencyProperty.Register("UpHourEnabled", typeof(bool), typeof(TimeControl), new PropertyMetadata(true));

        /// <summary>
        /// Thuộc tính DownHourEnabled xác định trạng thái bật/tắt của nút giảm giá trị giờ
        /// </summary>
        public bool DownHourEnabled
        {
            get { return (bool)GetValue(DownHourEnabledProperty); }
            set { SetValue(DownHourEnabledProperty, value); }
        }

        /// <summary>
        /// DependencyProperty cho thuộc tính DownHourEnabled.
        /// </summary>
        public static readonly DependencyProperty DownHourEnabledProperty =
            DependencyProperty.Register("DownHourEnabled", typeof(bool), typeof(TimeControl), new PropertyMetadata(true));

        /// <summary>
        /// Thuộc tính UpMinuteEnabled xác định trạng thái bật/tắt của nút tăng giá trị phút
        /// </summary>
        public bool UpMinuteEnabled
        {
            get { return (bool)GetValue(UpMinuteEnabledProperty); }
            set { SetValue(UpMinuteEnabledProperty, value); }
        }

        /// <summary>
        /// DependencyProperty cho thuộc tính UpMinuteEnabled.
        /// </summary>
        public static readonly DependencyProperty UpMinuteEnabledProperty =
            DependencyProperty.Register("UpMinuteEnabled", typeof(bool), typeof(TimeControl), new PropertyMetadata(true));

        /// <summary>
        /// Thuộc tính DownMinuteEnabled xác định trạng thái bật/tắt của nút giảm giá trị phút
        /// </summary>
        public bool DownMinuteEnabled
        {
            get { return (bool)GetValue(DownMinuteEnabledProperty); }
            set { SetValue(DownMinuteEnabledProperty, value); }
        }

        /// <summary>
        /// DependencyProperty cho thuộc tính DownMinuteEnabled.
        /// </summary>
        public static readonly DependencyProperty DownMinuteEnabledProperty =
            DependencyProperty.Register("DownMinuteEnabled", typeof(bool), typeof(TimeControl), new PropertyMetadata(true));

        /// <summary>
        /// Thuộc tính UpSecondEnabled xác định trạng thái bật/tắt của nút tăng giá trị giây
        /// </summary>
        public bool UpSecondEnabled
        {
            get { return (bool)GetValue(UpSecondEnabledProperty); }
            set { SetValue(UpSecondEnabledProperty, value); }
        }

        /// <summary>
        /// DependencyProperty cho thuộc tính UpSecondEnabled.
        /// </summary>
        public static readonly DependencyProperty UpSecondEnabledProperty =
            DependencyProperty.Register("UpSecondEnabled", typeof(bool), typeof(TimeControl), new PropertyMetadata(true));

        /// <summary>
        /// Thuộc tính DownSecondEnabled xác định trạng thái bật/tắt của nút giản giá trị giây
        /// </summary>
        public bool DownSecondEnabled
        {
            get { return (bool)GetValue(DownSecondEnabledProperty); }
            set { SetValue(DownSecondEnabledProperty, value); }
        }

        /// <summary>
        /// DependencyProperty cho thuộc tính DownSecondEnabled.
        /// </summary>
        public static readonly DependencyProperty DownSecondEnabledProperty =
            DependencyProperty.Register("DownSecondEnabled", typeof(bool), typeof(TimeControl), new PropertyMetadata(true));

        /// <summary>
        /// Thuộc tính UpTickEnabled xác định trạng thái bật/tắt của nút tăng giá trị tích
        /// </summary>
        public bool UpTickEnabled
        {
            get { return (bool)GetValue(UpTickEnabledProperty); }
            set { SetValue(UpTickEnabledProperty, value); }
        }

        /// <summary>
        /// DependencyProperty cho thuộc tính UpTickEnabled.
        /// </summary>
        public static readonly DependencyProperty UpTickEnabledProperty =
            DependencyProperty.Register("UpTickEnabled", typeof(bool), typeof(TimeControl), new PropertyMetadata(true));

        /// <summary>
        /// Thuộc tính DownTickEnabled xác định trạng thái bật/tắt của nút giảm giá trị tích
        /// </summary>
        public bool DownTickEnabled
        {
            get { return (bool)GetValue(DownTickEnabledProperty); }
            set { SetValue(DownTickEnabledProperty, value); }
        }

        /// <summary>
        /// DependencyProperty cho thuộc tính DownTickEnabled.
        /// </summary>
        public static readonly DependencyProperty DownTickEnabledProperty =
            DependencyProperty.Register("DownTickEnabled", typeof(bool), typeof(TimeControl), new PropertyMetadata(true));



        private SpinnerControl _hourControl, _minuteControl, _secondControl, _tickControl;
        public override void OnApplyTemplate()
        {
            _hourControl = GetTemplateChild("PART_Hour") as SpinnerControl;
            _minuteControl = GetTemplateChild("PART_Minute") as SpinnerControl;
            _secondControl = GetTemplateChild("PART_Second") as SpinnerControl;
            _tickControl = GetTemplateChild("PART_Tick") as SpinnerControl;

            if (_hourControl != null)
            {
                _hourControl.ValueChanged += (s, e) =>
                {
                    double hour = e.NewValue;
                    if (hour < 0)
                    {
                        _hourControl.Value = 0;
                    }
                    if (_hourControl.Value != e.OldValue)
                    {
                        Time = Time.Subtract(TimeSpan.FromHours(Time.Hours)).Add(TimeSpan.FromHours(_hourControl.Value));
                    }
                };
                SetBinding(_hourControl, SpinnerControl.DownEnabledProperty, "UpHourEnabled");
                SetBinding(_hourControl, SpinnerControl.DownEnabledProperty, "DownHourEnabled");
            }
            if (_minuteControl != null)
            {
                _minuteControl.ValueChanged += (s, e) =>
                      {
                          double minute = e.NewValue;
                          if (minute > 59)
                          {
                              _minuteControl.Value = 0;
                              _hourControl.Value++;
                          }
                          else if (minute < 0)
                          {
                              _minuteControl.Value = 59;
                              _hourControl.Value--;
                          }
                          if (_minuteControl.Value != e.OldValue)
                          {
                              Time = Time.Subtract(TimeSpan.FromMinutes(Time.Minutes)).Add(TimeSpan.FromMinutes(_minuteControl.Value));
                          }
                      };
                SetBinding(_minuteControl, SpinnerControl.DownEnabledProperty, "UpMinuteEnabled");
                SetBinding(_minuteControl, SpinnerControl.DownEnabledProperty, "DownMinuteEnabled");
            }
            if (_secondControl != null)
            {
                _secondControl.ValueChanged += (s, e) =>
                {
                    double second = e.NewValue;
                    if (second > 59)
                    {
                        _secondControl.Value = 0;
                        _minuteControl.Value++;
                    }
                    else if (second < 0)
                    {
                        _secondControl.Value = 59;
                        _minuteControl.Value--;
                    }
                    if (_secondControl.Value != e.OldValue)
                    {
                        Time = Time.Subtract(TimeSpan.FromSeconds(Time.Seconds)).Add(TimeSpan.FromSeconds(_secondControl.Value));
                    }
                };
                SetBinding(_secondControl, SpinnerControl.DownEnabledProperty, "UpSecondEnabled");
                SetBinding(_secondControl, SpinnerControl.DownEnabledProperty, "DownSecondEnabled");
            }
            if (_tickControl != null)
            {
                _tickControl.ValueChanged += (s, e) =>
                {
                    double tick = e.NewValue;
                    if (tick > 9)
                    {
                        _tickControl.Value = 0;
                        _secondControl.Value++;
                    }
                    else if (tick < 0)
                    {
                        _tickControl.Value = 9;
                        _secondControl.Value--;
                    }
                    if (_tickControl.Value != e.OldValue)
                    {
                        Time = Time.Subtract(TimeSpan.FromMilliseconds(Time.Milliseconds)).Add(TimeSpan.FromMilliseconds(_tickControl.Value * 100));
                    }
                };
                SetBinding(_tickControl, SpinnerControl.DownEnabledProperty, "UpTickEnabled");
                SetBinding(_tickControl, SpinnerControl.DownEnabledProperty, "DownTickEnabled");
            }
        }

        private void SetBinding(DependencyObject target, DependencyProperty dependencyProperty, string sourceProperty)
        {
            BindingOperations.SetBinding(target, dependencyProperty, new Binding(sourceProperty)
            {
                Source = this,
                Mode = BindingMode.TwoWay,
                UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
            });
        }
    }

    public class TimeChangedEventArgs : EventArgs
    {
        public TimeChangedEventArgs(TimeSpan oldValue, TimeSpan newValue)
        {
            OldValue = oldValue;
            NewValue = newValue;
        }

        public TimeSpan OldValue { get; set; }
        public TimeSpan NewValue { get; set; }
    }

    [TemplatePart(Name = "PART_Up", Type = typeof(RepeatButton))]
    [TemplatePart(Name = "PART_Down", Type = typeof(RepeatButton))]
    [TemplatePart(Name = "PART_Value", Type = typeof(TextBlock))]
    /// <summary>
    /// Lớp SpinnerControl là lớp có chức năng điều chỉnh số nguyên
    /// </summary>
    public class SpinnerControl : Control
    {
        /// <summary>
        /// Thuộc tính Value là giá trị số hiện tại
        /// </summary>
        public int Value
        {
            get { return (int)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        /// <summary>
        /// DependencyProperty cho thuộc tính Value.
        /// </summary>
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(int), typeof(SpinnerControl), new PropertyMetadata(0, OnValueChanged));

        private static void OnValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SpinnerControl spinner = d as SpinnerControl;
            spinner.ValueChanged?.Invoke(spinner, new ValueChangedEventArgs((int)e.OldValue, (int)e.NewValue));
        }

        /// <summary>
        /// Sự kiện giá trị thuộc tính <see cref="Value"/> thay đổi
        /// </summary>
        public event EventHandler<ValueChangedEventArgs> ValueChanged;

        /// <summary>
        /// Thuộc tính UpEnabled xác định trạng thái bật/tắt của nút tăng giá trị
        /// </summary>
        public bool UpEnabled
        {
            get { return (bool)GetValue(UpEnabledProperty); }
            set { SetValue(UpEnabledProperty, value); }
        }

        /// <summary>
        /// DependencyProperty cho thuộc tính UpEnabled.
        /// </summary>
        public static readonly DependencyProperty UpEnabledProperty =
            DependencyProperty.Register("UpEnabled", typeof(bool), typeof(SpinnerControl), new PropertyMetadata(true));

        /// <summary>
        /// Thuộc tính DownEnabled xác định trạng thái bật/tắt của nút giảm giá trị
        /// </summary>
        public bool DownEnabled
        {
            get { return (bool)GetValue(DownEnabledProperty); }
            set { SetValue(DownEnabledProperty, value); }
        }

        /// <summary>
        /// DependencyProperty cho thuộc tính DownEnabled.
        /// </summary>
        public static readonly DependencyProperty DownEnabledProperty =
            DependencyProperty.Register("DownEnabled", typeof(bool), typeof(SpinnerControl), new PropertyMetadata(true));


        private RepeatButton _upButton, _downButton;
        private TextBlock _textBlock;
        public override void OnApplyTemplate()
        {
            _upButton = GetTemplateChild("PART_Up") as RepeatButton;
            _downButton = GetTemplateChild("PART_Down") as RepeatButton;
            _textBlock = GetTemplateChild("PART_Value") as TextBlock;

            if (_upButton != null)
            {
                _upButton.Click += (s, e) => Value++;
                BindingOperations.SetBinding(_upButton, IsEnabledProperty, new Binding("UpEnabled")
                {
                    Source = this,
                    Mode = BindingMode.TwoWay,
                    UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
                });
            }
            if (_downButton != null)
            {
                _downButton.Click += (s, e) => Value--;
                BindingOperations.SetBinding(_downButton, IsEnabledProperty, new Binding("DownEnabled")
                {
                    Source = this,
                    Mode = BindingMode.TwoWay,
                    UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
                });
            }
            if (_textBlock != null)
            {
                BindingOperations.SetBinding(_textBlock, TextBlock.TextProperty, new Binding("Value")
                {
                    Source = this,
                    Mode = BindingMode.OneWay,
                    UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
                });
            }
        }
    }

    /// <summary>
    /// Lớp ValueChangedEventArgs là đối số cho sự kiện <see cref="SpinnerControl.ValueChanged"/>
    /// </summary>
    public class ValueChangedEventArgs : EventArgs
    {
        public ValueChangedEventArgs(int oldValue, int newValue)
        {
            OldValue = oldValue;
            NewValue = newValue;
        }

        public int OldValue { get; set; }
        public int NewValue { get; set; }
    }
}
