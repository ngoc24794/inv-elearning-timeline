﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  FileName.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System.Windows;
using System.Windows.Controls;

namespace INV.Elearning.Timeline
{
    public partial class TimelineResourceDictionary : ResourceDictionary
    {
        public void GridSizeChanged(object sender, SizeChangedEventArgs e)
        {
            Grid grid = sender as Grid;
            NameBox box = (NameBox)grid.Children[2];
            box.MaxWidth = e.NewSize.Width - 90;
        }
    }
}
