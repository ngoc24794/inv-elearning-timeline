﻿using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: CuePointMenuItem.cs
    // Description: Mẫu dữ liệu cho ContextMenu của MoveThumb
    // Develope by : Nguyen Van Ngoc
    // History:
    // 22/03/2018 : Modifier
    //---------------------------------------------------------------------------
    /// <summary>
    /// Mẫu dữ liệu cho ContextMenu của MoveThumb
    /// </summary>
    public class CuePointMenuItem : ButtonBase
    {
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
        }
    }
}
