﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: RulerThumb.cs
    // Description: Các nút trên thước dùng để tạo vùng chọn thời gian được sử dụng để Preview trang trình chiếu
    // Develope by : Nguyen Van Ngoc
    // History:
    // 22/03/2018 : Modifier
    //---------------------------------------------------------------------------
    /// <summary>
    /// Các nút trên thước dùng để tạo vùng chọn thời gian được sử dụng để Preview trang trình chiếu
    /// </summary>
    public abstract class RulerThumb : RootViewModel
    {
        private double _x;

        public RulerThumb()
        {
            Width = Height = 16;
        }

        #region Properites
        /// <summary>
        /// Độ rộng
        /// </summary>
        public double Width { get; protected set; }
        /// <summary>
        /// Độ cao
        /// </summary>
        public double Height { get; protected set; }
        /// <summary>
        /// Thời gian mà thumb nắm giữ khi nó đang ở một vị trí trên thước thời gian (RulerTimeline)
        /// </summary>
        public double Time { get; set; }

        /// <summary>
        /// Tọa độ của thumb trên thước
        /// </summary>
        public double X
        {
            get => _x; set
            {
                _x = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Lề trái
        /// </summary>
        public abstract double Left { get; }

        /// <summary>
        /// Lề phải
        /// </summary>
        public abstract double Right { get; }

        /// <summary>
        /// Biểu tượng
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// Trạng thái Ẩn/Hiện
        /// </summary>
        public bool IsVisible { get; set; }

        /// <summary>
        /// Trạng thái được nhấn
        /// </summary>
        public bool IsHitted { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Di chuyển đến tọa độ x
        /// </summary>
        /// <param name="x"></param>
        public void MoveTo(double x)
        {
            X = x;
        }

        /// <summary>
        /// Xác định trạng thái được nhấn
        /// </summary>
        /// <param name="mouseX"></param>
        public void GetHit(double mouseX)
        {
            IsHitted = mouseX >= Left && mouseX <= Right;
        }

        /// <summary>
        /// Nhấn thumb này
        /// </summary>
        public void HitMe()
        {
            IsHitted = true;
        }

        /// <summary>
        /// Thả thumb này
        /// </summary>
        public void ReleaseMe()
        {
            IsHitted = false;
        }

        /// <summary>
        /// Vẽ bề mặt cho thumb theo Icon
        /// </summary>
        /// <param name="dc"></param>
        /// <param name="containerHeight"></param>
        public virtual void Draw(DrawingContext dc, double containerHeight)
        {
            var bitmap = new BitmapImage(new Uri(Icon, UriKind.Absolute));
            var cropBitmap = new CroppedBitmap(bitmap, new Int32Rect(0, 0, 32, 32));
            dc.DrawImage(cropBitmap, new Rect(Left, containerHeight - Height, Width, Height));
        }
        #endregion
    }

    /// <summary>
    /// Nút bắt đầu
    /// </summary>
    public class StartThumb : RulerThumb
    {
        public override double Left { get => X - Width; }
        public override double Right { get => X; }
    }

    /// <summary>
    /// Nút chính giữa
    /// </summary>
    public class HeadThumb : RulerThumb
    {
        public override double Left { get => X - Width / 2; }
        public override double Right { get => X + Width / 2; }

        public override void Draw(DrawingContext dc, double containerHeight)
        {
            base.Draw(dc, containerHeight);


            //---------------------------------------------------------------------------
            // Trình bày thêm thời gian trên đầu thumb này
            //---------------------------------------------------------------------------
            dc.DrawRectangle(Brushes.Orange, null, new Rect(X + 25, 5, 50, 12));
            TimeSpan time = TimeSpan.FromSeconds(Time);
            dc.DrawText(new FormattedText($"{time.Minutes}:{time.Seconds}:{time.Milliseconds}", CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface(""), 11, Brushes.White) { TextAlignment = TextAlignment.Center }, new Point(X + 50, 6));
        }
    }

    /// <summary>
    /// Nút cuối 
    /// </summary>
    public class EndThumb : RulerThumb
    {
        public override double Left { get => X; }
        public override double Right { get => X + Width; }
    }
}
