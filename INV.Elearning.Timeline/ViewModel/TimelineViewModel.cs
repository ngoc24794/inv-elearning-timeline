﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Timeline;
using INV.Elearning.Core.View;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace INV.Elearning.Timeline
{
    public class TimelineViewModel : RootViewModel
    {

        public TimelineViewModel(Timeline timeline)
        {
            Timeline = timeline;
            Ruler = new RulerViewModel(this)
            {
                Ruler = new RulerModel()
                {
                    MinimummScale = 5,
                    MaximumScale = 30,
                    ScaleTime = 0.1,
                }
            };
            Ruler.UpdateProperties();
            Timer = new TimerViewModel(this);
            CuePoint = new CuePointViewModel(this);
            Track = new TrackViewModel(this);
        }

        public void OnCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            UpdateTracks(false);
        }

        public Timeline Timeline { get; set; }
        public RulerViewModel Ruler { get; set; }
        public TimerViewModel Timer { get; set; }
        public TrackViewModel Track { get; set; }
        public CuePointViewModel CuePoint { get; set; }
        internal void UpdateObjectElements()
        {
            foreach (ObjectElement item in GetObjectElements(false))
            {
                if (item.Timing != null)
                {
                    item.Timing.LeftTrack = Ruler.CalculatePixel(item.Timing.StartTime);
                    item.Timing.WidthTrack = Ruler.CalculatePixel(item.Timing.Duration);
                }
            }
        }

        internal void UpdateTracks(bool ignoreGroup = true)
        {
            //---------------------------------------------------------------------------
            // Cập nhật lại View và dữ liệu cho các khay
            //---------------------------------------------------------------------------
            List<ObjectElement> objectElements = GetObjectElements(ignoreGroup);
            foreach (ObjectElement item in objectElements)
            {
                if (item.Timing != null)
                {
                    if (item.Timing.ShowAlways)
                    {
                        item.Timing.StartTime = 0;
                        item.Timing.LeftTrack = 0;
                        item.Timing.Duration = Ruler.TotalTime;
                        item.Timing.WidthTrack = Ruler.TotalTimePixel;
                    }
                    else if (item.Timing.ShowUntilEnd)
                    {
                        item.Timing.Duration = Ruler.TotalTime - item.Timing.StartTime;
                        item.Timing.WidthTrack = Ruler.TotalTimePixel - item.Timing.LeftTrack;
                    }
                }
            }
        }

        public List<ObjectElement> GetObjectElements(bool ignoreGroup = true)
        {
            List<ObjectElement> objectElements = new List<ObjectElement>();

            if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Elements != null)
            {
                foreach (ObjectElement item in (Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Elements)
                {
                    //if (item is IGroupContainer group)
                    //{
                    //    if (!ignoreGroup)
                    //    {
                    //        objectElements.Add(item);
                    //    }
                    //    GetChilds(group, objectElements, ignoreGroup);
                    //}
                    //else
                    //{
                    //    objectElements.Add(item);
                    //}
                    objectElements.Add(item);
                }
            }

            return objectElements;
        }

        private void GetChilds(IGroupContainer group, List<ObjectElement> output, bool ignoreGroup)
        {
            foreach (ObjectElement item in group.Elements)
            {
                if (item is IGroupContainer groupContainer)
                {
                    if (!ignoreGroup)
                    {
                        output.Add(item);
                    }
                    GetChilds(groupContainer, output, ignoreGroup);
                }
                else
                {
                    output.Add(item);
                }
            }
        }
        /// <summary>
        /// Cập nhật lại Timeline khi thay đổi Layer
        /// </summary>
        public void UpdateTimeline()
        {
            UpdateObjectElements();
            UpdateTracks(false);

            if (CuePoint != null && CuePoint.CuePoints != null)
            {
                foreach (CuePointModel item in CuePoint.CuePoints)
                {
                    item.TimePixel = Ruler.CalculatePixel(item.Time);
                }
            }

            Ruler?.UpdateProperties();
        }

        public void UpdateTimeline(LayoutBase layer)
        {
            if (layer?.Elements != null)
            {
                foreach (ObjectElement item in layer.Elements)
                {
                    if (item.Timing != null)
                    {
                        item.Timing.LeftTrack = Ruler.CalculatePixel(item.Timing.StartTime);
                        item.Timing.WidthTrack = Ruler.CalculatePixel(item.Timing.Duration);
                    }
                }
            }

            if (layer?.CuePoints != null)
            {
                foreach (var item in layer.CuePoints)
                {
                    if (item is CuePointModel cuePoint)
                    {
                        cuePoint.TimePixel = Ruler.CalculatePixel(item.Time);
                    }
                }
            }

            UpdateTracks(false);
            if (layer != null && layer.Timing != null && Ruler != null)
            {
                Ruler.TotalTimePixel = layer.Timing.TotalTime / 0.1 * layer.Timing.ScaleRuler;
            }
        }

        public void UpdateTimeline(SlideBase slide)
        {
            if (slide.Layouts != null)
            {
                foreach (var item in slide.Layouts)
                {
                    UpdateTimeline(item);
                }
            }
        }

        #region ToggleShowAllCommand
        private RelayCommand _toggleShowAllCommand;
        /// <summary>
        /// Lệnh bật/tắt trạng thái Ẩn/Hiện
        /// </summary>
        public RelayCommand ToggleShowAllCommand
        {
            get { return _toggleShowAllCommand ?? (_toggleShowAllCommand = new RelayCommand(x => ToggleShowAll(x), c => true)); }

        }

        private void ToggleShowAll(object x)
        {
            if (x is bool isShowAll)
            {
                foreach (ObjectElement item in GetObjectElements(false))
                {
                    //item.Visibility = isShowAll ? Visibility.Visible : Visibility.Hidden;
                    item.IsShow = isShowAll;
                }
            }
        }
        #endregion

        #region ToggleLockAllCommand
        private RelayCommand _toggleLockAllCommand;

        /// <summary>
        /// Lệnh bật/tắt trạng thái Locked
        /// </summary>
        public RelayCommand ToggleLockAllCommand
        {
            get { return _toggleLockAllCommand ?? (_toggleLockAllCommand = new RelayCommand(x => ToggleLockAll(x), c => true)); }

        }

        private void ToggleLockAll(object x)
        {
            if (x is bool isLockAll)
            {
                foreach (ObjectElement item in GetObjectElements(false))
                {
                    item.Locked = isLockAll;
                }
            }
        }
        #endregion

        #region MoveUpCommand
        private ICommand _moveUpCommand;
        public ICommand MoveUpCommand
        {
            get => _moveUpCommand ?? (_moveUpCommand = new RelayCommand(x => MoveUp(), p => MoveUpCanExecute()));
        }

        private void MoveUp()
        {
            if ((Application.Current as IAppGlobal).SelectedItem is ObjectElement selectedItem)
            {
                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Elements is ObservableCollection<ObjectElement> objects)
                {
                    int selectedIndex = objects.IndexOf(selectedItem);
                    if (selectedIndex > 0)
                    {
                        Global.BeginInit();
                        objects.RemoveAt(selectedIndex);
                        objects.Insert(selectedIndex - 1, selectedItem);
                        Global.EndInit();
                        Global.PushUndo(new MoveUpStep<ObjectElement>(objects, selectedItem, selectedIndex, selectedIndex - 1));
                    }
                }
            }
        }

        private bool MoveUpCanExecute()
        {
            // Không cho di chuyển thi có nhiều/ít hơn 1 phần tử được chọn
            if ((Application.Current as IAppGlobal).SelectedElements?.Count != 1)
            {
                return false;
            }

            if ((Application.Current as IAppGlobal).SelectedItem is ObjectElement selectedItem)
            {
                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Elements is ObservableCollection<ObjectElement> objects)
                {
                    int selectedIndex = objects.IndexOf(selectedItem);

                    // Chỉ cho phép di chuyển khi danh sách có ít nhất 2 phần tử và phần tử được chọn không phải là phần tử đầu tiên
                    return objects.Count > 1 && selectedIndex > 0;
                }
            }
            return false;
        }
        #endregion

        #region MoveDownCommand
        private ICommand _moveDownCommand;
        public ICommand MoveDownCommand
        {
            get => _moveDownCommand ?? (_moveDownCommand = new RelayCommand(x => MoveDown(), p => MoveDownCanExecute()));
        }

        private void MoveDown()
        {
            if ((Application.Current as IAppGlobal).SelectedItem is ObjectElement selectedItem)
            {
                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Elements is ObservableCollection<ObjectElement> objects)
                {
                    int selectedIndex = objects.IndexOf(selectedItem);
                    if (selectedIndex < objects.Count - 1)
                    {
                        Global.BeginInit();
                        objects.RemoveAt(selectedIndex);
                        objects.Insert(selectedIndex + 1, selectedItem);
                        Global.EndInit();
                        Global.PushUndo(new MoveDownStep<ObjectElement>(objects, selectedItem, selectedIndex, selectedIndex + 1));
                    }
                }
            }
        }

        private bool MoveDownCanExecute()
        {
            // Không cho di chuyển thi có nhiều/ít hơn 1 phần tử được chọn
            if ((Application.Current as IAppGlobal).SelectedElements?.Count != 1)
            {
                return false;
            }

            if ((Application.Current as IAppGlobal).SelectedItem is ObjectElement selectedItem)
            {
                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Elements is ObservableCollection<ObjectElement> objects)
                {
                    int selectedIndex = objects.IndexOf(selectedItem);

                    // Chỉ cho phép di chuyển khi danh sách có ít nhất 2 phần tử và phần tử được chọn không phải là phần tử cuối cùng
                    return objects.Count > 1 && selectedIndex < objects.Count - 1;
                }
            }
            return false;
        }
        #endregion

        #region EscCommand
        private ICommand _escCommand;
        public ICommand EscCommand
        {
            get => _escCommand ?? (_escCommand = new RelayCommand(x =>
            Ruler.IsShowThumbs = false, p => true));
        }
        #endregion

        #region HomeCommand
        private ICommand _homeCommand;
        public ICommand HomeCommand
        {
            get => _homeCommand ?? (_homeCommand = new RelayCommand(x =>
            {
                Ruler.HeadTime = Ruler.LeftTime = Ruler.RightTime = Ruler.HeadTimePixel = Ruler.LeftTimePixel = Ruler.RightTimePixel = 0;
                Timeline.RightTrackManagerScrollViewer.ScrollToHome();
            }, c => true));
        }
        #endregion

        #region EndCommand
        private ICommand _endCommand;
        public ICommand EndCommand
        {
            get => _endCommand ?? (_endCommand = new RelayCommand(x =>
            {
                Ruler.HeadTime = Ruler.LeftTime = Ruler.RightTime = Ruler.TotalTime;
                Ruler.HeadTimePixel = Ruler.LeftTimePixel = Ruler.RightTimePixel = Ruler.TotalTimePixel;
                Timeline.RightTrackManagerScrollViewer.ScrollToEnd();
            }, c => true));
        }
        #endregion
    }
}
