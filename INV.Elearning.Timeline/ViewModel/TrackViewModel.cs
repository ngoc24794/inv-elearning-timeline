﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Windows;

namespace INV.Elearning.Timeline
{
    public class TrackViewModel
    {
        public TimelineViewModel TimelineViewModel { get; set; }
        public ObjectElement CurrentTrack { get; set; }

        public TrackViewModel(TimelineViewModel timelineViewModel)
        {
            TimelineViewModel = timelineViewModel;
            CurrentTrack = new ObjectElement();
        }
        public TrackViewModel()
        {

        }

        #region ShowUntilEndCommand
        private RelayCommand _showUntilEndCommand;
        /// <summary>
        /// Lệnh bật/tắt ShowUntiTolEnd
        /// </summary>
        public RelayCommand ShowUntilEndCommand
        {
            get { return _showUntilEndCommand ?? (_showUntilEndCommand = new RelayCommand(x => { ShowUntilEnd(); }, c => CurrentTrack?.Timing?.ShowAlways != true)); }

        }

        private void ShowUntilEnd()
        {
            if (CurrentTrack.Timing != null)
            {
                Toggle((item) =>
                {
                    if (item.IsSelected)
                    {
                        item.Timing.ShowUntilEnd = !item.Timing.ShowUntilEnd;
                    }
                });
            }
        }
        #endregion

        #region ShowAlwaysCommand
        private RelayCommand _showAlwaysCommand;
        /// <summary>
        /// Lệnh bật/tắt thuộc tính ShowAlways
        /// </summary>
        public RelayCommand ShowAlwaysCommand
        {
            get { return _showAlwaysCommand ?? (_showAlwaysCommand = new RelayCommand(x => { ShowAlways(); }, c => true)); }

        }

        private void ShowAlways()
        {
            if (CurrentTrack.Timing != null)
            {
                Toggle((item) =>
                {
                    if (item.IsSelected)
                    {
                        item.Timing.ShowAlways = !item.Timing.ShowAlways;
                    }
                });
            }
        }
        #endregion

        #region Methods
        public void Toggle(Action<ObjectElement> action)
        {
            List<ToggleState> states = new List<ToggleState>();
            List<ObjectElement> objectElements = TimelineViewModel.GetObjectElements();
            foreach (ObjectElement item in objectElements)
            {
                if (item.Timing != null)
                {
                    ToggleState toggleState = new ToggleState(item.Timing);
                    toggleState.SetOldValue();

                    action(item);

                    if (item.Timing.ShowAlways)
                    {
                        item.Timing.StartTime = 0;
                        item.Timing.LeftTrack = 0;
                        item.Timing.Duration = TimelineViewModel.Ruler.TotalTime;
                        item.Timing.WidthTrack = TimelineViewModel.Ruler.TotalTimePixel;
                    }
                    else if (item.Timing.ShowUntilEnd)
                    {
                        item.Timing.Duration = TimelineViewModel.Ruler.TotalTime - item.Timing.StartTime;
                        item.Timing.WidthTrack = TimelineViewModel.Ruler.TotalTimePixel - item.Timing.LeftTrack;
                    }
                    toggleState.SetNewValue();
                    states.Add(toggleState);
                }
            }
            Global.PushUndo(new ToggleStep(states, TimelineViewModel.Ruler));
        }
        #endregion

        #region AlignToPlayheadCommand
        private RelayCommand _alignToPlayheadCommand;
        /// <summary>
        /// Lệnh gióng theo điểm đầu
        /// </summary>
        public RelayCommand AlignToPlayheadCommand
        {
            get { return _alignToPlayheadCommand ?? (_alignToPlayheadCommand = new RelayCommand(x => AlignToPlayhead(), c => true)); }

        }

        private void AlignToPlayhead()
        {
            double
                headTimePixel = TimelineViewModel.Ruler.HeadTimePixel,
                totalTime = 0,
                scaleTime = TimelineViewModel.Ruler.ScaleTime;


            List<TrackState> trackStates = new List<TrackState>();
            foreach (ObjectElement item in TimelineViewModel.GetObjectElements())
            {
                TrackState trackState = new TrackState(item.Timing);
                trackState.SetOldValue();
                trackStates.Add(trackState);

                if (item.IsSelected)
                {
                    if (item.Timing.ShowUntilEnd)
                    {
                        if (headTimePixel < item.Timing.LeftTrack)
                        {
                            item.Timing.WidthTrack += 2 * Math.Abs(headTimePixel - item.Timing.LeftTrack);
                        }
                        else
                        {
                            item.Timing.WidthTrack -= Math.Abs(headTimePixel - item.Timing.LeftTrack);
                        }
                        item.Timing.Duration = TimelineViewModel.Ruler.CalculateTime(item.Timing.WidthTrack);
                    }

                    item.Timing.StartTime = TimelineViewModel.Ruler.CalculateTime(headTimePixel);
                    item.Timing.LeftTrack = headTimePixel;
                }



                if (item.Timing.StartTime + item.Timing.Duration > TimelineViewModel.Ruler.TotalTime)
                {
                    totalTime = item.Timing.StartTime + item.Timing.Duration;
                }

                item.Timing.ShowAlways = false;
                trackState.SetNewValue();
            }

            RulerState rulerState = new RulerState(TimelineViewModel.Ruler);
            rulerState.SetOldState();
            if (totalTime > 0)
            {
                TimelineViewModel.Ruler.Ruler.TotalTime = totalTime;
                TimelineViewModel.Ruler.TotalTimePixel = TimelineViewModel.Ruler.CalculatePixel(TimelineViewModel.Ruler.Ruler.TotalTime);
            }
            rulerState.SetNewState();

            TimelineViewModel.UpdateTracks();
            TimelineViewModel.UpdateObjectElements();

            Global.PushUndo(new AlignToHeadlineStep(trackStates, rulerState, TimelineViewModel.UpdateTracks, TimelineViewModel.UpdateObjectElements));
        }
        #endregion

        #region ShowTimingWindowCommand
        private RelayCommand _showTimingWindowCommand;
        /// <summary>
        /// Lệnh hiển thị hộp thoại Timing
        /// </summary>
        public RelayCommand ShowTimingWindowCommand
        {
            get { return _showTimingWindowCommand ?? (_showTimingWindowCommand = new RelayCommand(x => ShowTimingWindow(x), c => true)); }

        }

        private void ShowTimingWindow(object x)
        {
            if (x is TrackViewModel trackViewModel)
            {
                TimingWindow timingWindow = new TimingWindow();
                if (Application.Current?.Windows.Count > 1)
                {
                    timingWindow.Owner = Application.Current.Windows[Application.Current.Windows.Count - 2];
                }
                TimingViewModel timingViewModel = new TimingViewModel()
                {
                    TrackViewModel = trackViewModel,
                    OldTotalTime = trackViewModel.TimelineViewModel.Ruler.TotalTime
                };

                timingWindow.DataContext = timingViewModel;

                if (trackViewModel.CurrentTrack != null && trackViewModel.CurrentTrack.Timing != null)
                {
                    TimelineViewModel viewModel = TimelineViewModel;
                    // Khởi tạo và ghi trạng thái cũ cho Ruler
                    RulerState rulerState = new RulerState(viewModel.Ruler);
                    rulerState.SetOldState();

                    // Khởi tạo và ghi trạng thái cũ cho Timing
                    List<TimingState> timingStates = new List<TimingState>();
                    List<ObjectElement> objectElements = viewModel.GetObjectElements(false);
                    if (objectElements != null)
                    {
                        foreach (ObjectElement item in objectElements)
                        {
                            if (item.IsSelected && item.Timing != null)
                            {
                                TimingState timingState = new TimingState(item.Timing);
                                timingState.SetOldState();
                                timingStates.Add(timingState);
                            }
                        }
                    }

                    timingWindow.ShowDialog();

                    // Ghi giá trị mới cho Ruler
                    rulerState.SetNewState();
                    // Ghi giá trị mới cho Timing
                    foreach (TimingState state in timingStates)
                    {
                        state.SetNewState();
                    }
                    Global.PushUndo(new ThumbDragStep(timingStates, rulerState, viewModel.UpdateTracks));
                }
            }
        }
        #endregion
    }
}
