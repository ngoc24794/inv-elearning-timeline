﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Timeline;
using INV.Elearning.Core.View;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: CuePointViewModel.cs
    // Description: Điều khiển truy xuất dữ liệu trên CuePointModel
    // Develope by : Nguyen Van Ngoc
    // History:
    // 22/03/2018 : Modifier
    //---------------------------------------------------------------------------
    /// <summary>
    /// Điều khiển truy xuất dữ liệu trên CuePointModel
    /// </summary>
    public class CuePointViewModel : RootViewModel
    {
        public TimelineViewModel TimelineViewModel { get; set; }
        public CuePointFactory Factory { get; private set; }
        public CuePointModel CurrentCuePoint { get; set; }
        public CuePointViewModel(TimelineViewModel timelineViewModel)
        {
            TimelineViewModel = timelineViewModel;
            Factory = new CuePointFactory();
        }

        public ObservableCollection<CuePointModel> CuePoints
        {
            get => (Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.CuePoints;
        }

        #region Commands

        #region CreateCuePointToPlayheadCommand
        private RelayCommand _createCuePointToPlayheadCommand;

        public RelayCommand CreateCuePointToPlayheadCommand
        {
            get { return _createCuePointToPlayheadCommand ?? (_createCuePointToPlayheadCommand = new RelayCommand(x => CreateCuePointToPlayhead(), c => true)); }
        }

        private void CreateCuePointToPlayhead()
        {
            double headTime = TimelineViewModel.Ruler.HeadTime;
            CuePointModel cuePoint = Factory.CreateCuePoint(headTime);
            cuePoint.TimePixel = TimelineViewModel.Ruler.CalculatePixel(headTime);
            CuePoints.Add(cuePoint);
            Global.PushUndo(new AddStep<CuePointModel>(CuePoints, cuePoint));
        }
        #endregion

        #region DeleteCuePointsCommand
        private RelayCommand _deleteCuePointsCommand;

        public RelayCommand DeleteCuePointsCommand
        {
            get { return _deleteCuePointsCommand ?? (_deleteCuePointsCommand = new RelayCommand(x => DeleteCuePoints(), c => true)); }
        }

        private void DeleteCuePoints()
        {
            IList<CuePointModel> backupList = new List<CuePointModel>();
            foreach (CuePointModel item in CuePoints)
            {
                backupList.Add(item);
            }
            CuePointFactory.Reset();
            CuePoints.Clear();
            Global.PushUndo(new RemoveAllStep<CuePointModel>(CuePoints, backupList));
        }
        #endregion

        #region DeleteCuePointCommand
        private RelayCommand _deleteCuePointCommand;

        public RelayCommand DeleteCuePointCommand
        {
            get { return _deleteCuePointCommand ?? (_deleteCuePointCommand = new RelayCommand(x => DeleteCuePoint(), c => true)); }
        }

        private void DeleteCuePoint()
        {
            int currentIndex = CuePoints.IndexOf(CurrentCuePoint);

            if (currentIndex > -1)
            {
                CuePoints.Remove(CurrentCuePoint);
                Global.PushUndo(new RemoveStep<CuePointModel>(CuePoints, CurrentCuePoint, currentIndex));
            }
        }
        #endregion

        #region AlignToCuePointCommand
        private RelayCommand _alignToCuePointCommand;
        /// <summary>
        /// Gióng khay theo điểm này
        /// </summary>
        public RelayCommand AlignToCuePointCommand
        {
            get { return _alignToCuePointCommand ?? (_alignToCuePointCommand = new RelayCommand(x => AlignToCuePoint(x), c => true)); }
        }

        private void AlignToCuePoint(object x)
        {
            if (x is CuePointModel cuePoint)
            {
                double
                headTimePixel = cuePoint.TimePixel,
                totalTime = 0,
                scaleTime = TimelineViewModel.Ruler.ScaleTime;

                List<TrackState> trackStates = new List<TrackState>();
                foreach (ObjectElement item in TimelineViewModel.GetObjectElements())
                {
                    TrackState trackState = new TrackState(item.Timing);
                    trackState.SetOldValue();
                    trackStates.Add(trackState);

                    if (item.IsSelected && item.Timing != null)
                    {

                        if (item.Timing.ShowUntilEnd)
                        {
                            if (headTimePixel < item.Timing.LeftTrack)
                            {
                                item.Timing.WidthTrack += 2 * Math.Abs(headTimePixel - item.Timing.LeftTrack);
                            }
                            else
                            {
                                item.Timing.WidthTrack -= Math.Abs(headTimePixel - item.Timing.LeftTrack);
                            }
                            item.Timing.Duration = TimelineViewModel.Ruler.CalculateTime(item.Timing.WidthTrack);
                        }

                        item.Timing.StartTime = TimelineViewModel.Ruler.CalculateTime(headTimePixel);
                        item.Timing.LeftTrack = headTimePixel;

                    }



                    if (item.Timing.StartTime + item.Timing.Duration > TimelineViewModel.Ruler.TotalTime)
                    {
                        totalTime = item.Timing.StartTime + item.Timing.Duration;
                    }

                    item.Timing.ShowAlways = false;
                    trackState.SetNewValue();
                }


                RulerState rulerState = new RulerState(TimelineViewModel.Ruler);
                rulerState.SetOldState();
                if (totalTime > 0)
                {
                    TimelineViewModel.Ruler.TotalTime = totalTime;
                    TimelineViewModel.Ruler.TotalTimePixel = TimelineViewModel.Ruler.TimeToPixel(TimelineViewModel.Ruler.TotalTime);
                }
                rulerState.SetNewState();

                TimelineViewModel.UpdateTracks();
                TimelineViewModel.UpdateObjectElements();

                Global.PushUndo(new AlignToHeadlineStep(trackStates, rulerState, TimelineViewModel.UpdateTracks, TimelineViewModel.UpdateObjectElements));
            }
        }
        #endregion


        #endregion
    }
}
