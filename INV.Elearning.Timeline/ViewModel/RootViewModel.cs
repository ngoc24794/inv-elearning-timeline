﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: RootViewModel.cs
    // Description: Lớp tạo ViewModel có Notify đến Binding Target
    // Develope by : Nguyen Van Ngoc
    // History:
    // 22/03/2018 : Modifier
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp tạo ViewModel có Notify đến Binding Target
    /// </summary>
    public abstract class RootViewModel : INotifyPropertyChanged
    {
        #region . Interface Implementation .
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
