﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace INV.Elearning.Timeline
{
    public class RelayCommand:ICommand
    {
        private Action<object> execute;
        private Action<object, IInputElement> executeWithTarget;
        private Func<object, bool> canExecute;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public RelayCommand(Action<object> execute, Func<object, bool> canExecute = null)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public RelayCommand(Action<object, IInputElement> execute, Func<object, bool> canExecute = null)
        {
            this.executeWithTarget = execute;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return this.canExecute == null || this.canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            this.execute(parameter);
        }

        public void Execute(object parameter, IInputElement target)
        {
            this.executeWithTarget(parameter, target);
        }
    }
}
