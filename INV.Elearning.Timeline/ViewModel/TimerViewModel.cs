﻿using INV.Elearning.Controls.Audio;
using INV.Elearning.PreView.ViewModel;
using System;
using System.Windows.Input;

namespace INV.Elearning.Timeline
{
    public class TimerViewModel : RootViewModel
    {
        public TimelineViewModel TimelineViewModel { get; set; }
        public static bool IsRecording { get; set; }
        public TimerViewModel(TimelineViewModel timelineViewModel)
        {
            TimelineViewModel = timelineViewModel;
            Timer = new TimerTimeline
            {
                Interval = 0.2
            };
            Timer.Tick += OnTimerTick;
        }

        #region Quản lí Preivew
        public TimerTimeline Timer { get; set; }

        /// <summary>
        /// Xử lí Preview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnTimerTick(object sender, TimerTimelineEventArgs e)
        {
            if (TimelineViewModel.Ruler != null)
            {
                double headTime = e.StartTime + e.ElapsedTime;
                if (headTime < e.EndTime)
                {
                    TimelineViewModel.Ruler.HeadTime = headTime;
                }
                else
                {
                    TimelineViewModel.Ruler.HeadTime = e.EndTime;
                    StopCommand.Execute(null);
                }
                if (Math.Abs(TimelineViewModel.Ruler.RightTime - TimelineViewModel.Ruler.LeftTime) < 0.1)
                {
                    TimelineViewModel.Ruler.LeftTime = TimelineViewModel.Ruler.RightTime = TimelineViewModel.Ruler.HeadTime;
                }
                TimelineViewModel.Ruler.UpdateProperties();
            }

        }

        private bool _isPlaying = false;

        public bool IsPlaying
        {
            get { return _isPlaying; }
            set { _isPlaying = value; OnPropertyChanged(); }
        }
        private double _startTimeBackup;
        private RelayCommand _playCommand;
        /// <summary>
        /// Lệnh Play Preview
        /// </summary>
        public RelayCommand PlayCommand
        {
            get { return _playCommand ?? (_playCommand = new RelayCommand(x => PlayExcute(), c => true)); }
        }

        private void PlayExcute()
        {

            if (TimelineViewModel.Ruler != null && Timer != null)
            {
                TimelineViewModel.Ruler.IsShowThumbs = true;
                double
                    startTime = TimelineViewModel.Ruler.LeftTime,
                    endTime = TimelineViewModel.Ruler.RightTime,
                    toltalTime = TimelineViewModel.Ruler.TotalTime,
                    scaleTime = TimelineViewModel.Ruler.ScaleTime;

                if (startTime == endTime)
                {
                    //startTime = 0;
                    //endTime = toltalTime;
                }

                if (Math.Abs(startTime - endTime) < scaleTime)
                {
                    if (!(startTime > 0))
                    {
                        startTime = 0;
                    }

                    endTime = toltalTime;
                }

                if (IsPlaying && IsStopped)
                {
                    Timer.StartTime = startTime;
                }
                Timer.EndTime = endTime;

                if (IsStopped) // resume
                {
                    TimelineViewModel.Ruler.HeadTime = startTime;
                }

                TimelineViewModel.Ruler.UpdateProperties();

                IsFreeThumb = false;
                if (IsPlaying)
                {
                    if (IsStopped)
                    {
                        IsStopped = false;
                        _startTimeBackup = TimelineViewModel.Ruler.HeadTime;
                        TMainViewModel.PreViewPlayCommand.Execute(null);
                        //Thread.Sleep(1000);
                        Timer.Start();
                        Timeline.IsStartTimeLine = true;
                    }
                    else
                    {
                        TMainViewModel.PreViewResumeCommand.Execute(null);
                        //Thread.Sleep(1000);
                        if (IsRecording)
                        {
                            Record.StartRecord();
                            //Thread.Sleep(1000);
                        }
                        Timer.Resume();
                    }
                }
                else
                {
                    IsStopped = false;
                    TMainViewModel.PreViewPauseCommand.Execute(null);
                    if (IsRecording)
                        Record.PauseRecord();
                    Timer.Pause();
                }
            }
        }

        private bool _isStopped = true;
        /// <summary>
        /// Trạng thái đã dừng cho Preview
        /// </summary>
        public bool IsStopped
        {
            get { return _isStopped; }
            set { _isStopped = value; OnPropertyChanged(); }
        }

        private bool _isFreeThumb = true;
        public bool IsFreeThumb { get => _isFreeThumb; set => _isFreeThumb = value; }
        private RelayCommand _stopCommand;
        /// <summary>
        /// Lệnh ngừng Preview
        /// </summary>
        public RelayCommand StopCommand
        {
            get { return _stopCommand ?? (_stopCommand = new RelayCommand(x => StopExcute(), c => true)); }
        }

        private void StopExcute()
        {
            if (Timer != null)
            {
                double duration = (TimelineViewModel.Ruler.HeadTime - Timer.StartTime) * 1000;
                Timer.Stop();
                Timeline.IsStartTimeLine = false;
                IsPlaying = false;
                IsStopped = true;
                if (Math.Abs(TimelineViewModel.Ruler.LeftTime - TimelineViewModel.Ruler.RightTime) < 0.1)
                {
                    TimelineViewModel.Ruler.LeftTime = TimelineViewModel.Ruler.RightTime = TimelineViewModel.Ruler.HeadTime = _startTimeBackup;
                }
                else
                {
                    TimelineViewModel.Ruler.HeadTime = TimelineViewModel.Ruler.LeftTime;
                }
                TimelineViewModel.Ruler.UpdateProperties();
                IsFreeThumb = true;
                TMainViewModel.PreViewStopCommand.Execute(null);
                if (IsRecording)
                    Record.StopRecord(duration);
                IsRecording = false;
            }
        }
        #endregion

        #region RecordCommand
        private ICommand _recordCommand;
        public ICommand RecordCommand
        {
            get => _recordCommand ?? (_recordCommand = new RelayCommand(x => { RecordExcute(); }, p => RecordCanExcute()));
        }

        private bool RecordCanExcute()
        {
            return Record.DeviceSelected != null;
        }

        private void RecordExcute()
        {
            PlayExcute();
            Record.StartRecord();
            IsRecording = true;
        }
        #endregion
    }
}
