﻿using INV.Elearning.Controls.Audio;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Timeline;
using INV.Elearning.Core.View;
using System;
using System.Windows;
using System.Windows.Input;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: RulerViewModel.cs
    // Description: Điều khiển truy xuất dữ liệu RulerModel được dùng bởi RulerTimeline
    // Develope by : Nguyen Van Ngoc
    // History:
    // 22/03/2018 : Modifier
    //---------------------------------------------------------------------------
    /// <summary>
    /// Điều khiển truy xuất dữ liệu RulerModel được dùng bởi RulerTimeline
    /// </summary>
    public class RulerViewModel : RootViewModel
    {
        public TimelineViewModel TimelineViewModel { get; set; }
        public RulerViewModel(TimelineViewModel timelineViewModel)
        {
            TimelineViewModel = timelineViewModel;
        }

        #region Dữ liệu RulerModel và các thuộc tính truy xuất dữ liệu
        /// <summary>
        /// Dữ liệu
        /// </summary>
        public RulerModel Ruler { get; set; }

        /// <summary>
        /// Độ chia nhỏ nhất
        /// </summary>
        public double MinimummScale
        {
            get
            {
                return Ruler.MinimummScale;
            }

            set
            {
                Ruler.MinimummScale = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Độ chia
        /// </summary>
        public double Scale
        {
            get
            {
                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing is Timing timing)
                {
                    return timing.ScaleRuler == 0 ? 15 : timing.ScaleRuler;
                }

                return 15;
            }

            set
            {
                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing is Timing timing)
                {
                    (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Timing.ScaleRuler = Math.Round(value, 0);
                }

                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Độ chia lớn nhất
        /// </summary>
        public double MaximumScale
        {
            get
            {
                return Ruler.MaximumScale;
            }

            set
            {
                Ruler.MaximumScale = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Thời điểm bắt đầu Preview
        /// </summary>
        public double StartTime
        {
            get
            {
                return Ruler.StartTime;
            }

            set
            {
                Ruler.StartTime = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Độ chia thời gian
        /// </summary>
        public double ScaleTime
        {
            get
            {
                return Ruler.ScaleTime;
            }

            set
            {
                Ruler.ScaleTime = value;
                TimelineViewModel.UpdateObjectElements();
                UpdateProperties();
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Khoảng cuộn thước
        /// </summary>
        public double ScrollOffset
        {
            get
            {
                return Ruler.ScrollOffset;
            }

            set
            {
                Ruler.ScrollOffset = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Tổng thời gian trình chiếu
        /// </summary>
        public double TotalTime
        {
            get
            {
                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing is Timing timing)
                {
                    return timing.TotalTime;
                }

                return 5;
            }

            set
            {
                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing is Timing timing)
                {
                    timing.TotalTime = value;
                }

                TimelineViewModel.UpdateTracks();
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Độ rộng (px) tương ứng với TotalTime
        /// </summary>
        public double TotalTimePixel
        {
            get
            {
                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing is Timing timing)
                {
                    return CalculatePixel(timing.TotalTime);
                }

                return Ruler.TotalTimePixel;
            }

            set
            {
                Ruler.TotalTimePixel = value;
                // cập nhật lại độ rộng khay quản lí
                TrackManagerWidth = Math.Max(value + 150, TrackManagerWidth);
                if (Timeline.Current != null)
                {
                    if (value + 150 < Timeline.Current.ActualWidth)
                    {
                        // cập nhật lại độ rộng khay quản lí
                        TrackManagerWidth = Math.Min(value + 150, TrackManagerWidth);
                    }
                    else
                    {
                        // cập nhật lại độ rộng khay quản lí
                        TrackManagerWidth = Math.Max(value + 150, TrackManagerWidth);
                    }
                }
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Độ rộng khay quản lí <see cref="TrackManager"/>
        /// </summary>
        public double TrackManagerWidth
        {
            get => Ruler.TrackManagerWidth;
            set
            {
                Ruler.TrackManagerWidth = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Thời điểm bắt đầu vùng chọn thời gian (Ripple)
        /// </summary>
        public double LeftTime
        {
            get => Ruler.LeftTime;
            set
            {
                Ruler.LeftTime = value;
                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing is Timing timing)
                {
                    timing.StartTime = value;
                }
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Tọa độ trái của vùng chọn thời gian
        /// </summary>
        public double LeftTimePixel
        {
            get => Ruler.LeftTimePixel;
            set
            {
                Ruler.LeftTimePixel = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Thời điểm kết thúc vùng chọn thời gian (Ripple)
        /// </summary>
        public double RightTime
        {
            get => Ruler.RightTime;
            set
            {
                Ruler.RightTime = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Tọa độ phải của vùng chọn thời gian
        /// </summary>
        public double RightTimePixel
        {
            get => Ruler.RightTimePixel;
            set
            {
                Ruler.RightTimePixel = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Thời điểm hiện tại lúc Preview
        /// </summary>
        public double HeadTime
        {
            get
            {
                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing is Timing timing)
                {
                    return timing.HeadTime;
                }
                return Ruler.HeadTime;
            }

            set
            {
                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing is Timing timing)
                {
                    timing.HeadTime = value;
                }

                Ruler.HeadTime = value;
                OnPropertyChanged();
            }
        }

        internal void Notify()
        {
            foreach (ObjectElement item in TimelineViewModel.GetObjectElements(false))
            {
                if (item.Timing != null)
                {
                    item.Timing.LeftTrack = CalculatePixel(item.Timing.StartTime);
                    item.Timing.WidthTrack = CalculatePixel(item.Timing.Duration);
                    if (item.Timing.ShowAlways || item.Timing.ShowUntilEnd || TotalTime < item.Timing.StartTime + item.Timing.Duration)
                    {
                        if (item.Timing.ShowUntilEnd == true)
                        {
                            item.Timing.Duration = TotalTime - item.Timing.StartTime;
                            item.Timing.WidthTrack = TotalTimePixel - item.Timing.LeftTrack;
                        }
                        else
                        {
                            double durationAmount = TotalTime / ScaleTime - item.Timing.StartTime / ScaleTime;
                            if (durationAmount < 1)
                            {
                                item.Timing.Duration = ScaleTime;
                                item.Timing.WidthTrack = Scale;
                            }
                            else
                            {
                                item.Timing.StartTime = TotalTime - item.Timing.Duration;
                                item.Timing.LeftTrack = TotalTimePixel - item.Timing.WidthTrack;
                            }
                        }
                    }
                }

            }
        }

        /// <summary>
        /// Tọa độ tương ứng với <see cref="HeadTime"/>
        /// </summary>
        public double HeadTimePixel
        {
            get => Ruler.HeadTimePixel;
            set
            {
                Ruler.HeadTimePixel = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Số vạch chia mà <see cref="TotalTime"/> chiếm giữ
        /// </summary>
        public double TotalTimeAmount
        {
            get
            {
                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing is Timing timing)
                {
                    return timing.TotalTime / ScaleTime;
                }

                return TotalTime / ScaleTime;
            }

            set
            {
                Ruler.TotalTime = value * ScaleTime;                    // cập nhật dữ liệu
                TotalTimePixel = value * Scale;     // cập nhật view

                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing == null)
                {
                    (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Timing = new Timing()
                    {
                        ScaleRuler = 15,
                        TotalTime = 5
                    };

                }
                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing is Timing timing)
                {
                    timing.TotalTime = Ruler.TotalTime;
                }
            }
        }
        /// <summary>
        /// Trạng thái Ẩn/Hiện các thumb trên <see cref="RulerTimelineContainer"/>
        /// </summary>
        public bool IsShowThumbs
        {
            get => Ruler.IsShowThumbs;
            set
            {
                Ruler.IsShowThumbs = value;
                OnPropertyChanged();
            }
        }

        private double _leftRipple;
        /// <summary>
        /// Lề trái của vùng chọn thời gian (Ripple)
        /// </summary>
        public double LeftRipple
        {
            get { return _leftRipple; }
            set { _leftRipple = value; OnPropertyChanged(); }
        }

        private double _rightRipple;
        /// <summary>
        /// Lề phải của vùng chọn thời gian
        /// </summary>
        public double RightRipple
        {
            get { return _rightRipple; }
            set { _rightRipple = value; OnPropertyChanged(); }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Cập nhật lại giá trị cho các thuộc tính
        /// </summary>
        public void UpdateProperties()
        {
            LeftTimePixel = CalculatePixel(LeftTime);
            RightTimePixel = CalculatePixel(RightTime);
            HeadTimePixel = CalculatePixel(HeadTime);
            TotalTimePixel = CalculatePixel(TotalTime);
        }

        /// <summary>
        /// Cập nhật lại giá trị cho vùng chọn thời gian
        /// </summary>
        public void UpdateSelectionTime()
        {
            LeftTime = CalculateTime(LeftTimePixel);
            RightTime = CalculateTime(RightTimePixel);
            HeadTime = CalculateTime(HeadTimePixel);
        }

        /// <summary>
        /// Đổi pixel sang time
        /// </summary>
        /// <param name="pixel"></param>
        /// <returns></returns>
        public double CalculateTime(double pixel)
        {
            return Math.Round(pixel / Scale * ScaleTime, 2);
        }

        /// <summary>
        /// Đổi time sang pixel
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public double CalculatePixel(double time)
        {
            return ConvertPixel(time);
        }

        private double ConvertPixel(double time)
        {
            return (time / ScaleTime * Scale).Round();
        }

        public double TimeToPixel(double time)
        {
            return (time / ScaleTime).Round() * Scale;
        }

        //private double GetFitScale()
        //{
        //    double fitScale = 1;
        //    if (MaximumScale > MinimummScale)
        //    {
        //        if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing is Timing timing)
        //        {
        //            double
        //                totalTimePixel = ConvertPixel(timing.TotalTime),
        //                a = (1 - (TrackManagerWidth + 150) / totalTimePixel) / (MaximumScale - MinimummScale),
        //                b = 1 - a * MaximumScale;
        //            fitScale = a * Scale + b;
        //        }

        //    }
        //    return fitScale;
        //}
        #endregion

        
    }
}
