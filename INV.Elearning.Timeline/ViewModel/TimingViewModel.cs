﻿using INV.Elearning.Core.Timeline;
using INV.Elearning.Core.View;
using System;
using System.Windows;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: TimingViewModel.cs
    // Description: Quản lí cửa sổ Timing
    // Develope by : Nguyen Van Ngoc
    // History:
    // 22/03/2018 : Modifier
    //---------------------------------------------------------------------------
    /// <summary>
    /// Quản lí cửa sổ Timing
    /// </summary>
    public class TimingViewModel : RootViewModel
    {
        public TimingViewModel()
        {
            TrackViewModel = new TrackViewModel
            {
                CurrentTrack = new ObjectElement() { Timing = new Timing() }
            };
        }
        /// <summary>
        /// ViewModel của khay đang được xử lí
        /// </summary>
        public TrackViewModel TrackViewModel { get; set; }

        /// <summary>
        /// Thời điểm bắt đầu
        /// </summary>
        public double StartTime
        {
            get => TrackViewModel.CurrentTrack.Timing.StartTime;
            set
            {
                //TrackViewModel.CurrentTrack.Timing.StartTime = value;
                ChangeStartTime(TrackViewModel.CurrentTrack, value);
                TrackViewModel.TimelineViewModel.UpdateTracks(false);
                OnPropertyChanged("Duration");
                OnPropertyChanged();
            }
        }
        public double OldTotalTime { get; set; }
        private void ChangeStartTime(ObjectElement info, double startTime)
        {
            RulerViewModel ruler = TrackViewModel.TimelineViewModel.Ruler;
            double
                scaleTime = ruler.ScaleTime,
                scaleRuler = ruler.Scale,
                totalTime = OldTotalTime;

            if (!info.Timing.ShowAlways)
            {
                if (startTime <= 0.0)
                {
                    info.Timing.StartTime = 0;
                    if (info.Timing.ShowUntilEnd)
                    {
                        info.Timing.Duration += info.Timing.StartTime;
                    }
                }
                else
                {
                    info.Timing.StartTime = startTime;

                    if (info.Timing.ShowUntilEnd)
                    {
                        if (startTime > totalTime - scaleTime)
                        {
                            info.Timing.Duration = scaleTime;
                        }
                        else
                        {
                            info.Timing.Duration = Math.Abs(totalTime - startTime);
                        }
                    }

                    totalTime = info.Timing.StartTime + info.Timing.Duration;
                }

                ruler.TotalTime = Math.Max(totalTime, OldTotalTime);
                ruler.TotalTimePixel = ruler.TimeToPixel(ruler.TotalTime);
                info.Timing.LeftTrack = ruler.TimeToPixel(info.Timing.StartTime);
                info.Timing.WidthTrack = ruler.TimeToPixel(info.Timing.Duration);
            }
        }

        /// <summary>
        /// Khoảng thời gian
        /// </summary>
        public double Duration
        {
            get => TrackViewModel.CurrentTrack.Timing.Duration;
            set
            {
                ChangeDuration(TrackViewModel.CurrentTrack, value);
                TrackViewModel.TimelineViewModel.UpdateTracks();
                OnPropertyChanged();
            }
        }

        private void ChangeDuration(ObjectElement info, double duration)
        {
            RulerViewModel ruler = TrackViewModel.TimelineViewModel.Ruler;
            double
                scaleTime = ruler.ScaleTime,
                scaleRuler = ruler.Scale,
                totalTime = OldTotalTime;

            if (!info.Timing.ShowAlways && !info.Timing.ShowUntilEnd)
            {
                info.Timing.Duration = Math.Max(scaleTime, duration);
                totalTime = info.Timing.StartTime + info.Timing.Duration;

                ruler.TotalTime = Math.Max(totalTime, OldTotalTime);
                ruler.TotalTimePixel = ruler.TimeToPixel(ruler.TotalTime);
                info.Timing.LeftTrack = ruler.TimeToPixel(info.Timing.StartTime);
                info.Timing.WidthTrack = ruler.TimeToPixel(info.Timing.Duration);
            }
        }

        /// <summary>
        /// Trạng thái <see cref="ObjectElement.Timing.ShowUntilEnd"/>
        /// </summary>
        public bool ShowUntilEnd
        {
            get => TrackViewModel.CurrentTrack.Timing.ShowUntilEnd;
            set
            {
                TrackViewModel.CurrentTrack.Timing.ShowUntilEnd = value;
                TrackViewModel.TimelineViewModel.UpdateTracks(false);
                OnPropertyChanged("Duration");
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Trạng thái <see cref="ObjectElement.Timing.ShowAlways"/>
        /// </summary>
        public bool ShowAlways
        {
            get => TrackViewModel.CurrentTrack.Timing.ShowAlways;
            set
            {
                TrackViewModel.CurrentTrack.Timing.ShowAlways = value;
                TrackViewModel.TimelineViewModel.UpdateTracks(false);
                OnPropertyChanged("StartTime");
                OnPropertyChanged("Duration");
                OnPropertyChanged();
            }
        }

        public double TotalTime
        {
            get { return TrackViewModel.TimelineViewModel.Ruler.TotalTime; }
            set
            {
                TimelineViewModel viewModel = TrackViewModel.TimelineViewModel;
                double
                    totalTime = viewModel.Ruler.TotalTime,
                    deltaTime = value - totalTime;
                TotalTimeControl.ChangeTotalTime(viewModel, totalTime, deltaTime);
                OnPropertyChanged();
                OnPropertyChanged("UpHourEnabled");
                OnPropertyChanged("DownHourEnabled");
                OnPropertyChanged("UpMinuteEnabled");
                OnPropertyChanged("DownMinuteEnabled");
                OnPropertyChanged("UpSecondEnabled");
                OnPropertyChanged("DownSecondEnabled");
                OnPropertyChanged("UpTickEnabled");
                OnPropertyChanged("DownTickEnabled");
            }
        }

        public bool UpHourEnabled
        {
            get
            {
                double totalTime = TrackViewModel.TimelineViewModel.Ruler.TotalTime + TimeSpan.FromHours(1).TotalSeconds;
                return IsShouldChangeTotalTime(totalTime);
            }
            set
            {
                OnPropertyChanged();
            }
        }

        public bool DownHourEnabled
        {
            get
            {
                double totalTime = TrackViewModel.TimelineViewModel.Ruler.TotalTime - TimeSpan.FromHours(1).TotalSeconds;
                return IsShouldChangeTotalTime(totalTime);
            }
            set
            {
                OnPropertyChanged();
            }
        }
        public bool UpMinuteEnabled
        {
            get
            {
                double totalTime = TrackViewModel.TimelineViewModel.Ruler.TotalTime + TimeSpan.FromMinutes(1).TotalSeconds;
                return IsShouldChangeTotalTime(totalTime);
            }
            set
            {
                OnPropertyChanged();
            }
        }
        public bool DownMinuteEnabled
        {
            get
            {
                double totalTime = TrackViewModel.TimelineViewModel.Ruler.TotalTime - TimeSpan.FromMinutes(1).TotalSeconds;
                return IsShouldChangeTotalTime(totalTime);
            }
            set
            {
                OnPropertyChanged();
            }
        }
        public bool UpSecondEnabled
        {
            get
            {
                double totalTime = TrackViewModel.TimelineViewModel.Ruler.TotalTime + TimeSpan.FromSeconds(1).TotalSeconds;
                return IsShouldChangeTotalTime(totalTime);
            }
            set
            {
                OnPropertyChanged();
            }
        }
        public bool DownSecondEnabled
        {
            get
            {
                double totalTime = TrackViewModel.TimelineViewModel.Ruler.TotalTime - TimeSpan.FromSeconds(1).TotalSeconds;
                return IsShouldChangeTotalTime(totalTime);
            }
            set
            {
                OnPropertyChanged();
            }
        }

        public bool UpTickEnabled
        {
            get
            {
                double totalTime = TrackViewModel.TimelineViewModel.Ruler.TotalTime + 0.1;
                return IsShouldChangeTotalTime(totalTime);
            }
            set
            {
                OnPropertyChanged();
            }
        }

        public bool DownTickEnabled
        {
            get
            {
                double totalTime = TrackViewModel.TimelineViewModel.Ruler.TotalTime - 0.1;
                return IsShouldChangeTotalTime(totalTime);
            }
            set
            {
                OnPropertyChanged();
            }
        }
        private bool IsShouldChangeTotalTime(double totalTime)
        {
            return TotalTimeControl.IsShouldChangeTotalTime(TrackViewModel.TimelineViewModel, totalTime);
        }

        private RelayCommand _closeWindowCommand;
        /// <summary>
        /// Lệnh đóng cửa sổ
        /// </summary>
        public RelayCommand CloseWindowCommand
        {
            get { return _closeWindowCommand ?? (_closeWindowCommand = new RelayCommand(x => CloseWindow(x), c => true)); }
        }

        private void CloseWindow(object x)
        {
            if (x is Window window)
            {
                window.Close();
            }
        }

    }
}
