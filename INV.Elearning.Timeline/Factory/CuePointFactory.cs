﻿using INV.Elearning.Core.Timeline;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: CuePointFactory.cs
    // Description: Đối tượng được dùng để tạo các điểm bổ sung trên thước (CuePoint)
    // Develope by : Nguyen Van Ngoc
    // History:
    // 22/03/2018 : Modifier
    //---------------------------------------------------------------------------
    /// <summary>
    /// Đối tượng được dùng để tạo các điểm bổ sung trên thước (CuePoint)
    /// </summary>
    public class CuePointFactory
    {

        private static int _order;
        /// <summary>
        /// Số thứ tự của điểm được tạo
        /// </summary>
        public static int Order { get => _order; set => _order = value; }

        /// <summary>
        /// Tạo điểm bổ sung
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public CuePointModel CreateCuePoint(double time)
        {
            Order += 1;
            return new CuePointModel(Order, time);
        }

        public static void Reset()
        {
            Order = 0;
        }
    }
}
