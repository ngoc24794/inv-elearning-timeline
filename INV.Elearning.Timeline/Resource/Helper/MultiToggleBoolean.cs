﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: MultiToggleBoolean.cs
    // Description: Toggle Duration Spiner trong TimingWindow
    // Develope by : Nguyen Van Ngoc
    // History:
    // 01/01/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Toggle Duration Spiner trong TimingWindow
    /// </summary>
    public class MultiToggleBoolean : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            bool boolean = true;

            foreach (var item in values)
            {
                if(item is bool value && value)
                {
                    return false;
                }
            }

            return boolean;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
