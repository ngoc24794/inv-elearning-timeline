﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: ToggleBoolean.cs
    // Description: Toggle StartTime Spin trong TimingWindow
    // Develope by : Nguyen Van Ngoc
    // History:
    // 01/01/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Toggle StartTime Spin trong TimingWindow
    /// </summary>
    public class ToggleBoolean : IValueConverter
    {
        /// <summary>
        /// ToggleBoolean
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool boolean = (bool)value;
            return !boolean;
        }

        /// <summary>
        /// ToggleBoolean
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool boolean = (bool)value;
            return !boolean;
        }
    }
}
