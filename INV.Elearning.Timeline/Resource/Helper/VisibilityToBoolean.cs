﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: VisibilityToBoolean.cs
    // Description: Chuyển đổi kiểu khi Binding
    // Develope by : Nguyen Van Ngoc
    // History:
    // 01/01/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Chuyển đổi Visibitly - Boolean
    /// </summary>
    public class VisibilityToBoolean : IValueConverter
    {
        /// <summary>
        /// Visibily to Boolean
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility visibility = (Visibility)value;
            return visibility == Visibility.Visible;
        }

        /// <summary>
        /// Boolean to Visibility
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool boolean = (bool)value;
            return boolean ? Visibility.Visible : Visibility.Hidden;
        }
    }
}
