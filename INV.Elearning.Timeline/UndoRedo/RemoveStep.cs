﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  RemoveStep.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using INV.Elearning.Core.Helper;
using INV.Elearning.Core.ViewModel;
using System.Collections.Generic;

namespace INV.Elearning.Timeline
{
    /// <summary>
    /// Một bước Undo cho thao tác xóa một phần tử khỏi danh sách
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RemoveStep<T> : StepBase
    {
        public RemoveStep(IList<T> list, T item, int currentIndex)
        {
            List = list;
            Item = item;
            CurrentIndex = currentIndex;
        }
        /// <summary>
        /// Danh sách phần tử
        /// </summary>
        public IList<T> List { get; set; }
        /// <summary>
        /// Phần tử đang bị tác động
        /// </summary>
        public T Item { get; set; }
        /// <summary>
        /// Chỉ số của <see cref="Item"/> trong <see cref="List"/>
        /// </summary>
        public int CurrentIndex { get; set; }

        public override void UndoExcute()
        {
            Global.BeginInit();
            List.Insert(CurrentIndex, Item);
            Global.EndInit();
        }

        public override void RedoExcute()
        {
            Global.BeginInit();
            List.RemoveAt(CurrentIndex);
            Global.EndInit();
        }
    }
}
