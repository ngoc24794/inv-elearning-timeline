﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  DragTotalTimeControlUndo.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using INV.Elearning.Core.Helper;
using INV.Elearning.Core.ViewModel;

namespace INV.Elearning.Timeline
{
    /// <summary>
    /// Một bước Undo cho thao tác kéo thanh tổng thời gian
    /// </summary>
    internal class DragTotalTimeControlStep : StepBase
    {
        public DragTotalTimeControlStep(RulerState rulerState)
        {
            RulerState = rulerState;
        }
        /// <summary>
        /// Trạng thái của <see cref="RulerViewModel"/>
        /// </summary>
        public RulerState RulerState { get; set; }
        public override void UndoExcute()
        {
            Global.BeginInit();
            RulerState.Undo();
            RulerState.RulerViewModel.Notify();
            Global.EndInit();
        }

        public override void RedoExcute()
        {
            Global.BeginInit();
            RulerState.Redo();
            RulerState.RulerViewModel.Notify();
            Global.EndInit();
        }
    }
}
