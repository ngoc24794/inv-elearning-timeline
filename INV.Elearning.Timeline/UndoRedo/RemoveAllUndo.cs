﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  RemoveAllStep.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using INV.Elearning.Core.Helper;
using INV.Elearning.Core.ViewModel;
using System.Collections.Generic;

namespace INV.Elearning.Timeline
{
    /// <summary>
    /// Một bước Undo cho thao tác xóa tất cả các phần tử khỏi danh sách
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RemoveAllStep<T> : StepBase
    {
        public RemoveAllStep(IList<T> originList, IList<T> backupList)
        {
            OriginList = originList;
            BackupList = backupList;
        }
        /// <summary>
        /// Danh sách phần tử chịu tác động
        /// </summary>
        public IList<T> OriginList { get; set; }
        /// <summary>
        /// Danh sách lưu trữ
        /// </summary>
        public IList<T> BackupList { get; set; }

        public override void UndoExcute()
        {
            Global.BeginInit();
            OriginList.Clear();
            foreach (T item in BackupList)
            {
                OriginList.Add(item);
            }
            Global.EndInit();
        }

        public override void RedoExcute()
        {
            Global.BeginInit();
            OriginList.Clear();
            Global.EndInit();
        }
    }
}
