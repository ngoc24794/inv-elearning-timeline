﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  AddStep.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
* + Modifer: 23-8
===========================================================*/

using INV.Elearning.Core.Helper;
using INV.Elearning.Core.ViewModel;
using System.Collections.Generic;

namespace INV.Elearning.Timeline
{
    /// <summary>
    /// Một bước Undo cho thao tác thêm một phần tử vào danh sách
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class AddStep<T> : StepBase
    {
        public AddStep(IList<T> list, T item)
        {
            List = list;
            Item = item;
        }
        /// <summary>
        /// Danh sách phần tử
        /// </summary>
        public IList<T> List { get; set; }
        /// <summary>
        /// Phần tử đang bị tác động
        /// </summary>
        public T Item { get; set; }

        public override void UndoExcute()
        {
            Global.BeginInit();
            List.Remove(Item);
            Global.EndInit();
        }

        public override void RedoExcute()
        {
            Global.BeginInit();
            List.Add(Item);
            Global.EndInit();
        }
    }
}
