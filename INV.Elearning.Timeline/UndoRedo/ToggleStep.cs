﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  ToggleStep.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Timeline;
using INV.Elearning.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.Windows;

namespace INV.Elearning.Timeline
{
    internal class ToggleStep : StepBase
    {
        public ToggleStep(List<ToggleState> states, RulerViewModel rulerViewModel)
        {
            States = states;
            RulerViewModel = rulerViewModel;
        }

        public List<ToggleState> States { get; set; }
        public RulerViewModel RulerViewModel { get; set; }

        public override void UndoExcute()
        {
            Global.BeginInit();
            foreach (ToggleState state in States)
            {
                state.Undo(RulerViewModel);
            }
            Global.EndInit();
        }

        public override void RedoExcute()
        {
            Global.BeginInit();
            foreach (ToggleState state in States)
            {
                state.Redo(RulerViewModel);
            }
            Global.EndInit();
        }
    }

    internal struct ToggleState
    {
        public ToggleState(Timing timing) : this()
        {
            Timing = timing;
            StartTime = new UndoValue<double>();
            Duration = new UndoValue<double>();
            ShowEnd = new UndoValue<bool>();
            ShowAlways = new UndoValue<bool>();
        }

        public UndoValue<double> StartTime { get; private set; }
        public UndoValue<double> Duration { get; private set; }
        public UndoValue<bool> ShowEnd { get; private set; }
        public UndoValue<bool> ShowAlways { get; private set; }
        public Timing Timing { get; set; }
        public void SetOldValue()
        {
            StartTime.SetOldValue(Timing.StartTime);
            Duration.SetOldValue(Timing.Duration);
            ShowEnd.SetOldValue(Timing.ShowUntilEnd);
            ShowAlways.SetOldValue(Timing.ShowAlways);
        }

        public void SetNewValue()
        {
            StartTime.SetNewValue(Timing.StartTime);
            Duration.SetNewValue(Timing.Duration);
            ShowEnd.SetNewValue(Timing.ShowUntilEnd);
            ShowAlways.SetNewValue(Timing.ShowAlways);
        }

        public void Undo(RulerViewModel rulerViewModel)
        {
            Timing.StartTime = StartTime.OldValue;
            Timing.LeftTrack = rulerViewModel.TimeToPixel(Timing.StartTime);
            Timing.Duration = Duration.OldValue;
            Timing.WidthTrack = rulerViewModel.TimeToPixel(Timing.Duration);
            Timing.ShowUntilEnd = ShowEnd.OldValue;
            Timing.ShowAlways = ShowAlways.OldValue;
        }

        public void Redo(RulerViewModel rulerViewModel)
        {
            Timing.StartTime = StartTime.NewValue;
            Timing.LeftTrack = rulerViewModel.TimeToPixel(Timing.StartTime);
            Timing.Duration = Duration.NewValue;
            Timing.WidthTrack = rulerViewModel.TimeToPixel(Timing.Duration);
            Timing.ShowUntilEnd = ShowEnd.NewValue;
            Timing.ShowAlways = ShowAlways.NewValue;
        }
    }
}
