﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  MoveUpStep.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using INV.Elearning.Core.Helper;
using INV.Elearning.Core.ViewModel;
using System.Collections.Generic;

namespace INV.Elearning.Timeline
{
    /// <summary>
    /// Một bước Undo cho thao tác di chuyển khay lên trên
    /// </summary>
    public class MoveUpStep<T> : StepBase
    {
        public MoveUpStep(IList<T> elements, T element, int oldIndex, int newIndex)
        {
            List = elements;
            Item = element;
            OldIndex = oldIndex;
            NewIndex = newIndex;
        }

        /// <summary>
        /// Danh sách phần tử
        /// </summary>
        public IList<T> List { get; set; }
        /// <summary>
        /// Phần tử đang bị di chuyển
        /// </summary>
        public T Item { get; set; }
        /// <summary>
        /// Chỉ số xác định vị trí cũ của <see cref="Item"/> trong <see cref="List"/>
        /// </summary>
        public int OldIndex { get; set; }
        /// <summary>
        /// Chỉ số xác định vị trí hiện tại của <see cref="Item"/> trong <see cref="List"/>
        /// </summary>
        public int NewIndex { get; set; }

        public override void UndoExcute()
        {
            Global.BeginInit();
            List.RemoveAt(NewIndex);
            List.Insert(OldIndex, Item);
            Global.EndInit();
        }

        public override void RedoExcute()
        {
            Global.BeginInit();
            List.RemoveAt(OldIndex);
            List.Insert(NewIndex, Item);
            Global.EndInit();
        }
    }
}
