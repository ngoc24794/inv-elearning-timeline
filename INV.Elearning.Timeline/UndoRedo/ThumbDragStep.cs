﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  ThumbDragStep.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Timeline;
using INV.Elearning.Core.ViewModel;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Timeline
{
    /// <summary>
    /// Một bước Undo cho thao tác kéo Thumb
    /// </summary>
    internal class ThumbDragStep : StepBase
    {
        public ThumbDragStep(List<TimingState> states, RulerState rulerState, Action<bool> updateTracks)
        {
            States = states;
            RulerState = rulerState;
            UpdateTracks = updateTracks;
        }
        /// <summary>
        /// Danh sách trạng thái <see cref="Timing"/>
        /// </summary>
        public List<TimingState> States { get; set; }
        /// <summary>
        /// Trạng thái của <see cref="RulerViewModel"/>
        /// </summary>
        public RulerState RulerState { get; set; }
        /// <summary>
        /// Hàm cập nhật các khay
        /// </summary>
        public Action<bool> UpdateTracks { get; set; }

        public override void UndoExcute()
        {
            Global.BeginInit();
            foreach (TimingState state in States)
            {
                state.Undo(RulerState.RulerViewModel);
            }
            RulerState.Undo();
            UpdateTracks(false);
            Global.EndInit();
        }

        public override void RedoExcute()
        {
            Global.BeginInit();
            foreach (TimingState state in States)
            {
                state.Redo(RulerState.RulerViewModel);
            }
            RulerState.Redo();
            UpdateTracks(false);
            Global.EndInit();
        }
    }
    /// <summary>
    /// Trạng thái của <see cref="RulerViewModel"/>
    /// </summary>
    internal struct RulerState
    {
        public RulerState(RulerViewModel rulerViewModel) : this()
        {
            RulerViewModel = rulerViewModel;
            TotalTime = new UndoValue<double>();
        }

        public UndoValue<double> TotalTime { get; private set; }
        public RulerViewModel RulerViewModel { get; private set; }

        public void Undo()
        {
            // Phục hồi giá trị cho RulerViewModel
            RulerViewModel.TotalTime = TotalTime.OldValue;
            RulerViewModel.TotalTimePixel = RulerViewModel.TimeToPixel(RulerViewModel.TotalTime);
        }

        public void Redo()
        {
            // Phục hồi giá trị cho RulerViewModel
            RulerViewModel.TotalTime = TotalTime.NewValue;
            RulerViewModel.TotalTimePixel = RulerViewModel.TimeToPixel(RulerViewModel.TotalTime);
        }

        public void SetOldState()
        {
            // Ghi giá trị cũ cho RulerViewModel
            TotalTime.SetOldValue(RulerViewModel.TotalTime);
        }

        public void SetNewState()
        {
            // Ghi giá trị mới cho RulerViewModel
            TotalTime.SetNewValue(RulerViewModel.TotalTime);
        }
    }
    /// <summary>
    /// Trạng thái của <see cref="Timing"/>
    /// </summary>
    internal struct TimingState
    {
        public TimingState(Timing timing) : this()
        {
            Timing = timing;
            StartTime = new UndoValue<double>();
            Duration = new UndoValue<double>();
            ShowEnd = new UndoValue<bool>();
            ShowAlways = new UndoValue<bool>();
        }

        public UndoValue<double> StartTime { get; private set; }
        public UndoValue<double> Duration { get; private set; }
        public UndoValue<bool> ShowEnd { get; set; }
        public UndoValue<bool> ShowAlways { get; set; }
        public Timing Timing { get; private set; }

        public void Undo(RulerViewModel rulerViewModel)
        {
            // Phục hồi giá trị cho Timing
            Timing.StartTime = StartTime.OldValue;
            Timing.LeftTrack = rulerViewModel.TimeToPixel(Timing.StartTime);
            Timing.Duration = Duration.OldValue;
            Timing.WidthTrack = rulerViewModel.TimeToPixel(Timing.Duration);
            Timing.ShowUntilEnd = ShowEnd.OldValue;
            Timing.ShowAlways = ShowAlways.OldValue;
        }

        public void Redo(RulerViewModel rulerViewModel)
        {
            // Hủy bỏ "Phục hồi giá trị cho Timing"
            Timing.StartTime = StartTime.NewValue;
            Timing.LeftTrack = rulerViewModel.TimeToPixel(Timing.StartTime);
            Timing.Duration = Duration.NewValue;
            Timing.WidthTrack = rulerViewModel.TimeToPixel(Timing.Duration);
            Timing.ShowUntilEnd = ShowEnd.NewValue;
            Timing.ShowAlways = ShowAlways.NewValue;
        }
        /// <summary>
        /// Ghi trạng thái cũ
        /// </summary>
        public void SetOldState()
        {
            // Ghi giá trị cũ cho Timing
            StartTime.SetOldValue(Timing.StartTime);
            Duration.SetOldValue(Timing.Duration);
            ShowEnd.SetOldValue(Timing.ShowUntilEnd);
            ShowAlways.SetOldValue(Timing.ShowAlways);
        }
        /// <summary>
        /// Ghi trạng thái mới
        /// </summary>
        public void SetNewState()
        {
            // Ghi giá trị mới cho Timing
            StartTime.SetNewValue(Timing.StartTime);
            Duration.SetNewValue(Timing.Duration);
            ShowEnd.SetNewValue(Timing.ShowUntilEnd);
            ShowAlways.SetNewValue(Timing.ShowAlways);
        }
    }
    /// <summary>
    /// Cấu trúc dữ liệu sử dụng cho Undo
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class UndoValue<T>
    {
        public UndoValue() { }
        public UndoValue(T oldValue, T newValue) : this()
        {
            OldValue = oldValue;
            NewValue = newValue;
        }
        /// <summary>
        /// Giá trị cũ
        /// </summary>
        public T OldValue { get; private set; }
        /// <summary>
        /// Giá trị mới
        /// </summary>
        public T NewValue { get; private set; }
        /// <summary>
        /// Ghi giá trị cũ vào <see cref="OldValue"/>
        /// </summary>
        /// <param name="value"></param>
        internal void SetOldValue(T value)
        {
            OldValue = value;
        }
        /// <summary>
        /// Ghi giá trị mới vào <see cref="NewValue"/>
        /// </summary>
        /// <param name="value"></param>
        internal void SetNewValue(T value)
        {
            NewValue = value;
        }
    }
}
