﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  AlignToHeadlineStep.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Timeline;
using INV.Elearning.Core.ViewModel;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Timeline
{
    /// <summary>
    /// Một bước Undo cho thao tác căn StartTime theo CuePoint
    /// </summary>
    internal class AlignToHeadlineStep : StepBase
    {
        public AlignToHeadlineStep(List<TrackState> states, RulerState rulerState, Action<bool> updateTracks, Action updateObjectElements)
        {
            States = states;
            RulerState = rulerState;
            UpdateTracks = updateTracks;
            UpdateObjectElements = updateObjectElements;
        }

        /// <summary>
        /// Danh sách trạng thái <see cref="Timing"/>
        /// </summary>
        public List<TrackState> States { get; set; }
        /// <summary>
        /// Trạng thái của <see cref="RulerViewModel"/>
        /// </summary>
        public RulerState RulerState { get; set; }
        /// <summary>
        /// Delegate cho hàm cập nhật các khay
        /// </summary>
        public Action<bool> UpdateTracks { get; set; }
        /// <summary>
        /// Delegate cho hàm cập nhật các đối tượng
        /// </summary>
        public Action UpdateObjectElements { get; set; }

        public override void UndoExcute()
        {
            Global.BeginInit();
            foreach (TrackState trackState in States)
            {
                trackState.Undo(RulerState.RulerViewModel);
            }
            RulerState.Undo();
            UpdateTracks(true);
            UpdateObjectElements();
            Global.EndInit();
        }

        public override void RedoExcute()
        {
            Global.BeginInit();
            foreach (TrackState trackState in States)
            {
                trackState.Redo(RulerState.RulerViewModel);
            }
            RulerState.Redo();
            UpdateTracks(true);
            UpdateObjectElements();
            Global.EndInit();
        }
    }
    /// <summary>
    /// Trạng thái của khay
    /// </summary>
    internal struct TrackState
    {
        public TrackState(Timing timing) : this()
        {
            Timing = timing;
            LeftTrack = new UndoValue<double>();
            WidthTrack = new UndoValue<double>();
        }
        /// <summary>
        /// Lề trái
        /// </summary>
        public UndoValue<double> LeftTrack { get; private set; }
        /// <summary>
        /// Độ rộng
        /// </summary>
        public UndoValue<double> WidthTrack { get; private set; }
        /// <summary>
        /// Thời gian
        /// </summary>
        public Timing Timing { get; set; }

        public void Undo(RulerViewModel ruler)
        {
            Timing.LeftTrack = LeftTrack.OldValue;
            Timing.StartTime = ruler.CalculateTime(Timing.LeftTrack);
            Timing.WidthTrack = WidthTrack.OldValue;
            Timing.Duration = ruler.CalculateTime(Timing.WidthTrack);
        }

        public void Redo(RulerViewModel ruler)
        {
            Timing.LeftTrack = LeftTrack.NewValue;
            Timing.StartTime = ruler.CalculateTime(Timing.LeftTrack);
            Timing.WidthTrack = WidthTrack.NewValue;
            Timing.Duration = ruler.CalculateTime(Timing.WidthTrack);
        }

        public void SetOldValue()
        {
            LeftTrack.SetOldValue(Timing.LeftTrack);
            WidthTrack.SetOldValue(Timing.WidthTrack);
        }

        public void SetNewValue()
        {
            LeftTrack.SetNewValue(Timing.LeftTrack);
            WidthTrack.SetNewValue(Timing.WidthTrack);
        }
    }
}
