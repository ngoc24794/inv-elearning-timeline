﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  DurationAnimationConverter.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using INV.Elearning.Animations;
using System;
using System.Globalization;
using System.Windows.Data;

namespace INV.Elearning.Timeline
{
    public class DurationAnimationConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] is double scaleRuler && values[1] is TimeSpan duration)
            {
                if (values[2] is NoneAnimation)
                {
                    return 0.1;
                }
                return Math.Round(duration.TotalSeconds, 1, MidpointRounding.AwayFromZero) / 0.1 * scaleRuler;
            }

            return 0.1;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
