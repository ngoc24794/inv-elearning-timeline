﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  TotalTimeConverter.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Timeline;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace INV.Elearning.Timeline
{
    public class TotalTimeConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length > 1)
            {
                if (values[0] is double totalTime && values[1] is double scale)
                {
                    return totalTime / 0.1 * scale;
                }
            }
            return 200;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            if (value is double totalTimePixel)
            {
                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing is Timing timing)
                {
                    object[] result = new object[2];
                    result[0] = totalTimePixel / timing.ScaleRuler * 0.1;
                    result[1] = timing.ScaleRuler;
                    return result;
                }
            }
            return null;
        }
    }
}
