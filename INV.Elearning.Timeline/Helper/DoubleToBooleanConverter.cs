﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  DoubleToBooleanConverter.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;
using System.Globalization;
using System.Windows.Data;

namespace INV.Elearning.Timeline
{
    public class DoubleToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double time = (double)value;
            TimelineViewModel viewModel = parameter as TimelineViewModel;
            return TotalTimeControl.IsShouldChangeTotalTime(viewModel, time);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
