﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  ScaleRulerConverter.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;
using System.Globalization;
using System.Windows.Data;

namespace INV.Elearning.Timeline
{
    public class ScaleRulerConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double scaleRuler)
            {
                return scaleRuler;
            }
            return 15;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double scaleRuler)
            {
                return scaleRuler;
            }
            return 15;
        }
    }
}
