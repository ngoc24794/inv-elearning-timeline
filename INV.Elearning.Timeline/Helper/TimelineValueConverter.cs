﻿using INV.Elearning.Core.View;
using System;
using System.Globalization;
using System.Windows.Data;

namespace INV.Elearning.Timeline
{
    public class TimelineValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is IGroupContainer;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
