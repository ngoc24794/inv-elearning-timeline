﻿using INV.Elearning.Core.View;
using System.Windows;
using System.Windows.Media;
using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace INV.Elearning.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: TimelineHelper.cs
    // Description: Lớp cung cấp các hàm hỗ trợ cho các xử lí trên Timeline
    // Develope by : Nguyen Van Ngoc
    // History:
    // 14/3/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp cung cấp các hàm hỗ trợ cho các xử lí trên Timeline
    /// </summary>
    public class TimelineHelper
    {
        /// <summary>
        /// Tìm cha trên VisualTree
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="child"></param>
        /// <returns></returns>
        public static T FindParent<T>(DependencyObject child) where T : DependencyObject
        {
            // Lấy cha
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);

            // không tìm thấy cha
            if (parentObject == null) return null;

            // lọc cha phù hợp
            if (parentObject is T parent)
                return parent;
            else
                return FindParent<T>(parentObject);
        }

        /// <summary>
        /// Tính toán lại độ rộng cho dải băng xanh (Ripple)
        /// </summary>
        /// <param name="listTrack"></param>
        /// <param name="left"></param>
        /// <param name="width"></param>
        public static void CalculateRipple(ObjectElement[] listTrack, out double left, out double right, bool ignoreSelected = false)
        {
            left = 0;
            right = 0;

            if (listTrack.Length > 0)
            {
                foreach (var item in listTrack)
                {
                    //---------------------------------------------------------------------------
                    // Xác định left, width
                    //---------------------------------------------------------------------------

                    if (item is ObjectElement first)
                    {
                        if (first.Timing != null && (first.IsSelected || ignoreSelected))
                        {
                            left = first.Timing.LeftTrack;
                            right = first.Timing.LeftTrack + first.Timing.WidthTrack;
                            break;
                        }
                    }
                }

                foreach (var item in listTrack)
                {
                    if (item is ObjectElement info)
                    {
                        if (info.Timing != null && (info.IsSelected || ignoreSelected))
                        {
                            if (info.Timing.LeftTrack < left) left = info.Timing.LeftTrack;
                            if (info.Timing.LeftTrack + info.Timing.WidthTrack > right) right = info.Timing.LeftTrack + info.Timing.WidthTrack;
                        }
                    }
                }
            }
        }

        public static IGroupContainer GetGroupingParent(ObjectElement[] list, ObjectElement element)
        {
            foreach (ObjectElement item in list)
            {
                if (item is IGroupContainer group && group.Elements.Contains(element))
                {
                    return group;
                }
            }
            return null;
        }

        public static ObservableCollection<ObjectElement> GetSubObjectCollection(TimelineViewModel viewModel, ObjectElement objectElement)
        {
            ObjectElement[] list = new ObjectElement[viewModel.GetObjectElements(false).Count];
            viewModel.GetObjectElements(false).CopyTo(list, 0);
            IGroupContainer groupingParent = GetGroupingParent(list, objectElement);
            ObservableCollection<ObjectElement> objectElements = new ObservableCollection<ObjectElement>();
            if (groupingParent == null)
            {
                objectElements = viewModel.Timeline.ObjectElements;
            }
            else
            {
                objectElements = groupingParent.Elements;
            }
            return objectElements;
        }
    }

    /// <summary>
    /// Lớp mở rộng của <see cref="Math"/>
    /// </summary>
    public static class MathEx
    {
        /// <summary>
        /// Làm tròn số thực đến số nguyên gần nhất.
        /// Quy tắc: số thực a,b.
        /// Nếu b >= 0.5 thì trả về a + 1.
        /// Ngược lại, trả về a.
        /// </summary>
        /// <param name="me">Số thực cần làm tròn</param>
        /// <returns></returns>
        public static int Round(this double me)
        {
            int integerPart = (int)me;
            double decimalPart = me - integerPart;

            return decimalPart < 0.5 ? integerPart : integerPart + 1;
        }

        public static double Round(this double me, int digits)
        {
            int integerPart = (int)me;
            if (digits == 0)
            {
                return integerPart;
            }
            double decimalPart = me - integerPart;
            int
                previousLastDigit = Digit(decimalPart, -digits + 1),
                lastDigit = Digit(me, -digits);

            return lastDigit < 5 ? integerPart + (int)(decimalPart * Math.Pow(10, digits - 1)) / Math.Pow(10, digits) : integerPart + 1;
        }

        private static byte Digit(double number, int index)
        {
            string intergerPart = ((int)number).ToString(), decimalPart = (number - (int)number).ToString();
            return (byte)number.ToString()[index];
        }
        public static int FixByUnit(this double me, double unit, int decimals = 3)
        {
            double _unit = Math.Round(unit, decimals), _me = Math.Round(me, decimals);
            return 1;
        }
    }
}
