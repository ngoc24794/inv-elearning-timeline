﻿using System.Windows;
using System.Windows.Controls;
namespace INV.Elearning.Timeline
{
    public class ContainerStyleSelector : StyleSelector
    {
        public override Style SelectStyle(object item, DependencyObject container)
        {
            if (!Application.Current.Resources.Contains("LeftObjectElementDataStyle"))
            {
                Application.Current.Resources.MergedDictionaries.Add(
                    new ResourceDictionary() { Source = new System.Uri("pack://application:,,,/INV.Elearning.Timeline;Component/Resource/Generic.xaml") });
            }
            return Application.Current.TryFindResource("LeftObjectElementDataStyle") as Style;
        }
    }
}
