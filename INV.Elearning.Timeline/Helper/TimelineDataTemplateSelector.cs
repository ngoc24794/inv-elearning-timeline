﻿using INV.Elearning.Core.Model;
using System.Windows;
using System.Windows.Controls;

namespace INV.Elearning.Timeline
{
    public class TimelineDataTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (!Application.Current.Resources.Contains("LeftObjectElementDataTemplte"))
            {
                Application.Current.Resources.MergedDictionaries.Add(
                    new ResourceDictionary() { Source = new System.Uri("pack://application:,,,/INV.Elearning.Timeline;Component/Resource/Generic.xaml") });
            }

            return Application.Current.TryFindResource("LeftObjectElementDataTemplte") as DataTemplate;
        }
    }
}
